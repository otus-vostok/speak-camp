// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SpeakCamp.AccountService.Data;
using SpeakCamp.AccountService.Data.Models;

namespace SpeakCamp.AccountService.Apps
{
    public interface IRefreshTokenApp
    {
        Task<(string jwtId, string  refresh)> CreateToken(int userId);
        Task<bool> DeleteToken(int userId);
        Task<(bool successeful, string message, int userId)> CheckToken(string refresh, string jti);
    }

    public class RefreshTokenApp : IRefreshTokenApp
    {
        private readonly IOptionsSnapshot<JwtSettings> _jwtTokenConfig;
        private readonly ApplicationDbContext _dbContext;
        public RefreshTokenApp(IOptionsSnapshot<JwtSettings> jwtTokenConfig, ApplicationDbContext dbContext)
        {
            _jwtTokenConfig = jwtTokenConfig;
            _dbContext = dbContext;
        }
        public async Task<(string jwtId, string  refresh)> CreateToken(int userId)
        {
            await DeleteToken(userId);

            var model = GenerateRefreshTokenString(userId);
            _dbContext.RefreshTokens.Add(model);
            await _dbContext.SaveChangesAsync();
            return (model.Id.ToString(), model.Token);
        }

        public async Task<bool> DeleteToken(int userId)
        {
            var models = await _dbContext.RefreshTokens.Where(t => t.UserId == userId).ToListAsync();
            if (models.Count == 0)
                return false;

            for (int i = 0; i < models.Count; i++)
                _dbContext.Entry(models[i]).State = EntityState.Deleted;

            return (await _dbContext.SaveChangesAsync()) > 0;
        }

        public async Task<(bool successeful, string message, int userId)> CheckToken(string refresh, string jti)
        {
            var storedRefreshToken = await _dbContext.RefreshTokens.FirstOrDefaultAsync(x => x.Token == refresh);
            if (!IsValid(storedRefreshToken, jti, out var message))
                return (false, message, 0);

            return (true, "", storedRefreshToken.UserId);
        }


        private bool IsValid(RefreshTokenModel storedRefreshToken, string jti, out string messageError)
        {
            if(storedRefreshToken == null)
            {
                messageError = "refresh token doesnt exist";
                return false;
            }

            if(DateTime.UtcNow > storedRefreshToken.Expires)
            {
                messageError = "token has expired, user needs to relogin";
                return false;
            }

            if(storedRefreshToken.Revoked != null)
            {
                messageError = "token has been revoked";
                return false;
            }

            if(storedRefreshToken.Id.ToString() != jti)
            {
                messageError = "the jti is not valid";
                return false;
            }

            messageError = "";
            return true;
        }
        private RefreshTokenModel GenerateRefreshTokenString(int userId) => new(){
            Id = Guid.NewGuid(),
            UserId = userId,
            Created = DateTime.UtcNow,
            Expires = DateTime.UtcNow.AddHours(_jwtTokenConfig.Value.RefreshTokenExpirationHours),
            Token =  CreateRandomString()
        };

        private string CreateRandomString(int length = 32)
        {
            var randomNumber = new byte[length];
            using var generator = new RNGCryptoServiceProvider();
            generator.GetBytes(randomNumber);
            return Convert.ToBase64String(randomNumber);
        }
    }
}
