// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace SpeakCamp.AccountService.Apps
{
    public interface IJwtTokenApp
    {
        string CreateToken(string jwi, string userId, string role);
        (bool successeful, string message, string jwtId) CanBeReplaced(string token);
    }


    public class JwtTokenApp : IJwtTokenApp
    {
        private readonly IOptionsSnapshot<JwtSettings> _jwtTokenConfig;

        public JwtTokenApp(IOptionsSnapshot<JwtSettings> jwtTokenConfig)
        {
            _jwtTokenConfig = jwtTokenConfig;
        }

        public string CreateToken(string jwi, string userId, string role)
        {
            var claims = new[]
            {
                new Claim(ClaimTypes.Role, role),
                new Claim(ClaimTypes.Name, userId),
                new Claim(JwtRegisteredClaimNames.Jti, jwi)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtTokenConfig.Value.Secret));
            var signIn = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var token = new JwtSecurityToken(
                _jwtTokenConfig.Value.Issuer,
                _jwtTokenConfig.Value.Audience,
                claims,
                expires: DateTime.UtcNow.AddMinutes(_jwtTokenConfig.Value.AccessTokenExpiration),
                signingCredentials: signIn
            );

            var access = new JwtSecurityTokenHandler().WriteToken(token);
            return access;
        }

        public (bool successeful, string message, string jwtId) CanBeReplaced(string token)
        {
            IEnumerable<Claim> claims = GetPrincipalFromExpiredToken(token)?.Claims;
            if(claims == null)
                return (false, "The token is invalid", "");

            // expiry date get
            if(!TryTakeExpClaimsFromToken(claims, out var expDate))
                return (false, "Failed to get data(Exp) from token", "");

            // expiry date check
            if(expDate > DateTime.UtcNow)
                return (false, "We cannot refresh this since the token has not expired", "");

            // expiry jti
            string jti = claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Jti)?.Value;
            if(string.IsNullOrWhiteSpace(jti))
                return (false, "We cannot refresh this since the token has not expired", "");

            return (true, "", jti);
        }

        private bool TryTakeExpClaimsFromToken(IEnumerable<Claim> claims, out DateTime expiryDate)
        {
            expiryDate = DateTime.UtcNow.AddDays(-1);

            string claimsExp = claims.FirstOrDefault(x => x.Type == JwtRegisteredClaimNames.Exp)?.Value;
            if (string.IsNullOrWhiteSpace(claimsExp))
                return false;

            if (!long.TryParse(claimsExp, out var utcExpiryDate))
                return false;

            expiryDate = UnixTimeStampToDateTime(utcExpiryDate);
            return true;
        }

        private ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
        {
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateAudience = false,
                ValidateIssuer = false,
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtTokenConfig.Value.Secret)),
                ValidateLifetime = false
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out var securityToken);
            var jwtSecurityToken = securityToken as JwtSecurityToken;
            if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
                return null;

            return principal;
        }

        private DateTime UnixTimeStampToDateTime( double unixTimeStamp )
        {
            System.DateTime dtDateTime = new DateTime(1970,1,1,0,0,0,0,System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds( unixTimeStamp ).ToUniversalTime();
            return dtDateTime;
        }
    }
}
