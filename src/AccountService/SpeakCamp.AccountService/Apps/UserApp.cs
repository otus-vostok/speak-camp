// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SpeakCamp.AccountService.Data.Models;
using SpeakCamp.AccountService.ViewModels;

namespace SpeakCamp.AccountService.Apps
{
    public interface IUserApp
    {
        Task<ResponseBasic> Login(RequestLogin command);
        Task<ResponseBasic> Register(RequestRegister command);
        Task<ResponseBasic> ChangePassword(string email, RequestChangePassword command);
        Task<ResponseBasic> RefreshToken(RequestRefreshToken command);
        Task<ResponseBasic> Logout(string email);
    }


    public class UserApp : IUserApp
    {
        private readonly UserManager<UserModel> _userManager;
        private readonly IJwtTokenApp _jwtManager;
        private readonly IRefreshTokenApp _refreshTokenApp;

        public UserApp(UserManager<UserModel> userManager, IJwtTokenApp jwtManager, IRefreshTokenApp refreshTokenApp)
        {
            _userManager = userManager;
            _jwtManager = jwtManager;
            _refreshTokenApp = refreshTokenApp;
        }

        public async Task<ResponseBasic> Login(RequestLogin command)
        {
            var user = await _userManager.FindByNameAsync(command.Email);
            if (user != null && await _userManager.CheckPasswordAsync(user, command.Password))
            {
                var (accessToken, refreshToken) = await CreateBothTokenForUser(user);
                return new ResponseAuth(accessToken, refreshToken);
            }
            return new ResponseBasic(false);
        }

        public async Task<ResponseBasic> Logout(string email)
        {
            var user = await _userManager.FindByNameAsync(email);
            bool successful = user != null && await _refreshTokenApp.DeleteToken(user.Id);
            return new ResponseBasic(successful);
        }

        public async Task<ResponseBasic> Register(RequestRegister command)
        {
            var user = new UserModel { Email = command.Email, UserName = command.Email, Role = CustomRoles.Convert(command.Role)};
            var createResult = await _userManager.CreateAsync(user, command.Password);

            if (!createResult.Succeeded)
                return new ResponseBasic(createResult.Succeeded);

             var (accessToken, refreshToken) = await CreateBothTokenForUser(user);
             return new ResponseAuth(accessToken, refreshToken);
        }

        public async Task<ResponseBasic> ChangePassword(string email, RequestChangePassword command)
        {
            var user = await _userManager.FindByNameAsync(email);
            if (user != null && await _userManager.CheckPasswordAsync(user, command.CurrentPassword))
            {
                var result = await _userManager.ChangePasswordAsync(user, command.CurrentPassword, command.NewPassword);
                return new ResponseBasic(result.Succeeded);
            }
            return new ResponseBasic(false);
        }

        public async Task<ResponseBasic> RefreshToken(RequestRefreshToken command)
        {
            // check access token
            var resultCheckAccessToken = _jwtManager.CanBeReplaced(command.AccessToken);
            if(resultCheckAccessToken.successeful == false)
                return new ResponseWithError(resultCheckAccessToken.message);

            // check refresh token
            var resultRefreshToken = await _refreshTokenApp.CheckToken(command.RefreshToken, resultCheckAccessToken.jwtId);
            if(resultRefreshToken.successeful == false)
                return new ResponseWithError(resultRefreshToken.message);

            // generate new access  token
            var user = await _userManager.FindByIdAsync(resultRefreshToken.userId.ToString());
            var tokens = await CreateBothTokenForUser(user);

            return new ResponseAuth(tokens.accessToken, tokens.refreshToken);
        }



        private async Task<(string accessToken, string refreshToken)> CreateBothTokenForUser(UserModel user)
        {
            var newRefresh = await _refreshTokenApp.CreateToken(user.Id);
            var accessToken = _jwtManager.CreateToken(newRefresh.jwtId, user.Id.ToString(), CustomRoles.Convert(user.Role));
            return (accessToken, newRefresh.refresh);
        }
    }
}
