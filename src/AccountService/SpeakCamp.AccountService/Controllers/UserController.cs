using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpeakCamp.AccountService.Apps;
using SpeakCamp.AccountService.Data.Models;
using SpeakCamp.AccountService.ViewModels;

namespace SpeakCamp.AccountService.Controllers
{
    [ApiController]
    [Route(("[controller]"))]
    public class UserController : Controller
    {
        private readonly IUserApp _userApp;

        public UserController(IUserApp userApp)
        {
            _userApp = userApp;
        }
        
        [AllowAnonymous]
        [HttpGet("role")]
        public IActionResult GetRoles() => Json(new {roles = CustomRoles.Roles});


        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> Login(RequestLogin command)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            return Ok(await _userApp.Login(command));
        }
        
        
        [AllowAnonymous]
        [HttpPost("register")]
        public async Task<IActionResult> Register(RequestRegister command)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            return Ok(await _userApp.Register(command));
        }
        
        [HttpPost("refreshToken")]
        public async Task<IActionResult> RefreshToken(RequestRefreshToken command)
        {
            if(!ModelState.IsValid)
                return BadRequest();
            
            return Ok(await  _userApp.RefreshToken(command));
        }
        
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("logout")]
        public async Task<IActionResult> Logout()
        {
            if (!ModelState.IsValid || !HasClaimsEmail(out var email))
                return BadRequest(ModelState);
            
            return Ok(await _userApp.Logout(email));
        }

        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpPost("changePassword")]
        public async Task<IActionResult> ChangePassword(RequestChangePassword command)
        {
            if (!ModelState.IsValid || !HasClaimsEmail(out var email))
                return BadRequest(ModelState);

            return Ok(await _userApp.ChangePassword(email, command));
        }

        
        [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        [HttpGet("private")]
        public IActionResult GetPrivateData() => Ok("private data");


        private bool HasClaimsEmail(out string email)
        {
            email = "";
            if (HttpContext.User.Identity is not ClaimsIdentity identity)
                return false;
            
            email = identity.FindFirst(ClaimTypes.Name)?.Value;
            return !string.IsNullOrWhiteSpace(email);
        }
    }
}