// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

namespace SpeakCamp.AccountService.ViewModels
{
    public class ResponseWithError : ResponseBasic
    {
        public string Message { get; set; }

        public ResponseWithError(string message) : base(false)
        {
            Message = message;
        }
    }
}