// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.ComponentModel.DataAnnotations;
using SpeakCamp.AccountService.Data.Models;

namespace SpeakCamp.AccountService.ViewModels
{
    public class RequestRegister
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
 
        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        
        [Role]
        [Display(Name = "Role")]
        public string Role { get; set; }
    }
    
    
    public class RoleAttribute : RequiredAttribute
    {
        public override bool IsValid(object value) 
            => base.IsValid(value) && value is string and (CustomRoles.Student or CustomRoles.Tutor);
    }
}