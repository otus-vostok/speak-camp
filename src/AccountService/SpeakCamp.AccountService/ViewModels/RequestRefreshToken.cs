// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.ComponentModel.DataAnnotations;

namespace SpeakCamp.AccountService.ViewModels
{
    public class RequestRefreshToken
    {
        
        [Required]
        public string RefreshToken { get; set; }

        [Required]
        public string AccessToken { get; set; }
    }
}