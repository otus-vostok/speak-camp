// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

namespace SpeakCamp.AccountService.ViewModels
{
    public class ResponseAuth : ResponseBasic
    {
        
        public string AccessToken { get; set; }
        public string RefreshToken { get; set; }
        

        public ResponseAuth(string tokenAccess, string tokenRefresh) : base(!string.IsNullOrEmpty(tokenAccess) && !string.IsNullOrEmpty(tokenRefresh))
        {
            AccessToken = tokenAccess;
            RefreshToken = tokenRefresh;
        }

    }
}