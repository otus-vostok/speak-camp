// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System.Text.Json.Serialization;

namespace SpeakCamp.AccountService
{
    public class JwtSettings
    {
        
        [JsonPropertyName("secret")]
        public string Secret { get; set; }

        [JsonPropertyName("issuer")]
        public string Issuer { get; set; }

        [JsonPropertyName("audience")]
        public string Audience { get; set; }

        [JsonPropertyName("accessTokenExpiration")]
        public int AccessTokenExpiration { get; set; }

        [JsonPropertyName("refreshTokenExpirationHours")]
        public int RefreshTokenExpirationHours { get; set; }

        [JsonPropertyName("authDomain")]
        public string AuthDomain { get; set; }
    }
}