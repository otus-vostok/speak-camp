using System.Threading.Tasks;

namespace SpeakCamp.AccountService.Data.DataSeeders
{
    public interface IDataSeeder
    {
        public Task Seed();

    }
}
