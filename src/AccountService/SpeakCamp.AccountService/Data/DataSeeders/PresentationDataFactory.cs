using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Identity;
using SpeakCamp.AccountService.Data.Models;

namespace SpeakCamp.AccountService.Data.DataSeeders
{
    public static class PresentationDataFactory
    {


        public static IEnumerable<UserModel> GetUsersAndPasswords()
        {
            for (var i = 1; i < 12 + 1; i++)
                yield return new UserModel {UserName = $"user{i}@test.ru", Role = i < 5 ? UserModelRole.Tutor : UserModelRole.Student};
        }

        public static string GetMasterPassword() => "123";

        public static RefreshTokenModel GetRefreshToken(UserModel userModel)
            => new RefreshTokenModel { UserId = userModel.Id, Token = "token" + userModel.Id, Created = DateTime.UtcNow, Expires = DateTime.UtcNow.AddYears(1)};

    }
}
