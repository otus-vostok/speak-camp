using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SpeakCamp.AccountService.Data.Models;

namespace SpeakCamp.AccountService.Data.DataSeeders
{
    public class PresentationDataSeeder: IDataSeeder
    {
        private readonly ApplicationDbContext _dataContext;
        private readonly UserManager<UserModel> _userManager;

        public PresentationDataSeeder(ApplicationDbContext dataContext, UserManager<UserModel> userManager)
        {
            _dataContext = dataContext;
            _userManager = userManager;
        }

        public async Task Seed()
        {
            if (_dataContext.Users.Any() || _dataContext.RefreshTokens.Any())
                return;

            var users = PresentationDataFactory.GetUsersAndPasswords().ToList();
            foreach (var item in users)
                await _userManager.CreateAsync(item, PresentationDataFactory.GetMasterPassword());

            await _dataContext.AddRangeAsync(users.Select(PresentationDataFactory.GetRefreshToken).ToList());
            await _dataContext.SaveChangesAsync();


        }
    }
}
