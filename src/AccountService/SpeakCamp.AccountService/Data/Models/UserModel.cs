// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;
using Microsoft.AspNetCore.Identity;

namespace SpeakCamp.AccountService.Data.Models
{
    public class UserModel : IdentityUser<int>
    {
        public UserModelRole Role { get; set; }

        public override string ToString() => UserName;
    }
}
