using System.Collections.Generic;
using System.Linq;

namespace SpeakCamp.AccountService.Data.Models
{
    public static class CustomRoles
    {
        public const string Administrator = "Administrator";
        public const string Tutor = "Tutor";
        public const string Student = "Student";

        public static UserModelRole Convert(string role) => Roles[role];
        public static string Convert(UserModelRole role) => Roles.FirstOrDefault(r => r.Value == role).Key;

        public static readonly Dictionary<string, UserModelRole> Roles = new Dictionary<string, UserModelRole>
        {
            {Administrator, UserModelRole.Administrator},
            {Tutor, UserModelRole.Tutor},
            {Student, UserModelRole.Student},
        };
    }
    
    public enum UserModelRole
    {
        Student = 0,
        Tutor = 5,
        Administrator = 10
    }
}