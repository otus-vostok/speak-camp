// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using Microsoft.EntityFrameworkCore;
using SpeakCamp.AccountService.Data.Models;

namespace SpeakCamp.AccountService.Data
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<UserModel> Users { get; set; }
        public DbSet<RefreshTokenModel>  RefreshTokens{ get; set; }

        public ApplicationDbContext(DbContextOptions options)
            : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<UserModel>().HasIndex(t => t.Email).IsUnique();

            modelBuilder.Entity<RefreshTokenModel>().HasKey(u => u.Id);
            modelBuilder.Entity<RefreshTokenModel>().HasIndex(t => t.Token).IsUnique();
            modelBuilder.Entity<RefreshTokenModel>().HasIndex(t => t.UserId);
        }
    }
}
