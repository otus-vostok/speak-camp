﻿using System;
using MongoDB.Bson.Serialization.Attributes;

namespace SpeakCamp.ReviewService.Core.Domain
{
    /// <summary>
    /// Базовая сущность.
    /// </summary>
    public abstract class BaseEntity
    {
        /// <summary>
        /// ИД.
        /// </summary>
        [BsonId]
        public Guid Id { get; set; }

        /// <summary>
        /// Дата создания.
        /// </summary>
        public DateTime CreationDateUtc { get; set; }
    }
}
