﻿namespace SpeakCamp.ReviewService.Core.Domain
{
    public class AverageRating
    {
        public int UserId { get; set; }
        public double Value { get; set; }
        public int ReviewsCount { get; set; }
    }
}
