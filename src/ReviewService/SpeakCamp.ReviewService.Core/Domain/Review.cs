﻿namespace SpeakCamp.ReviewService.Core.Domain
{
    /// <summary>
    /// Отзыв.
    /// </summary>
    public class Review : BaseEntity
    {
        /// <summary>
        /// ИД преподавателя.
        /// </summary>
        public int TutorId { get; set; }

        /// <summary>
        /// ИД инициатора.
        /// </summary>
        public int InitiatorUserId { get; set; }

        /// <summary>
        /// Тело отзыва.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Оценка.
        /// </summary>
        public uint Rating { get; set; }
    }
}
