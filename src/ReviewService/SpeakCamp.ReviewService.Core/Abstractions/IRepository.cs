﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using SpeakCamp.ReviewService.Core.Domain;

namespace SpeakCamp.ReviewService.Core.Abstractions
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<List<T>> GetAllAsync();

        Task<T> GetByIdAsync(Guid id);

        Task<List<T>> GetRangeByIdsAsync(List<Guid> ids);

        Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate);

        Task<List<T>> GetWhere(Expression<Func<T, bool>> predicate);

        Task AddAsync(T entity);

        Task AddManyAsync(IEnumerable<T> entities);

        Task UpdateAsync(T entity);

        Task<Guid> CreateOrUpdateAsync(T entity, Expression<Func<T, bool>> findOnePredicate);

        Task DeleteAsync(T entity);
    }
}
