﻿using SpeakCamp.ReviewService.Core.Domain;

namespace SpeakCamp.ReviewService.Core.Gateways
{
    public interface IAverageRatingProviderGateway
    {
        void NotifyAverageRatingChanged(int userId, AverageRating rating);
    }
}
