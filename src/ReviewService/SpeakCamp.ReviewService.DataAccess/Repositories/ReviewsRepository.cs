﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using SpeakCamp.ReviewService.Core.Domain;

namespace SpeakCamp.ReviewService.DataAccess.Repositories
{
    public class ReviewsRepository : Repository<Review>
    {
        public ReviewsRepository(IMongoDatabase mongoDb) : base(mongoDb)
        {
        }

        // HACK. See https://jira.mongodb.org/browse/SERVER-41578.
        public override async Task<Guid> CreateOrUpdateAsync(Review entity, Expression<Func<Review, bool>> findOnePredicate)
        {
            var updateDefinition = Builders<Review>.Update
                .Set(rec => rec.Rating, entity.Rating)
                .Set(rec => rec.Body, entity.Body)
                .SetOnInsert(rec => rec.Id, entity.Id)
                .SetOnInsert(rec => rec.TutorId, entity.TutorId)
                .SetOnInsert(rec => rec.CreationDateUtc, entity.CreationDateUtc);

            var result = await Collection.FindOneAndUpdateAsync(findOnePredicate, updateDefinition,
                new FindOneAndUpdateOptions<Review> { IsUpsert = true });

            return result?.Id ?? entity.Id;
        }

        public async Task<AverageRating> GetAverageRating(int userId)
        {
            var pipeline = Collection.Aggregate()
                .Match(x => x.TutorId == userId)
                .Group(k => k.TutorId,
                    g =>
                        new
                        {
                            UserId = g.Key,
                            Value = g.Average(x => x.Rating),
                            Count = g.Count()
                        }
                )
                .Project(r => new AverageRating
                {
                    UserId = r.UserId,
                    Value = r.Value,
                    ReviewsCount = r.Count
                });

            var averageRating = await pipeline
                .FirstOrDefaultAsync();

            if (averageRating == null)
                return new AverageRating { UserId = userId };

            averageRating.Value = Math.Round(averageRating.Value, 2, MidpointRounding.AwayFromZero);

            return averageRating;
        }
    }
}
