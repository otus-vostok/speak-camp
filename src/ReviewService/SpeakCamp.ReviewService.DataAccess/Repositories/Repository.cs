﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using SpeakCamp.ReviewService.Core.Abstractions;
using SpeakCamp.ReviewService.Core.Domain;

namespace SpeakCamp.ReviewService.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T>
        where T : BaseEntity
    {
        protected readonly IMongoCollection<T> Collection;

        // ReSharper disable once MemberCanBeProtected.Global
        public Repository(IMongoDatabase mongoDb)
        {
            Collection = mongoDb.GetCollection<T>(typeof(T).Name);
        }

        public Task<List<T>> GetAllAsync()
        {
            return Collection.AsQueryable().ToListAsync();
        }

        public virtual Task<T> GetByIdAsync(Guid id)
        {
            var filter = Builders<T>.Filter.Eq(x => x.Id, id);
            return Collection.Find(filter).SingleOrDefaultAsync();
        }

        public Task<List<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            return Collection
                .AsQueryable()
                .Where(x => ids.Contains(x.Id))
                .ToListAsync();
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            return Collection
                .AsQueryable()
                .FirstOrDefaultAsync(predicate);
        }

        public Task<List<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            return Collection
                .AsQueryable()
                .Where(predicate)
                .ToListAsync();
        }

        public virtual Task AddAsync(T entity)
        {
            return Collection.InsertOneAsync(entity);
        }

        public Task AddManyAsync(IEnumerable<T> entities)
        {
            return Collection.InsertManyAsync(entities);
        }

        public Task UpdateAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(x => x.Id, entity.Id);
            return Collection.FindOneAndReplaceAsync(filter, entity);
        }

        public virtual async Task<Guid> CreateOrUpdateAsync(T entity, Expression<Func<T, bool>> findOnePredicate)
        {
            var result = await Collection.FindOneAndReplaceAsync(findOnePredicate, entity,
                new FindOneAndReplaceOptions<T> { IsUpsert = true });

            return result.Id;
        }

        public Task DeleteAsync(T entity)
        {
            var filter = Builders<T>.Filter.Eq(x => x.Id, entity.Id);
            return Collection.DeleteOneAsync(filter);
        }
    }
}
