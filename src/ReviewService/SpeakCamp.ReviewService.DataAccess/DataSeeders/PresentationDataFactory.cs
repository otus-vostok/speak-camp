using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.ReviewService.Core.Domain;

namespace SpeakCamp.ReviewService.DataAccess.DataSeeders
{
    public static class PresentationDataFactory
    {



        const string ReviewBody = "Tutor review is copy that aims to tell your potential attendees what will be happening at the event, who will be speaking, and what they will get out of attending. Good event descriptions can drive attendance to events and also lead to more media coverage.";
        const string ReviewBodyAlt = "Excellent tutor review is copy that aims to tell your potential attendees what will be happening at the event, who will be speaking, and what they will get out of attending.";


        public static List<Review> GetReviews()
        {

            return new List<Review>()
            {
                new Review(){CreationDateUtc = DateTime.UtcNow.AddDays(-5), Body=ReviewBody, Rating=4, InitiatorUserId=6, TutorId=1},
                new Review(){CreationDateUtc = DateTime.UtcNow.AddDays(-15), Body=ReviewBodyAlt, Rating=5, InitiatorUserId=7, TutorId=1},
                new Review(){CreationDateUtc = DateTime.UtcNow.AddDays(-5), Body=ReviewBody, Rating=4, InitiatorUserId=6, TutorId=2},
                new Review(){CreationDateUtc = DateTime.UtcNow.AddDays(-20), Body=ReviewBodyAlt, Rating=4, InitiatorUserId=8, TutorId=2},
                new Review(){CreationDateUtc = DateTime.UtcNow.AddDays(-3), Body=ReviewBody, Rating=4, InitiatorUserId=8, TutorId=3},
                new Review(){CreationDateUtc = DateTime.UtcNow.AddDays(-12), Body=ReviewBodyAlt, Rating=3, InitiatorUserId=9, TutorId=3},
                new Review(){CreationDateUtc = DateTime.UtcNow.AddDays(-10), Body=ReviewBody, Rating=5, InitiatorUserId=10, TutorId=4},
            };
         }

    }
}
