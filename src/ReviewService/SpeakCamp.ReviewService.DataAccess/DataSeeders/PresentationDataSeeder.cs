using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.ReviewService.DataAccess.Repositories;

namespace SpeakCamp.ReviewService.DataAccess.DataSeeders
{
    public class PresentationDataSeeder : IDataSeeder
    {
        private readonly ReviewsRepository _reviewRepository;

        public PresentationDataSeeder(ReviewsRepository reviewRepository)
        {
            _reviewRepository = reviewRepository;
        }

        public async Task Seed()
        {
            if (await _reviewRepository.GetFirstWhere(review => true) != null)
                return;

            await _reviewRepository.AddManyAsync(PresentationDataFactory.GetReviews());

        }



    }
}
