using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpeakCamp.ReviewService.Core.Domain;
using SpeakCamp.ReviewService.Core.Gateways;
using SpeakCamp.ReviewService.DataAccess.Repositories;
using CreateReviewRequest = SpeakCamp.ReviewService.Api.Models.CreateReviewRequest;

namespace SpeakCamp.ReviewService.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ReviewsController : BaseController
    {
        private readonly ReviewsRepository _reviewRepository;
        private readonly IAverageRatingProviderGateway _averageRatingProviderGateway;

        public ReviewsController(ReviewsRepository reviewRepository, IAverageRatingProviderGateway averageRatingProviderGateway)
        {
            _reviewRepository = reviewRepository;
            _averageRatingProviderGateway = averageRatingProviderGateway;
        }

        [AllowAnonymous]
        [HttpGet("{id:int}")]
        public Task<List<Review>> GetAsync(int id)
        {
            return _reviewRepository.GetWhere(t => t.TutorId == id);
        }

        [AllowAnonymous]
        [HttpGet("ratings/{id:int}")]
        public Task<AverageRating> GetUserRatingAsync(int id)
        {
            return _reviewRepository.GetAverageRating(id);
        }

        [HttpPost]
        public async Task<Guid> CreateAsync([FromBody]CreateReviewRequest request)
        {
            var review = new Review
            {
                Id = Guid.NewGuid(),
                TutorId = request.UserId,
                InitiatorUserId = request.InitiatorId,
                Body = request.Body,
                Rating = request.Rating,
                CreationDateUtc = DateTime.UtcNow
            };

            await _reviewRepository.AddAsync(review);

            var rating = await _reviewRepository.GetAverageRating(request.UserId);

            _averageRatingProviderGateway.NotifyAverageRatingChanged(request.UserId, rating);

            return review.Id;
        }
    }
}
