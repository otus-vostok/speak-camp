﻿namespace SpeakCamp.ReviewService.Api.Models
{
    public class CreateReviewRequest
    {
        public int InitiatorId { get; set; }
        public int UserId { get; set; }
        public string Body { get; set; }
        public uint Rating { get; set; }
    }
}
