using System;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Driver;
using NSwag;
using NSwag.Generation.Processors.Security;
using SpeakCamp.ReviewService.Core.Abstractions;
using SpeakCamp.ReviewService.Core.Gateways;
using SpeakCamp.ReviewService.DataAccess.DataSeeders;
using SpeakCamp.ReviewService.DataAccess.Repositories;
using SpeakCamp.ReviewService.Integration;

namespace SpeakCamp.ReviewService.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers();

            services
                .AddSingleton<IMongoClient>(_ =>
                    new MongoClient(_configuration["ReviewServiceDb:ConnectionString"]))
                .AddSingleton(serviceProvider =>
                    serviceProvider.GetRequiredService<IMongoClient>()
                        .GetDatabase(_configuration["ReviewServiceDb:Database"]));

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<ReviewsRepository>();
            services.AddScoped<IDataSeeder, PresentationDataSeeder>();

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;

                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateLifetime = true,
                        ValidateAudience = false,
                        ValidIssuer = _configuration.GetSection("JwtSettings:issuer").Value,
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(_configuration.GetSection("JwtSettings:secret").Value)),
                        ClockSkew = TimeSpan.Zero
                    };
                });

            services.Configure<RabbitMqConfiguration>(_configuration.GetSection("RabbitMqConfiguration"));
            services.AddSingleton<IAverageRatingProviderGateway, AverageRatingProviderGateway>();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "SpeakCamp ReviewService API";
                options.Version = "1.0";

                options.AddSecurity("Bearer", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.Http,
                    Scheme = JwtBearerDefaults.AuthenticationScheme,
                    BearerFormat = "JWT",
                    Description = "Type into the textbox: {your JWT token}."
                });

                options.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("Bearer"));
            });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IDataSeeder dataSeeder)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseOpenApi();
            app.UseSwaggerUi3(x => { x.DocExpansion = "list"; });

            app.UseRouting();

            app.UseCors(x => x
             .AllowAnyMethod()
             .AllowAnyHeader()
             .SetIsOriginAllowed(origin => true) // allow any origin
             .AllowCredentials()); // allow credentials

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireAuthorization();
            });

            dataSeeder.Seed().Wait();
        }
    }
}
