﻿namespace SpeakCamp.ReviewService.Integration
{
    public class RabbitMqConfiguration
    {
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string RatingsQueue { get; set; }
    }
}
