﻿using System;
using System.Text;
using System.Text.Json;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using SpeakCamp.ReviewService.Core.Domain;
using SpeakCamp.ReviewService.Core.Gateways;

namespace SpeakCamp.ReviewService.Integration
{
    public class AverageRatingProviderGateway : IAverageRatingProviderGateway
    {
        private readonly IOptions<RabbitMqConfiguration> _rabbitMqConfiguration;
        private IConnection _connection;

        public AverageRatingProviderGateway(IOptions<RabbitMqConfiguration> rabbitMqOptions)
        {
            _rabbitMqConfiguration = rabbitMqOptions;
            CreateConnection();
        }

        public void NotifyAverageRatingChanged(int userId, AverageRating rating)
        {
            if (!ConnectionExists())
                return;

            using var channel = _connection.CreateModel();

            channel.QueueDeclarePassive(_rabbitMqConfiguration.Value.RatingsQueue);

            var json = JsonSerializer.Serialize(rating);
            var body = Encoding.UTF8.GetBytes(json);

            channel.BasicPublish(
                exchange: string.Empty,
                routingKey: _rabbitMqConfiguration.Value.RatingsQueue,
                basicProperties: null,
                body: body);
        }

        private void CreateConnection()
        {
            try
            {
                var factory = new ConnectionFactory
                {
                    HostName = _rabbitMqConfiguration.Value.HostName,
                    UserName = _rabbitMqConfiguration.Value.UserName,
                    Password = _rabbitMqConfiguration.Value.Password
                };

                _connection = factory.CreateConnection();
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private bool ConnectionExists()
        {
            if (_connection != null)
                return true;

            CreateConnection();

            return _connection != null;
        }
    }
}
