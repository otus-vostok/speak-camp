В командах ниже есть необходимость, если кому-то надо создавать образы и запускать контейнеры отдельно, не используя docker-compose.

### Создаем виртуальную сеть для главного микросервиса и его БД
docker network create main-service-network

### Запускаем контейнер БД postgre
docker run -d --name main-service-db --net main-service-network -e POSTGRES_PASSWORD=postgres -e POSTGRES_USER=postgres -p 5432:5432 postgres:13.3

### Подключение к этой же сети из Visual Studio происходит при помощи параметра в файле проекта (например, для дебага)
<DockerfileRunArguments>--net main-service-network</DockerfileRunArguments>

### Сборка образа главного микросервиса из директории src
docker build -f .\MainService\MainService.Api\Dockerfile --force-rm -t main-service-api:1.0 .

### Запуск главного микросервиса
docker run -d --name main-service-api --net main-service-network -p 5000:5000 main-service-api:1.0