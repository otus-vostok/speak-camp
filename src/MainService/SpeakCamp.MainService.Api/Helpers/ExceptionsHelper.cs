using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Exceptions;
using System.Net;

namespace SpeakCamp.MainService.Api.Helpers
{
    public static class ExceptionsHelper
    {
        public static Dictionary<Type, HttpStatusCode> ExceptionsHttpStatusCodes
            => new()
            {
                [typeof(BusinessException)] = HttpStatusCode.BadRequest,
                [typeof(ValidationException)] = HttpStatusCode.UnprocessableEntity,
                [typeof(EntityNotFoundException)] = HttpStatusCode.NotFound
            };
    }
}
