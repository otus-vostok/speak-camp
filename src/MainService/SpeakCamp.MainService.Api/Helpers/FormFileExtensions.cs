using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace SpeakCamp.MainService.Api.Helpers
{
    /// <summary>
    /// Расширения для удобства работы с объектом, хранящим в себе файлы на Web
    /// </summary>
    public static class FormFileExtensions
    {
        public static async Task<byte[]> FormFileToByteArrayAsync(this IFormFile file)
        {
            if (file.Length <= 0) throw new FormatException("Sending empty file");

            using var ms = new MemoryStream();
            await file.CopyToAsync(ms);
            return ms.ToArray();
        }
    }
}
