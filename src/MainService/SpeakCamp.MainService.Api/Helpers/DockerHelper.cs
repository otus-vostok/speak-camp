﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Api.Helpers {
    public static class DockerHelper {

        /// <summary>
        /// Выясняем, выполняется ли приложение в контейнере.
        /// В перспективе предлагается добавлять собственную переменную окружения в Dockerfile,
        /// на случай если разработчики сторонних библиотек переименуют или удалят сущетствующую.
        /// </summary>
        public static bool IN_DOCKER => 
            Environment.GetEnvironmentVariable("DOTNET_RUNNING_IN_CONTAINER") == "true";

    }
}
