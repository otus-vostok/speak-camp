using System;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;
using SpeakCamp.MainService.Api.Models;
using SpeakCamp.MainService.Application.Users.UpdateUserRating;

namespace SpeakCamp.MainService.Api.Services
{

    public class ListeningReviewsService : IHostedService
    {
        private IConnection _connection;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private IModel _channel;
        private readonly ILogger _logger;
        private readonly string _queueName;
        private readonly IOptions<RabbitMqConfiguration> _configuration;

        public ListeningReviewsService(IOptions<RabbitMqConfiguration> configuration,
            ILogger<ListeningReviewsService> logger, IServiceScopeFactory serviceScopeFactory)
        {
            _queueName = configuration.Value.QueueName;
            _configuration = configuration;
            _logger = logger;
            _serviceScopeFactory = serviceScopeFactory;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var factory = new ConnectionFactory()
            {
                HostName = _configuration.Value.HostName,
                UserName = _configuration.Value.UserName,
                Password = _configuration.Value.Password,
                Port = _configuration.Value.Port,
                VirtualHost = _configuration.Value.VirtualHost,
                AutomaticRecoveryEnabled = true,
                NetworkRecoveryInterval = TimeSpan.FromSeconds(15),
                DispatchConsumersAsync = true
            };

            for(var i = 0; i < 3; i++)
            {
                try
                {
                    _connection = factory.CreateConnection();
                    break;
                }
                catch (BrokerUnreachableException)
                {
                    Console.WriteLine($"RabbitMq not ready. Wait {i}...");
                    await Task.Delay(TimeSpan.FromSeconds(10), cancellationToken);
                }
            }

            _channel = _connection.CreateModel();

            Register(_queueName);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            this._connection.Close();
            return Task.CompletedTask;
        }

        private void Register(string queueName)
        {
            _logger.LogInformation($"RabbitListener register(listen), queueName: {queueName}");
            _channel.QueueDeclare(queue:queueName, true, false, false, null);
            var consumer = new AsyncEventingBasicConsumer(_channel);
            consumer.Received += ConsumerOnReceived;
            _channel.BasicConsume(queue: queueName, consumer: consumer, autoAck: true);
        }

        private async Task ConsumerOnReceived(object sender, BasicDeliverEventArgs e)
        {
            try
            {
                if(!TryGetObjectFromMessage(e, out var request))
                    throw new Exception("Unknown command");

                await ActionWithRequest(request);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Received");
            }
        }

        private async Task ActionWithRequest(AverageRatingDto averageRatingDto)
        {
            using var scope = _serviceScopeFactory.CreateScope();
            var mapper = scope.ServiceProvider.GetService<IMapper>();
            var mediator = scope.ServiceProvider.GetService<IMediator>();

            var command = mapper.Map<UpdateUserRatingCommand>(averageRatingDto);
            await mediator.Send(command);
        }

        private bool TryGetObjectFromMessage(BasicDeliverEventArgs e, out AverageRatingDto ratingDto)
        {
            ratingDto = null;

            var requestMessage = Encoding.UTF8.GetString(e.Body.ToArray());

            if (string.IsNullOrEmpty(requestMessage))
                return false;

            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true
            };

            ratingDto = JsonSerializer.Deserialize<AverageRatingDto>(requestMessage, options);

            return ratingDto?.Value >= 0;
        }
    }
}
