using System;
using System.Linq;
using System.Text;
using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using NSwag;
using NSwag.Generation.Processors.Security;
using SpeakCamp.MainService.Api.Helpers;
using SpeakCamp.MainService.Api.Middlewares;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Common.Behaviors;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate;
using SpeakCamp.MainService.Infrastructure.Caching;
using SpeakCamp.MainService.Infrastructure.DataAccess;
using SpeakCamp.MainService.Infrastructure.DataAccess.DataSeeders;
using SpeakCamp.MainService.Infrastructure.DataAccess.Repositories;
using SpeakCamp.MainService.Infrastructure.ImageProcess;
using SpeakCamp.MainService.Api.RealtimeActions.Hubs;
using SpeakCamp.MainService.Api.RealtimeActions;
using SpeakCamp.MainService.Api.Services;
using SpeakCamp.MainService.Application.Gateways;
using SpeakCamp.MainService.Integration;

namespace SpeakCamp.MainService.Api
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddControllers().AddMvcOptions(x =>
                x.SuppressAsyncSuffixInActionNames = false);

            services.AddMediatR(typeof(BaseEntity).Assembly, typeof(Startup).Assembly);
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(PipelineWithValidationCommandBehavior<,>));
            services.AddTransient<GlobalExceptionHandlerMiddleware>();

            services.AddAutoMapper(typeof(Startup));
            services.AddValidatorsFromAssemblyContaining(typeof(BaseEntity));

            services.AddDbContext<MainDbContext>(x =>
            {
                var connectionStringName = DockerHelper.IN_DOCKER ? "MainDbDocker" : "MainDb";
                x.UseNpgsql(_configuration.GetConnectionString(connectionStringName));
                x.UseSnakeCaseNamingConvention();
            });

            services.AddImageServices();
            services.AddCachingServices();
            services.AddDataAccessServices();
            services.AddScoped(typeof(IMeetingModelFactory), typeof(MeetingModelFactory));

            services.AddHeaderPropagation(o =>
            {
                o.Headers.Add("Authorization");
            });

            services.AddHttpClient<IReviewGateway, ReviewGateway>(c =>
                    c.BaseAddress = new Uri(_configuration.GetSection("IntegrationSettings:ReviewServiceApiUrl").Value)
                ).AddHeaderPropagation();

            services.AddSignalR();

            services.AddOpenApiDocument(options =>
            {
                options.Title = "SpeakCamp MainService API";
                options.Version = "1.0";

                options.AddSecurity("Bearer", Enumerable.Empty<string>(), new OpenApiSecurityScheme
                {
                    Type = OpenApiSecuritySchemeType.Http,
                    Scheme = JwtBearerDefaults.AuthenticationScheme,
                    BearerFormat = "JWT",
                    Description = "Type into the textbox: {your JWT token}."
                });

                options.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("Bearer"));
            });

            services.AddAuthentication(x =>
                {
                    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    x.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;

                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateLifetime = true,
                        ValidateAudience = false,
                        ValidIssuer = _configuration.GetSection("JwtSettings:issuer").Value,
                        IssuerSigningKey = new SymmetricSecurityKey(
                            Encoding.UTF8.GetBytes(_configuration.GetSection("JwtSettings:secret").Value)),
                        ClockSkew = TimeSpan.Zero
                    };
                });

            services.Configure<RabbitMqConfiguration>(_configuration.GetSection("RabbitMqConfiguration"));
            services.AddHostedService<ListeningReviewsService>();
        }

        public void Configure(IApplicationBuilder app, MainDbContext context, IDataSeeder dataSeeder)
        {
            app.UseMiddleware<GlobalExceptionHandlerMiddleware>();
            app.UseOpenApi();
            app.UseSwaggerUi3(x => {
                x.DocExpansion = "list";
            });

            app.UseRouting();

            app.UseCors(x => x
               .AllowAnyMethod()
               .AllowAnyHeader()
               .SetIsOriginAllowed(origin => true) // allow any origin
               .AllowCredentials()); // allow credentials

            app.UseAuthentication();
            app.UseAuthorization();
            app.UseHeaderPropagation();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers().RequireAuthorization();
                endpoints.MapHub<UsersAmountInformationHub>("/api/v1/users/amount");
            });

            context.Database.Migrate();
            dataSeeder.Seed();
        }
    }
}
