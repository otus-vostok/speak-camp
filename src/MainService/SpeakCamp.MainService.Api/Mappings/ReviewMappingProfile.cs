﻿using AutoMapper;
using SpeakCamp.MainService.Api.Models;
using SpeakCamp.MainService.Application.Reviews.CreateReview;

namespace SpeakCamp.MainService.Api.Mappings
{
    public class ReviewMappingProfile : Profile
    {
        public ReviewMappingProfile()
        {
            CreateMap<CreateReviewRequest, CreateReviewCommand>();
        }
    }
}
