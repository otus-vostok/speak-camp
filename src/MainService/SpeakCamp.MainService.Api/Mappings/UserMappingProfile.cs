using AutoMapper;
using SpeakCamp.MainService.Api.Models;
using SpeakCamp.MainService.Application.Users.CreateUser;
using SpeakCamp.MainService.Application.Users.GetUsers;
using SpeakCamp.MainService.Application.Users;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Languages;
using SpeakCamp.MainService.Application.Users.UpdateUser;

namespace SpeakCamp.MainService.Api.Mappings {
    
    public class UserMappingProfile : Profile {

        public UserMappingProfile() {
            CreateMap<CreateUserRequest, CreateUserCommand>();
            CreateMap<UpdateUserRequest, UpdateUserCommand>();
            CreateMap<GetUsersQuery, UserSearchParams>();
            CreateMap<User, UserDto>();
        }
    }
}
