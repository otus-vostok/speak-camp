using AutoMapper;
using SpeakCamp.MainService.Application.Languages;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Api.Mappings {
    public class LanguageMappingProfile : Profile {

        public LanguageMappingProfile() {
            CreateMap<Language, LanguageDto>();
        }

    }
}
