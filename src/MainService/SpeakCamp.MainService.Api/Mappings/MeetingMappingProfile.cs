using AutoMapper;
using SpeakCamp.MainService.Api.Models;
using System;
using SpeakCamp.MainService.Application.Meetings;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.CreateMeeting;
using SpeakCamp.MainService.Application.Meetings.GetUserMeetings;
using SpeakCamp.MainService.Application.Meetings.StartMeeting;
using SpeakCamp.MainService.Application.Meetings.GetMeetingDetails;
using SpeakCamp.MainService.Application.Meetings.GetMeetings;

namespace SpeakCamp.MainService.Api.Mappings
{
    public class MeetingMappingProfile : Profile
    {
        public MeetingMappingProfile()
        {
            CreateMap<CreateMeetingRequest, CreateMeetingCommand>()
                .ForMember(dest => dest.StartDateUtc,
                    opt => opt.MapFrom(src => src.StartDate.ToUniversalTime()));

            CreateMap<UserMeetingsRequest, GetUserMeetingsQuery>()
             .ForMember(dest => dest.States,
                         opt => opt.MapFrom(src => src.States == null ? null : src.States.Split(',', StringSplitOptions.RemoveEmptyEntries)))
             .ForMember(dest => dest.Page,
                         opt => opt.NullSubstitute(1))
             .ForMember(dest => dest.PageSize,
                         opt => opt.NullSubstitute(20));

            CreateMap<MeetingsRequest, GetMeetingsQuery>()
              .ForMember(dest => dest.DateFrom,
                    opt => opt.MapFrom(src => src.DateFrom == null ? (DateTime?)null :  src.DateFrom.Value.ToUniversalTime()))
              .ForMember(dest => dest.DateTo,
                    opt => opt.MapFrom(src => src.DateTo == null ? (DateTime?)null : src.DateTo.Value.ToUniversalTime()))
             .ForMember(dest => dest.States,
                       opt => opt.MapFrom(src => src.States == null ? null : src.States.Split(',', StringSplitOptions.RemoveEmptyEntries)))
             .ForMember(dest => dest.Page,
                       opt => opt.NullSubstitute(1))
             .ForMember(dest => dest.PageSize,
                       opt => opt.NullSubstitute(20));

            CreateMap<GetUserMeetingsQuery, MeetingSearchParams>();
            CreateMap<GetMeetingsQuery, MeetingSearchParams>();
            CreateMap<StartMeetingRequest, StartMeetingCommand>();
            CreateMap<Meeting, MeetingDto>()
                .ForMember(dest => dest.StartDate,
                    opt => opt.MapFrom(src => src.StartDate.ToString("yyyy-MM-ddThh:mm:ssZ")));
            CreateMap<Meeting, MeetingDetailsDto>()
                .ForMember(dest => dest.StartDate,
                    opt => opt.MapFrom(src => src.StartDate.ToString("yyyy-MM-ddThh:mm:ssZ")));

        }
    }
}
