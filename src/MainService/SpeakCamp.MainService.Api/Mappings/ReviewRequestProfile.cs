using System;
using AutoMapper;
using SpeakCamp.MainService.Api.Models;
using SpeakCamp.MainService.Application.Users.UpdateUserRating;

namespace SpeakCamp.MainService.Api.Mappings {
    public class ReviewRequestProfile : Profile {

        public ReviewRequestProfile()
        {
            CreateMap<AverageRatingDto, UpdateUserRatingCommand>()
                .ForMember(d => d.Rating, d => d.MapFrom(m => Convert.ToDecimal(m.Value)));
        }
    }
}
