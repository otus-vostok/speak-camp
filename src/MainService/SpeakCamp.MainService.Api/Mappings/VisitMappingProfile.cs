using AutoMapper;
using SpeakCamp.MainService.Api.Models;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Meetings.GetMeetingDetails;
using SpeakCamp.MainService.Application.Visits;
using SpeakCamp.MainService.Application.Visits.CreateVisit;
using SpeakCamp.MainService.Application.Visits.GetUserVisits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Api.Mappings
{
    public class VisitMappingProfile : Profile
    {
        public VisitMappingProfile() { 

            CreateMap<CreateVisitRequest, CreateVisitCommand>();
            CreateMap<UserVisitsRequest, GetUserVisitsQuery>()
              .ForMember(dest => dest.States,
                          opt => opt.MapFrom(src => src.States == null ? null : src.States.Split(',', StringSplitOptions.RemoveEmptyEntries)))
              .ForMember(dest => dest.Page,
                          opt => opt.NullSubstitute(1))
              .ForMember(dest => dest.PageSize,
                          opt => opt.NullSubstitute(20)); 
            CreateMap<GetUserVisitsQuery, VisitSearchParams>();
            CreateMap<Visit, MeetingVisitDto>();
            CreateMap<Visit, VisitDto>();
            
          
        }
    }
}
