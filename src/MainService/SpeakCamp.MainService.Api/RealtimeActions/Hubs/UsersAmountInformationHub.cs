using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Users;
using MediatR;
using SpeakCamp.MainService.Api.RealtimeActions.Events;

namespace SpeakCamp.MainService.Api.RealtimeActions.Hubs
{
    public class UsersAmountInformationHub : Hub<IUsersAmountInformationClient>
    {
        private readonly IMediator _mediator;

        public UsersAmountInformationHub(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Task InitialInformationRequest()
        {
            return _mediator.Publish(new UsersAmountRequestedEvent());
        }
    }
}
