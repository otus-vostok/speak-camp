using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Users;
using Microsoft.AspNetCore.SignalR;
using SpeakCamp.MainService.Api.RealtimeActions.Hubs;

namespace SpeakCamp.MainService.Api.RealtimeActions
{
    public class UsersAmountInformationSender
    {
        private readonly IHubContext<UsersAmountInformationHub, IUsersAmountInformationClient> _hubContext;

        public UsersAmountInformationSender(IHubContext<UsersAmountInformationHub, IUsersAmountInformationClient> hubContext)
        {
            _hubContext = hubContext;
        }

        public async Task SendUsersAmount(UsersAmountInformation message)
        {
            await _hubContext.Clients.All.ReceiveUsersAmount(message);
        }
    }
}
