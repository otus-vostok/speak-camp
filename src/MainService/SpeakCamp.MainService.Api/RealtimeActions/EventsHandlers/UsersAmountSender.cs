using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Users;
using SpeakCamp.MainService.Application.Common.Events;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.SignalR;
using SpeakCamp.MainService.Api.RealtimeActions.Hubs;
using SpeakCamp.MainService.Api.RealtimeActions.Events;

namespace SpeakCamp.MainService.Api.RealtimeActions.EventsHandlers
{
    public class UsersAmountSender : INotificationHandler<UserCreatedEvent>, INotificationHandler<UsersAmountRequestedEvent>
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;

        private readonly IHubContext<UsersAmountInformationHub, IUsersAmountInformationClient> _hubContext;

        public UsersAmountSender(
            IServiceScopeFactory serviceScopeFactory,
            IHubContext<UsersAmountInformationHub, IUsersAmountInformationClient> hubContext)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _hubContext = hubContext;
        }

        public async Task Handle(UserCreatedEvent notification, CancellationToken cancellationToken)
        {
            await SendInformation();
        }

        public async Task Handle(UsersAmountRequestedEvent notification, CancellationToken cancellationToken)
        {
           await SendInformation();
        }

        private async Task SendInformation()
        {
            using var scope = _serviceScopeFactory.CreateScope();

            var usersRepository = scope.ServiceProvider.GetRequiredService<IUsersRepository>();

            var typesAmount = await usersRepository.GetTypesCount();

            var information = new UsersAmountInformation
            {
                StudentsAmount = typesAmount.FirstOrDefault(x => x.userType == UserTypes.Student).amount,
                TutorsAmount = typesAmount.FirstOrDefault(x => x.userType == UserTypes.Tutor).amount,
            };

            await _hubContext.Clients.All.ReceiveUsersAmount(information);
        }
    }
}
