using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Users;

namespace SpeakCamp.MainService.Api.RealtimeActions
{
    public interface IUsersAmountInformationClient
    {
        Task ReceiveUsersAmount(UsersAmountInformation message);
    }
}
