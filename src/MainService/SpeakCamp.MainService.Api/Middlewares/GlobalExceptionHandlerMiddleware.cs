using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using static SpeakCamp.MainService.Api.Helpers.ExceptionsHelper;
using SpeakCamp.MainService.Api.Models;
using Microsoft.Extensions.Logging;

namespace SpeakCamp.MainService.Api.Middlewares
{
    public class GlobalExceptionHandlerMiddleware : IMiddleware
    {
        private readonly ILogger<GlobalExceptionHandlerMiddleware> _logger;

        public GlobalExceptionHandlerMiddleware(ILogger<GlobalExceptionHandlerMiddleware> logger)
        {
            _logger = logger;
        }

        public async Task InvokeAsync(HttpContext httpContext, RequestDelegate next)
        {
            try
            {
                await next(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError($"Something went wrong: {ex.Message}");
                await HandleExceptionAsync(httpContext, ex);
            }
        }

        private static async Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            context.Response.ContentType = "application/json";

            if (!ExceptionsHttpStatusCodes.TryGetValue(exception.GetType(), out var statusCode))
            {
                statusCode = HttpStatusCode.InternalServerError;
            }

            context.Response.StatusCode = (int) statusCode;
            await context.Response.WriteAsync(
                new ErrorResponse()
                {
                    StatusCode = (int) statusCode,
                    StatusDescription = statusCode.ToString(),
                    Message = exception.Message
                }
                .ToString());
        }
    }
}
