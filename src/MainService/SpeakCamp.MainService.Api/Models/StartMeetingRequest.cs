using System;
using FluentValidation;

namespace SpeakCamp.MainService.Api.Models
{
    public class StartMeetingRequest
    {
       
        public string ConferenceLink { get; set; }
        public string Note { get; set; }
    }

    
}
