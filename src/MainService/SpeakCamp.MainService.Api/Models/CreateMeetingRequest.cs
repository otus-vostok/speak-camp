using System;
using FluentValidation;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Api.Models
{
    public class CreateMeetingRequest
    {
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public int DurationMinutes { get; set; }
        public string LanguageKey { get; set; }
        public string LanguageLevel { get; set; }
        public int PlacesLimit { get; set; }
        public string Description { get; set; }
        public Guid? PhotoKey { get; set; }
    }

   
}
