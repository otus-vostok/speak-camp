using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace SpeakCamp.MainService.Api.Models
{
    public class AddImageRequest
    {
        public string ImageType { get; set; }
        public IFormFile File { get; set; }
    }
}
