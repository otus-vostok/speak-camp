﻿
namespace SpeakCamp.MainService.Api.Models
{
    public class CreateReviewRequest
    {
        public int UserId { get; set; }
        public string Body { get; set; }
        public uint Rating { get; set; }
    }
}
