using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Api.Models
{
    public class ErrorResponse
    {
        public int StatusCode { get; init; }
        public string StatusDescription { get; init; }
        public string Message { get; init; }

        public override string ToString() => JsonSerializer.Serialize(this);
    }
}
