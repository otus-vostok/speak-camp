using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Api.Models
{
    public class MeetingsRequest
    {
        public string LanguageKey { get; set; }
        public string LanguageLevel { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string States { get; set; }
        public string Sorting { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }
}
