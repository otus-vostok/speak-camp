﻿
namespace SpeakCamp.MainService.Api.Models
{
    public class UpdateUserRequest
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LanguageKey { get; set; }
        public string LanguageLevel { get; set; }
        public string Description { get; set; }
        public string BirthDate { get; set; }
        public string Gender { get; set; }
        public string PhotoKey { get; set; }
        public short TimezoneOffsetMinutes { get; set; }
        public string Email { get; set; }
    }
}
