// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;

namespace SpeakCamp.MainService.Api.Models
{
    public class UpdateMeetingRequest
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public int DurationMinutes { get; set; }
        public string LanguageKey { get; set; }
        public string LanguageLevel { get; set; }
        public int PlacesLimit { get; set; }
        public string Description { get; set; }
        public Guid? PhotoKey { get; set; }
    }
}
