using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Api.Models
{
    public class UserMeetingsRequest
    {
        public string States { get; set; }
        public string Sorting { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }
    }
}
