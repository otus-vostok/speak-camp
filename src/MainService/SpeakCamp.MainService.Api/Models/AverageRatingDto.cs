namespace SpeakCamp.MainService.Api.Models
{
    public class AverageRatingDto
    {
        public int UserId { get; set; }
        public double Value  { get; set; }
        public int ReviewsCount { get; set; }
    }
}
