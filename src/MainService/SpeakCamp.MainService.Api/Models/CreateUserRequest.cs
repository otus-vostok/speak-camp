﻿
namespace SpeakCamp.MainService.Api.Models
{
    public class CreateUserRequest
    {
        public string UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LanguageKey { get; set; }
        public string LanguageLevel { get; set; }
        public short TimezoneOffsetMinutes { get; set; }
    }
}
