using SpeakCamp.MainService.Application.References;
using SpeakCamp.MainService.Application.References.GetReferences;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace SpeakCamp.MainService.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ReferencesController : BaseController
    {

        private readonly IMediator _mediator;


        public ReferencesController(IMediator mediator)
        {
            _mediator = mediator;

        }


        /// <summary>
        /// Получить список значений справочников (Enums)
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpGet]
        public async Task<ICollection<ReferenceGroupDto>> GetAsync()
        {
            return await _mediator.Send(new GetReferencesQuery() { });
        }


        }

}
