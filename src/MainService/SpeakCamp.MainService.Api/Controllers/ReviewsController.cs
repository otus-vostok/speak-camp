﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using SpeakCamp.MainService.Api.Models;
using SpeakCamp.MainService.Application.Reviews.CreateReview;

namespace SpeakCamp.MainService.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class ReviewsController : BaseController
    {
        private readonly IMapper _mapper;
        private readonly IMediator _mediator;

        public ReviewsController(IMapper mapper, IMediator mediator)
        {
            _mapper = mapper;
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult<Guid>> CreateAsync([FromBody] CreateReviewRequest request)
        {
            var command = _mapper.Map<CreateReviewCommand>(request);
            command.InitiatorAccountId = GetAccountId();

            return await _mediator.Send(command);
        }
    }
}
