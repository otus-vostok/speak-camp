using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MediatR;
using AutoMapper;
using SpeakCamp.MainService.Api.Models;
using System.IO;
using SpeakCamp.MainService.Api.Helpers;
using SpeakCamp.MainService.Application.Images.AddImage;
using SpeakCamp.MainService.Application.Images.GetImage;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using Microsoft.AspNetCore.Authorization;

namespace SpeakCamp.MainService.Api.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ImagesController : BaseController
    {
        private readonly IMediator _mediator;

        public ImagesController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult<Guid>> AddImageAsync([FromForm] AddImageRequest request)
        {
            var command = new AddImageCommand
            {
                ImageType = request.ImageType,
                File = await request.File.FormFileToByteArrayAsync(),
                ContentType = request.File.ContentType
            };

            return await _mediator.Send(command);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> GetImageAsync([FromQuery] GetImageRequest request)
        {
            var query = new GetImageQuery
            {
                Key = request.Key,
                ImageSize = request.ImageSize
            };

            var image = await _mediator.Send(query);
            return image != null ? File(image.Data, image.ContentType) : NotFound();
        }
    }
}
