﻿using AutoMapper;
using SpeakCamp.MainService.Api.Models;
using SpeakCamp.MainService.Application.Visits.CreateVisit;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Visits.DeleteVisit;

namespace SpeakCamp.MainService.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class VisitsController : BaseController
    {

        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public VisitsController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<int>> CreateAsync([FromBody] CreateVisitRequest request)
        {
            var command = _mapper.Map<CreateVisitCommand>(request);
            command.AccountId = GetAccountId();
            return await _mediator.Send(command);
        }


        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> DeleteAsync(int id)
        {

            var command = new DeleteVisitCommand()
            {   AccountId = GetAccountId(),
                VisitId = id
            };


            return await _mediator.Send(command);
        }
    }
    }
