using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using SpeakCamp.MainService.Api.Models;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Meetings;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.CreateMeeting;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.UpdateMeeting;
using SpeakCamp.MainService.Application.Meetings.StartMeeting;
using SpeakCamp.MainService.Application.Meetings.DeleteMeeting;
using SpeakCamp.MainService.Application.Meetings.GetMeetingDetails;
using SpeakCamp.MainService.Application.Meetings.GetMeetings;

namespace SpeakCamp.MainService.Api.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class MeetingsController : BaseController
    {
        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public MeetingsController(IMediator mediator, IMapper mapper)
        {
            _mediator = mediator;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<ActionResult<int>> CreateAsync([FromBody] CreateMeetingRequest request)
        {
            var command = _mapper.Map<CreateMeetingCommand>(request);
            command.AccountId = GetAccountId();
            return await _mediator.Send(command);
        }

        [HttpPut]
        public async Task<ActionResult<int>> UpdateAsync([FromBody] UpdateMeetingRequest request)
        {
            var command = _mapper.Map<UpdateMeetingCommand>(request);
            command.AccountId = GetAccountId();
            return await _mediator.Send(command);
        }

        [HttpPost("{id:int}/start")]
        public async Task<ActionResult<int>> StartAsync(int id, [FromBody] StartMeetingRequest request)
        {

            var command = _mapper.Map<StartMeetingCommand>(request);
            command.MeetingId = id;
            command.AccountId  = GetAccountId();
            return await _mediator.Send(command);
        }

        [HttpDelete("{id:int}")]
        public async Task<ActionResult<bool>> DeleteAsync(int id)
        {
            var accountId = GetAccountId();

            var command = new DeleteMeetingCommand()
            {
                AccountId = accountId,
                MeetingId = id
            };

            return await _mediator.Send(command);
        }

        [AllowAnonymous]
        [HttpGet("{id:int}")]
        public async Task<ActionResult<MeetingDetailsDto>> GetDetailsAsync(int id)
        {
            return await _mediator.Send(new GetMeetingDetailsQuery() { Id = id});
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<PagedResult<MeetingDto>>> GetMeetingsAsync([FromQuery] MeetingsRequest request)
        {
            var query = _mapper.Map<GetMeetingsQuery>(request);
            return await _mediator.Send(query);
        }
    }
}
