using System;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;

namespace SpeakCamp.MainService.Api.Controllers
{
    public class BaseController : ControllerBase
    {
        protected int GetAccountId() =>
            int.TryParse(HttpContext.User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value, out var id) ? id : throw new Exception("Auth error");
    }
}
