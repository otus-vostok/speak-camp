using Microsoft.AspNetCore.Mvc;
using MediatR;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Users.CreateUser;
using SpeakCamp.MainService.Application.Users;
using SpeakCamp.MainService.Application.Users.GetUsers;
using AutoMapper;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Api.Models;
using SpeakCamp.MainService.Application.Users.UpdateUser;
using SpeakCamp.MainService.Application.Visits;
using SpeakCamp.MainService.Application.Visits.GetUserVisits;
using SpeakCamp.MainService.Application.Meetings.GetUserMeetings;
using SpeakCamp.MainService.Application.Meetings;
using Microsoft.AspNetCore.Authorization;
using SpeakCamp.MainService.Application.Reviews.GetReviews;
using SpeakCamp.MainService.Application.Users.GetUserByAccount;

namespace SpeakCamp.MainService.Api.Controllers {

    [ApiController]
    [Route("api/v1/[controller]")]
    public class UsersController : BaseController {

        private readonly IMediator _mediator;
        private readonly IMapper _mapper;

        public UsersController(IMediator mediator, IMapper mapper) {
            _mediator = mediator;
            _mapper = mapper;
        }


        [HttpPost]
        public async Task<ActionResult<int>> CreateAsync([FromBody] CreateUserRequest request) {

            var command = _mapper.Map<CreateUserCommand>(request);
            command.AccountId = GetAccountId();
            return await _mediator.Send(command);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<PagedResult<UserDto>>> GetAsync(string userType, string states, string sorting, int page = 1, int pageSize = 20 )
        {
            var query = new GetUsersQuery
            {
                UserType = userType,
                States = states?.Split(','),
                Sorting=sorting,
                Page = page,
                PageSize = pageSize
            };
            return await _mediator.Send(query);
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<UserDetailDto>> GetDetailsAsync(int id)
        {
            var query = new GetUserDetailQuery
            {
                Id = id
            };

            return await _mediator.Send(query);
        }


        [AllowAnonymous]
        [HttpGet("byaccount/{id}")]
        public async Task<ActionResult<UserDto>> GetByAccountAsync(int id)
        {
            var query = new GetUserByAccountQuery
            {
                AccountId = id
            };

            return await _mediator.Send(query);
        }

        [HttpPut]
        public async Task<ActionResult<int>> UpdateAsync([FromBody] UpdateUserRequest request)
        {
            var command = _mapper.Map<UpdateUserCommand>(request);
            command.AccountId = GetAccountId();
            return await _mediator.Send(command);
        }

        [AllowAnonymous]
        [HttpGet("{id}/visits")]
        public async Task<ActionResult<PagedResult<VisitDto>>> GetVisitsAsync(int id, [FromQuery]UserVisitsRequest request)
        {
            var query = _mapper.Map<GetUserVisitsQuery>(request);
            query.UserId = id;

            return await _mediator.Send(query);
        }

        [AllowAnonymous]
        [HttpGet("{id}/meetings")]
        public async Task<ActionResult<PagedResult<MeetingDto>>> GetMeetingsAsync(int id, [FromQuery]UserMeetingsRequest request)
        {
            var query = _mapper.Map<GetUserMeetingsQuery>(request);
            query.UserId = id;

            return await _mediator.Send(query);
        }

        [AllowAnonymous]
        [HttpGet("{id}/reviews")]
        public async Task<ActionResult<PagedResult<ReviewDto>>> GetReviewsAsync(int id)
        {
            var query = new GetReviewsQuery
            {
                UserId = id
            };

            return await _mediator.Send(query);
        }
    }
}
