using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Languages;
using SpeakCamp.MainService.Application.Common.Abstractions;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;

namespace SpeakCamp.MainService.Api.Controllers {

    [ApiController]
    [Route("api/v1/[controller]")]
    public class LanguagesController : BaseController
    {

        private readonly IMediator _mediator;

        public LanguagesController(IMediator mediator)
        {
            _mediator = mediator;
        }


        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<ICollection<LanguageDto>>> GetAsync()
        {

            return Ok(await _mediator.Send(new GetLanguagesQuery() { }));
        }

    }
}
