using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Gateways;
using SpeakCamp.MainService.Integration.Model;

namespace SpeakCamp.MainService.Integration
{
    public class ReviewGateway : IReviewGateway
    {
        private readonly HttpClient _httpClient;

        public ReviewGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<Guid> CreateReview(int initiatorId, int userId, string body, uint rating)
        {
            var request = new CreateReviewRequest
            {
                InitiatorId = initiatorId,
                UserId = userId,
                Body = body,
                Rating = rating
            };

            var response = await _httpClient.PostAsJsonAsync("api/v1/reviews", request);

            response.EnsureSuccessStatusCode();
            var result = await response.Content.ReadAsStringAsync();

            return JsonSerializer.Deserialize<Guid>(result);
        }

        public async Task<List<Review>> GetReviews(int userId)
        {
            var options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

            return await _httpClient.GetFromJsonAsync<List<Review>>($"api/v1/reviews/{userId}", options);
        }
    }
}
