using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Migrations
{
    public partial class AlterMediaWithLinks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_medias_key",
                table: "medias");

            migrationBuilder.DropColumn(
                name: "photo_key",
                table: "users");

            migrationBuilder.DropColumn(
                name: "photo_key",
                table: "meetings");

            migrationBuilder.DropColumn(
                name: "key",
                table: "medias");

            migrationBuilder.AddColumn<Guid>(
                name: "photo_key",
                table: "users",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<Guid>(
                name: "photo_key",
                table: "meetings",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));

            migrationBuilder.AddColumn<string>(
                name: "content_type",
                table: "medias",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "key",
                table: "medias",
                type: "uuid",
                nullable: false,
                defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "photo_key",
                table: "users");

            migrationBuilder.DropColumn(
                name: "photo_key",
                table: "meetings");

            migrationBuilder.DropColumn(
                name: "content_type",
                table: "medias");

            migrationBuilder.DropColumn(
                name: "key",
                table: "medias");

            migrationBuilder.AddColumn<string>(
                name: "photo_key",
                table: "users",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "photo_key",
                table: "meetings",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "key",
                table: "medias",
                type: "character varying(255)",
                maxLength: 255,
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "ix_medias_key",
                table: "medias",
                column: "key",
                unique: true);
        }
    }
}
