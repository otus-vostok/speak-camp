using Microsoft.EntityFrameworkCore.Migrations;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Migrations
{
    public partial class LanguagesData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                insert into languages (name, key_name, creation_date) values ('English','en', current_timestamp);
                insert into languages (name, key_name, creation_date) values ('Chinese','zh', current_timestamp);
                insert into languages (name, key_name, creation_date) values ('Spanish','es', current_timestamp);
                insert into languages (name, key_name, creation_date) values ('German','de', current_timestamp);
                insert into languages (name, key_name, creation_date) values ('French','fr', current_timestamp);
                insert into languages (name, key_name, creation_date) values ('Portuguese','pt', current_timestamp);
                insert into languages (name, key_name, creation_date) values ('Russian','ru', current_timestamp);
                insert into languages (name, key_name, creation_date) values ('Japanese','ja', current_timestamp);
                insert into languages (name, key_name, creation_date) values ('Arabic','ar', current_timestamp);
                insert into languages (name, key_name, creation_date) values ('Hindi','hi', current_timestamp);
          ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"delete from languages");
        }
    }
}
