﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Migrations
{
    public partial class ReviewsCount : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "reviews_count",
                table: "users",
                type: "integer",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "reviews_count",
                table: "users");
        }
    }
}
