using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Visits;
using SpeakCamp.MainService.Infrastructure.DataAccess.Helpers;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Repositories
{
    public class VisitsRepository : Repository<Visit>, IVisitsRepository
    {
        public VisitsRepository(MainDbContext dataContext) : base(dataContext)
        {

        }

        public async Task<ICollection<Visit>> GetForMeeting(int meetingId)
        {
            var q = dataContext.Visits.AsNoTracking()
                    .Where(o => o.MeetingId == meetingId
                             && o.State == States.Active);
            q = q.Include(o => o.User).ThenInclude(u => u.Language);
            q = q.OrderBy(o => o.Id);

            return await q.ToArrayAsync();
        }


        public async Task<int> GetCount(int? userId, States[] states)
        {
            var q = dataContext.Visits.AsNoTracking();
            q = ApplyFilter(q, userId, states);
            return await q.CountAsync();
        }

        public async Task<ICollection<Visit>> Search(VisitSearchParams searchParams)
        {
            var q = dataContext.Visits.AsNoTracking();

            q = q.Include(o => o.Meeting).ThenInclude(o => o.User)
                .Include(o => o.Meeting).ThenInclude(o => o.Language);

            q = ApplyFilter(q, searchParams.UserId, searchParams.States);
            q = ApplySorting(q, searchParams.Sorting);

            return await q.GetPage(searchParams.Page, searchParams.PageSize).ToListAsync();
        }

        private IQueryable<Visit> ApplyFilter(IQueryable<Visit> q, int? userId, States[] states)
        {
            var filteredQuery = q;

            if (userId.HasValue)
                filteredQuery = filteredQuery.Where(o => o.UserId == userId.Value);

            if (states?.Length > 0)
                filteredQuery = filteredQuery.Where(o => states.Contains(o.State));

            return filteredQuery;
        }

        private IQueryable<Visit> ApplySorting(IQueryable<Visit> q, VisitSortings sorting)
        {
            var orderedQuery = q;

            switch (sorting)
            {
                case VisitSortings.CreationDateAsc: orderedQuery = orderedQuery.OrderBy(u => u.CreationDate); break;
                case VisitSortings.CreationDateDesc: orderedQuery = orderedQuery.OrderByDescending(u => u.CreationDate); break;
            }

            return orderedQuery;
        }


        


    }
}
