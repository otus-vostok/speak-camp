using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected readonly MainDbContext dataContext;

        public Repository(MainDbContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public virtual async Task<IEnumerable<T>> GetAll()
        {
            return await dataContext.Set<T>().ToListAsync();

        }

        public virtual async Task<T> GetById(int id)
        {
            return await dataContext.Set<T>().Where(o => o.Id == id).FirstOrDefaultAsync();
        }

        public virtual async Task<IEnumerable<T>> GetByIds(int[] ids)
        {
            return await dataContext.Set<T>().Where(o => ids.Contains(o.Id)).ToListAsync();
        }

        public virtual async Task Create(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            await dataContext.Set<T>().AddAsync(entity);
            await dataContext.SaveChangesAsync();
        }

        public virtual async Task CreateMany(IEnumerable<T> entities)
        {
            if (entities == null)
                throw new ArgumentNullException(nameof(entities));

            if (!entities.Any())
                throw new ArgumentOutOfRangeException(nameof(entities));

            await dataContext.Set<T>().AddRangeAsync(entities);
            await dataContext.SaveChangesAsync();
        }

        public virtual async Task Update(T entity)
        {
            await dataContext.SaveChangesAsync();
        }

        public virtual async Task Delete(T entity)
        {

            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            dataContext.Set<T>().Remove(entity);
            await dataContext.SaveChangesAsync();
        }

        public async Task<bool> CheckExists(Expression<Func<T, bool>> expr)
        {
            return await dataContext.Set<T>().AnyAsync(expr);
        }
    }
}
