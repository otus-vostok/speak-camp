using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Repositories
{

    public class LanguagesRepository : Repository<Language>, ILanguagesRepository
    {
        private readonly ICacheManager _cacheManager;
        public LanguagesRepository(MainDbContext dataContext, ICacheManager cacheManager) : base(dataContext)
        {
            _cacheManager = cacheManager;
        }


        public async Task<ICollection<Language>> GetAllCached()
        {
            string cacheKey = "Languages_GetAllCached";
            int cacheTimeMinutes = 60;

            var res = _cacheManager.Get<ICollection<Language>>(cacheKey);

            if (res == null)
            {
                res = await dataContext.Languages.AsNoTracking().ToArrayAsync();
                _cacheManager.Set(cacheKey, res, TimeSpan.FromMinutes(cacheTimeMinutes));
            }

            return res;
        }


        public async Task<Language> GetByKey(string key)
        {
            return await dataContext.Languages.Where(o => o.KeyName == key).FirstOrDefaultAsync();
        }



    }
}
