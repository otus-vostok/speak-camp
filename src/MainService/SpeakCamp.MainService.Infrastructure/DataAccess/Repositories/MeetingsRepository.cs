using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Meetings;
using SpeakCamp.MainService.Infrastructure.DataAccess.Helpers;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Repositories
{
    public class MeetingsRepository : Repository<Meeting>, IMeetingsRepository
    {

        public MeetingsRepository(MainDbContext dataContext) : base(dataContext)
        {
           
        }

        public async Task<Meeting> GetByIdDetailed(int id)
        {
            return await dataContext.Meetings
                .Where(m => m.Id == id)
                .Include(m => m.Visits)
                .Include(m => m.Language)
                .Include(m => m.User)
                .FirstOrDefaultAsync();
        }

        public async Task<int> GetCount(MeetingFilterParams filterParams)
        {
            var q = dataContext.Meetings.AsNoTracking();
            q = ApplyFilter(q, filterParams);
            return await q.CountAsync();
        }

        public async Task<ICollection<Meeting>> Search(MeetingSearchParams searchParams)
        {
            var q = dataContext.Meetings.AsNoTracking();

            q = q.Include(m => m.User).ThenInclude(u => u.Language)
                .Include(m => m.Language);

            q = ApplyFilter(q, searchParams);
            q = ApplySorting(q, searchParams.Sorting);

            return await q.GetPage(searchParams.Page, searchParams.PageSize).ToListAsync();
        }

        private IQueryable<Meeting> ApplyFilter(IQueryable<Meeting> q, MeetingFilterParams filterParams)
        {
            var filteredQuery = q;

            if (filterParams.UserId.HasValue)
                filteredQuery = filteredQuery.Where(m => m.UserId == filterParams.UserId.Value);

            if (filterParams.States?.Length > 0)
                filteredQuery = filteredQuery.Where(m => filterParams.States.Contains(m.State));

            if (!string.IsNullOrEmpty(filterParams.LanguageKey))
                filteredQuery = filteredQuery.Where(m => m.Language.KeyName == filterParams.LanguageKey);

            if (filterParams.LanguageLevel.HasValue)
                filteredQuery = filteredQuery.Where(m => m.LanguageLevel == filterParams.LanguageLevel.Value);

            if (filterParams.DateFrom.HasValue)
                filteredQuery = filteredQuery.Where(m => m.StartDate  >= filterParams.DateFrom.Value);

            if (filterParams.DateTo.HasValue)
                filteredQuery = filteredQuery.Where(m => m.StartDate <= filterParams.DateTo.Value);


            return filteredQuery;
        }

        private IQueryable<Meeting> ApplySorting(IQueryable<Meeting> q, MeetingSortings sorting)
        {
            var orderedQuery = q;

            switch (sorting)
            {
                case MeetingSortings.Popularity: orderedQuery = orderedQuery.OrderByDescending(m => m.PlacesOccupied).ThenBy(m=>m.StartDate); break;
                case MeetingSortings.StartDate: orderedQuery = orderedQuery.OrderBy(m => m.StartDate); break;
                case MeetingSortings.Rating: orderedQuery = orderedQuery.OrderByDescending(m => m.User.Rating).ThenBy(m => m.StartDate); break;
            }

            return orderedQuery;
        }


    }
}
