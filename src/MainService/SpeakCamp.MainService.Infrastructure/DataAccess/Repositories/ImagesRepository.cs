using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Abstractions;
using Microsoft.EntityFrameworkCore;
using SpeakCamp.MainService.Application.Images.GetImage;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Repositories
{
    public class ImagesRepository : Repository<Image>, IImageRepository
    {
        public ImagesRepository(MainDbContext dataContext)
            : base(dataContext) { }

        public Task<Image> GetImageByParamsAsync(ImageSearchParams searchParams)
        {
            return dataContext.Images.AsNoTracking()
                    .FirstOrDefaultAsync(x =>
                        x.Key == searchParams.Key
                        && x.ImageSize == searchParams.ImageSize
                    );
        }
    }
}
