using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Helpers;
using SpeakCamp.MainService.Application.Users;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SpeakCamp.MainService.Infrastructure.DataAccess.Helpers;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Repositories
{
    public class UsersRepository : Repository<User>, IUsersRepository
    {

        public UsersRepository(MainDbContext dataContext) : base (dataContext)
        {
           
        }

        public async Task<User> GetByAccountId(int accountId)
        {
            return await dataContext.Users.Where(o => o.AccountId == accountId).FirstOrDefaultAsync();
        }


        public async Task<int> GetCount(UserSearchParams searchParams)
        {
            var q = PrepareSearchFilter(searchParams);
            return await q.CountAsync();
        }

        public async Task<IEnumerable<(UserTypes, int)>> GetTypesCount()
        {
            var data = await dataContext.Users.AsNoTracking()
                .GroupBy(x => x.UserType)
                .Select(x => new { userType = x.Key, amount = x.Count() }).ToListAsync();

            return data.Select(x => (x.userType, x.amount));
        }

        public async Task<ICollection<User>> Search(UserSearchParams searchParams) 
        {

            var q = PrepareSearchFilter(searchParams);

            q = q.Include(u => u.Language);

            switch (searchParams.Sorting) 
            {
                case UsersSortings.CreationDateAsc: q = q.OrderBy(u => u.CreationDate); break;
                case UsersSortings.CreationDateDesc: q = q.OrderByDescending(u => u.CreationDate); break;
                case UsersSortings.RatingAsc: q = q.OrderBy(u => u.Rating); break;
                case UsersSortings.RatingDesc: q = q.OrderByDescending(u => u.Rating); break;
            }

            return await q.GetPage(searchParams.Page, searchParams.PageSize).ToListAsync();
        
        }



        private IQueryable<User> PrepareSearchFilter(UserSearchParams searchParams)
        {

            var q = dataContext.Users.AsNoTracking();

            if (searchParams.UserType.HasValue)
                q = q.Where(u => u.UserType == searchParams.UserType);

            if (searchParams.States?.Length > 0)
                q = q.Where(u => searchParams.States.Contains(u.State));

            return q;

        }
    }
}
