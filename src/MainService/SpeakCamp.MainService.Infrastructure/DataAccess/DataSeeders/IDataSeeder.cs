using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.DataSeeders
{
    public interface IDataSeeder
    {
        public void Seed();

    }
}
