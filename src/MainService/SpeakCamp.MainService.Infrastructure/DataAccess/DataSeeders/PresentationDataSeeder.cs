using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.DataSeeders
{
    public class PresentationDataSeeder: IDataSeeder
    {
        private readonly MainDbContext _dataContext;

        public PresentationDataSeeder(MainDbContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void Seed()
        {

            if (_dataContext.Images.Any() || _dataContext.Users.Any())
                return;

            _dataContext.AddRange(PresentationDataFactory.GetImages());
            _dataContext.SaveChanges();

            _dataContext.AddRange(PresentationDataFactory.GetUsers());
            _dataContext.SaveChanges();

            _dataContext.AddRange(PresentationDataFactory.GetMeetings());
            _dataContext.SaveChanges();

            _dataContext.AddRange(PresentationDataFactory.GetVisits());
            _dataContext.SaveChanges();

            RestartSequences(100);



        }

        private void RestartSequences(int startingId)
        {
            _dataContext.Database.ExecuteSqlRaw($"ALTER SEQUENCE public.images_id_seq	RESTART {startingId};");
            _dataContext.Database.ExecuteSqlRaw($"ALTER SEQUENCE public.users_id_seq	RESTART {startingId};");
            _dataContext.Database.ExecuteSqlRaw($"ALTER SEQUENCE public.meetings_id_seq	RESTART {startingId};");
            _dataContext.Database.ExecuteSqlRaw($"ALTER SEQUENCE public.visits_id_seq	RESTART {startingId};");
        }
    }
}
