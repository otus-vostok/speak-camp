using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.DataSeeders
{
    public static class PresentationDataFactory
    {
        const int LangEn = 1;
        const int LangGe = 4;
        const string MeetingPhotoKeyTemplate = "ed001a7f-4444-42d8-bde1-7ecb7e397";
        const string UserPhotoKeyTemplate = "ed002a7f-5555-42d8-bde1-7ecb7e397";

        const string TutorDescription = "Tutor description is copy that aims to tell your potential attendees what will be happening at the event, who will be speaking, and what they will get out of attending. Good event descriptions can drive attendance to events and also lead to more media coverage.";
        const string TutorDescriptionGe = "Die Beschreibung des Tutors ist eine Kopie, die darauf abzielt, Ihren potenziellen Teilnehmern mitzuteilen, was bei der Veranstaltung passiert, wer sprechen wird und was sie von der Teilnahme haben. Gute Veranstaltungsbeschreibungen können die Teilnahme an Veranstaltungen steigern und auch zu mehr Medienberichterstattung führen.";

        static readonly string MeetingDescription = "Meeting description is copy that aims to tell your potential attendees what will be happening at the event, who will be speaking, and what they will get out of attending. Good event descriptions can drive attendance to events and also lead to more media coverage."
                                            + Environment.NewLine + Environment.NewLine + "Also description is copy that aims to tell your potential attendees what will be happening at the event, who will be speaking, and what they will get out of attending. Good event descriptions can drive attendance to events and also lead to more media coverage.";
        static readonly string MeetingDescriptionGe = "Die Besprechungsbeschreibung ist eine Kopie, die darauf abzielt, Ihren potenziellen Teilnehmern mitzuteilen, was bei der Veranstaltung passiert, wer sprechen wird und was sie von der Teilnahme haben. Gute Veranstaltungsbeschreibungen können die Teilnahme an Veranstaltungen steigern und auch zu mehr Medienberichterstattung führen."
                                             + Environment.NewLine + Environment.NewLine + "Eine Beschreibung ist auch eine Kopie, die darauf abzielt, Ihren potenziellen Teilnehmern mitzuteilen, was bei der Veranstaltung passiert, wer sprechen wird und was sie von der Teilnahme haben. Gute Veranstaltungsbeschreibungen können die Teilnahme an Veranstaltungen steigern und auch zu mehr Medienberichterstattung führen.";

        public static List<Image> GetImages()
        {
            string currentAssemblyFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            string imagesFolder = $"{currentAssemblyFolder}{Path.DirectorySeparatorChar}DataAccess{Path.DirectorySeparatorChar}DataSeeders{Path.DirectorySeparatorChar}Images{Path.DirectorySeparatorChar}";

            var list = new List<Image>();

            // Meetings photos
            for (byte i=1; i<=12; i++)
            {
                list.Add(new Image()
                {
                    CreationDate = DateTime.UtcNow,
                    Key = GetMeetingPhotoKey(i),
                    Data = File.ReadAllBytes(imagesFolder + $"m{i}.jpg"),
                    ImageSize = ImageSizes.Full,
                    ImageType = ImageTypes.MeetingPhoto,
                    State = States.Active,
                    ContentType = "image/jpeg"
                });

                list.Add(new Image()
                {
                    CreationDate = DateTime.UtcNow,
                    Key = GetMeetingPhotoKey(i),
                    Data = File.ReadAllBytes(imagesFolder + $"m{i}-preview.jpg"),
                    ImageSize = ImageSizes.Preview,
                    ImageType = ImageTypes.MeetingPhoto,
                    State = States.Active,
                    ContentType = "image/jpeg"
                });
            }

            // Users photos
            for (byte i = 1; i <= 12; i++)
            {
                list.Add(new Image()
                {
                    CreationDate = DateTime.UtcNow,
                    Key = GetUserPhotoKey(i),
                    Data = File.ReadAllBytes(imagesFolder + $"u{i}.jpg"),
                    ImageSize = ImageSizes.Full,
                    ImageType = ImageTypes.UserPhoto,
                    State = States.Active,
                    ContentType = "image/jpeg"
                });

                list.Add(new Image()
                {
                    CreationDate = DateTime.UtcNow,
                    Key = GetUserPhotoKey(i),
                    Data = File.ReadAllBytes(imagesFolder + $"u{i}-preview.jpg"),
                    ImageSize = ImageSizes.Preview,
                    ImageType = ImageTypes.UserPhoto,
                    State = States.Active,
                    ContentType = "image/jpeg"
                });
            }

            return list;

        }


        public static List<User> GetUsers()
        {
            return new List<User>()
            {
                new User(){ Id = 1, AccountId = 1, UserType=UserTypes.Tutor, CreationDate = DateTime.UtcNow.AddMonths(-3), FirstName="Alex", LastName="Brown", PhotoKey = GetUserPhotoKey(1),  MeetingsCount = 3 , Rating = 4.5M, LanguageId = LangEn, LanguageLevel=LanguageLevels.C1, Description = TutorDescription, State=States.Active },
                new User(){ Id = 2, AccountId = 2, UserType=UserTypes.Tutor, CreationDate = DateTime.UtcNow.AddMonths(-1), FirstName="Eva", LastName="Williams", PhotoKey = GetUserPhotoKey(2),  MeetingsCount = 3 , Rating = 4M, LanguageId = LangEn, LanguageLevel=LanguageLevels.C2,  Description = TutorDescription, State=States.Active },
                new User(){ Id = 3, AccountId = 3, UserType=UserTypes.Tutor, CreationDate = DateTime.UtcNow.AddMonths(-5), FirstName="George", LastName="Taylor", PhotoKey = GetUserPhotoKey(3),  MeetingsCount = 3 , Rating = 3.5M, LanguageId = LangEn, LanguageLevel=LanguageLevels.C1,  Description = TutorDescription, State=States.Active },
                new User(){ Id = 4, AccountId = 4, UserType=UserTypes.Tutor, CreationDate = DateTime.UtcNow.AddMonths(-2), FirstName="Maria", LastName="Schmidt", PhotoKey = GetUserPhotoKey(4),  MeetingsCount = 3 , Rating = 5M, LanguageId = LangGe, LanguageLevel=LanguageLevels.C2, Description = TutorDescriptionGe, State=States.Active  },

                new User(){ Id = 5, AccountId = 5, UserType=UserTypes.Student, CreationDate = DateTime.UtcNow.AddMonths(-1), FirstName="Anna", LastName="Wilson", PhotoKey = GetUserPhotoKey(5),    LanguageId = LangEn, LanguageLevel=LanguageLevels.B2,  State=States.Active  },
                new User(){ Id = 6, AccountId = 6, UserType=UserTypes.Student, CreationDate = DateTime.UtcNow.AddMonths(-2), FirstName="Olivia", LastName="Jones", PhotoKey = GetUserPhotoKey(6),    LanguageId = LangEn, LanguageLevel=LanguageLevels.B1,  State=States.Active  },
                new User(){ Id = 7, AccountId = 7, UserType=UserTypes.Student, CreationDate = DateTime.UtcNow.AddMonths(-1), FirstName="Chuck", LastName="Lee", PhotoKey = GetUserPhotoKey(7),    LanguageId = LangEn, LanguageLevel=LanguageLevels.A2,  State=States.Active  },
                new User(){ Id = 8, AccountId = 8, UserType=UserTypes.Student, CreationDate = DateTime.UtcNow.AddMonths(-1), FirstName="Poppy", LastName="Evans", PhotoKey = GetUserPhotoKey(8),    LanguageId = LangEn, LanguageLevel=LanguageLevels.A2,  State=States.Active  },
                new User(){ Id = 9, AccountId = 9, UserType=UserTypes.Student, CreationDate = DateTime.UtcNow.AddMonths(-1), FirstName="Peter", LastName="Brown", PhotoKey = GetUserPhotoKey(9),    LanguageId = LangEn, LanguageLevel=LanguageLevels.C1,  State=States.Active  },

                new User(){ Id = 10, AccountId = 10, UserType=UserTypes.Student, CreationDate = DateTime.UtcNow.AddMonths(-1), FirstName="Grace", LastName="Johnson", PhotoKey = GetUserPhotoKey(10),    LanguageId = LangGe, LanguageLevel=LanguageLevels.B1,  State=States.Active  },
                new User(){ Id = 11, AccountId = 11, UserType=UserTypes.Student, CreationDate = DateTime.UtcNow.AddMonths(-2), FirstName="Emily", LastName="Roberts", PhotoKey = GetUserPhotoKey(11),    LanguageId = LangGe, LanguageLevel=LanguageLevels.B2,  State=States.Active  },
                new User(){ Id = 12, AccountId = 12, UserType=UserTypes.Student, CreationDate = DateTime.UtcNow.AddMonths(-1), FirstName="Leo", LastName="White", PhotoKey = GetUserPhotoKey(12),    LanguageId = LangGe, LanguageLevel=LanguageLevels.A2,  State=States.Active  },
            };
        }


        public static List<Meeting> GetMeetings()
        {
            return new List<Meeting>()
            {
                new Meeting(){ Id = 1, UserId = 1, Name = "In the office: useful phrases, verbs and idioms", PhotoKey = GetMeetingPhotoKey(1), CreationDate = DateTime.UtcNow.AddDays(-10), LanguageId = LangEn, Description = MeetingDescription, LanguageLevel=LanguageLevels.B1, DurationMinutes = 30, PlacesLimit = 20, PlacesOccupied=4, StartDate = DateTime.UtcNow.AddDays(3.1), State = MeetingStates.Active },
                new Meeting(){ Id = 2, UserId = 1, Name = "Birthday is coming", PhotoKey = GetMeetingPhotoKey(2), CreationDate = DateTime.UtcNow.AddDays(-6), LanguageId = LangEn, Description = MeetingDescription, LanguageLevel=LanguageLevels.A2, DurationMinutes = 30, PlacesLimit = 15, PlacesOccupied=3, StartDate = DateTime.UtcNow.AddDays(2.3), State = MeetingStates.Active },
                new Meeting(){ Id = 3, UserId = 1, Name = "Education system in the USA and Britain", PhotoKey = GetMeetingPhotoKey(3), CreationDate = DateTime.UtcNow.AddDays(-5), LanguageId = LangEn, Description = MeetingDescription, LanguageLevel=LanguageLevels.B2, DurationMinutes = 30, PlacesLimit = 25,PlacesOccupied=4, StartDate = DateTime.UtcNow.AddDays(10.5), State = MeetingStates.Active },
                new Meeting(){ Id = 4, UserId = 2, Name = "Tasty topic: let's discuss food and drinks", PhotoKey = GetMeetingPhotoKey(4), CreationDate = DateTime.UtcNow.AddDays(-3), LanguageId = LangEn, Description = MeetingDescription, LanguageLevel=LanguageLevels.B1, DurationMinutes = 30, PlacesLimit = 25, StartDate = DateTime.UtcNow.AddDays(1.5), State = MeetingStates.Active },
                new Meeting(){ Id = 5, UserId = 2, Name = "Pets at home and outdoors", PhotoKey = GetMeetingPhotoKey(5), CreationDate = DateTime.UtcNow.AddDays(-4), LanguageId = LangEn, Description = MeetingDescription, LanguageLevel=LanguageLevels.C1, DurationMinutes = 20, PlacesLimit = 20, StartDate = DateTime.UtcNow.AddDays(3.4), State = MeetingStates.Active },
                new Meeting(){ Id = 6, UserId = 2, Name = "Famous British music groups", PhotoKey = GetMeetingPhotoKey(6), CreationDate = DateTime.UtcNow.AddDays(-1), LanguageId = LangEn, Description = MeetingDescription, LanguageLevel=LanguageLevels.A1, DurationMinutes = 30, PlacesLimit = 25, StartDate = DateTime.UtcNow.AddDays(5.7), State = MeetingStates.Active },
                new Meeting(){ Id = 7, UserId = 3, Name = "Football players and fans", PhotoKey = GetMeetingPhotoKey(7), CreationDate = DateTime.UtcNow.AddDays(-2), LanguageId = LangEn, Description = MeetingDescription, LanguageLevel=LanguageLevels.B1, DurationMinutes = 30, PlacesLimit = 25, StartDate = DateTime.UtcNow.AddDays(1.7), State = MeetingStates.Active },
                new Meeting(){ Id = 8, UserId = 3, Name = "Household chores vocabulary and grammar", PhotoKey = GetMeetingPhotoKey(8), CreationDate = DateTime.UtcNow.AddDays(-5), LanguageId = LangEn, Description = MeetingDescription, LanguageLevel=LanguageLevels.A1, DurationMinutes = 20, PlacesLimit = 10, StartDate = DateTime.UtcNow.AddDays(3.7), State = MeetingStates.Active },
                new Meeting(){ Id = 9, UserId = 3, Name = "Long-haul flight - on the plane and at the airport", PhotoKey = GetMeetingPhotoKey(9), CreationDate = DateTime.UtcNow.AddDays(-3), LanguageId = LangEn, Description = MeetingDescription, LanguageLevel=LanguageLevels.B2, DurationMinutes = 30, PlacesLimit = 25, StartDate = DateTime.UtcNow.AddDays(6.2), State = MeetingStates.Active },
                new Meeting(){ Id = 10, UserId = 4, Name = "Umstellung auf grüne Energie", PhotoKey = GetMeetingPhotoKey(10), CreationDate = DateTime.UtcNow.AddDays(-3), LanguageId = LangGe, Description = MeetingDescriptionGe, LanguageLevel=LanguageLevels.B1, DurationMinutes = 30, PlacesLimit = 25, StartDate = DateTime.UtcNow.AddDays(2.7), State = MeetingStates.Active },
                new Meeting(){ Id = 11, UserId = 4, Name = "Deutsche klassische langweilige Literatur", PhotoKey = GetMeetingPhotoKey(11), CreationDate = DateTime.UtcNow.AddDays(-2), LanguageId = LangGe, Description = MeetingDescriptionGe, LanguageLevel=LanguageLevels.A2, DurationMinutes = 20, PlacesLimit = 15, StartDate = DateTime.UtcNow.AddDays(3.2), State = MeetingStates.Active },
                new Meeting(){ Id = 12, UserId = 4, Name = "Blätter an einer Schnur sind wunderschön", PhotoKey = GetMeetingPhotoKey(12), CreationDate = DateTime.UtcNow.AddDays(-5), LanguageId = LangGe, Description = MeetingDescriptionGe, LanguageLevel=LanguageLevels.B2, DurationMinutes = 30, PlacesLimit = 25, StartDate = DateTime.UtcNow.AddDays(4.1), State = MeetingStates.Active },

            };

        }


        public static List<Visit> GetVisits()
        {
            return new List<Visit>()
            {
                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-2), MeetingId = 1, UserId = 8, State = States.Active},
                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-1.9), MeetingId = 1, UserId = 9, State = States.Active},
                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-1.8), MeetingId = 1, UserId = 7, State = States.Active},
                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-1.7), MeetingId = 1, UserId = 2, State = States.Active},

                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-2), MeetingId = 2, UserId = 5, State = States.Active},
                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-1.9), MeetingId = 2, UserId = 6, State = States.Active},
                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-1.8), MeetingId = 2, UserId = 7, State = States.Active},

                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-2), MeetingId = 3, UserId = 9, State = States.Active},
                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-1.9), MeetingId = 3, UserId = 6, State = States.Active},
                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-1.8), MeetingId = 3, UserId = 5, State = States.Active},
                new Visit(){CreationDate=DateTime.UtcNow.AddDays(-1.7), MeetingId = 3, UserId = 3, State = States.Active},
            };
       }



        private static Guid GetUserPhotoKey(byte num)
        {
            return Guid.Parse( $"{UserPhotoKeyTemplate}{num.ToString("000")}");
        }


        private static Guid GetMeetingPhotoKey(byte num)
        {
            return Guid.Parse($"{MeetingPhotoKeyTemplate}{num.ToString("000")}");
        }


    }
}
