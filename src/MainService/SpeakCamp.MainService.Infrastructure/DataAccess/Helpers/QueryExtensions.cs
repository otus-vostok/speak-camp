using System.Linq;

namespace SpeakCamp.MainService.Infrastructure.DataAccess.Helpers
{
    public static class QueryExtensions
    {
        public static IQueryable<TSource> GetPage<TSource>(this IQueryable<TSource> source, int page, int pageSize) {

            return source.Skip((page - 1) * pageSize).Take(pageSize);
        }
    }
}
