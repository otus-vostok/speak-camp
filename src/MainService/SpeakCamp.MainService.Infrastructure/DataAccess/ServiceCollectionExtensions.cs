using Microsoft.Extensions.DependencyInjection;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Infrastructure.DataAccess.DataSeeders;
using SpeakCamp.MainService.Infrastructure.DataAccess.Repositories;

namespace SpeakCamp.MainService.Infrastructure.DataAccess
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddDataAccessServices(this IServiceCollection services)
        {
            services.AddScoped(typeof(IDataSeeder), typeof(PresentationDataSeeder));
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped(typeof(IUsersRepository), typeof(UsersRepository));
            services.AddScoped(typeof(ILanguagesRepository), typeof(LanguagesRepository));
            services.AddScoped(typeof(IMeetingsRepository), typeof(MeetingsRepository));
            services.AddScoped(typeof(IVisitsRepository), typeof(VisitsRepository));
            services.AddScoped(typeof(IImageRepository), typeof(ImagesRepository));

            return services;
        }
    }
}
