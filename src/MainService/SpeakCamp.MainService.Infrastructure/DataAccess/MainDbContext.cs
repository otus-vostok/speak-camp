using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Entities;
using Microsoft.EntityFrameworkCore;

namespace SpeakCamp.MainService.Infrastructure.DataAccess {
    public class MainDbContext : DbContext {


        public DbSet<User> Users { get; set; }
        public DbSet<Meeting> Meetings { get; set; }
        public DbSet<Language> Languages { get; set; }
        public DbSet<Visit> Visits { get; set; }
        public DbSet<Image> Images { get; set; }

        public MainDbContext(DbContextOptions<MainDbContext> options) : base(options)
        {

        }



        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            
            modelBuilder.Entity<User>().Property(p => p.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<User>().HasIndex(i => i.AccountId).IsUnique();
            modelBuilder.Entity<User>().Property(p => p.Email).HasMaxLength(255);
            modelBuilder.Entity<User>().Property(p => p.FirstName).HasMaxLength(255);
            modelBuilder.Entity<User>().Property(p => p.LastName).HasMaxLength(255);
            modelBuilder.Entity<Meeting>().Property(p => p.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Meeting>().Property(p => p.Name).HasMaxLength(255);
            modelBuilder.Entity<Meeting>().Property(p => p.ConferenceLink).HasMaxLength(1000);
            modelBuilder.Entity<Language>().Property(p => p.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Language>().Property(p => p.KeyName).HasMaxLength(255);
            modelBuilder.Entity<Language>().Property(p => p.Name).HasMaxLength(255);
            modelBuilder.Entity<Visit>().Property(p => p.Id).ValueGeneratedOnAdd();
            modelBuilder.Entity<Image>().Property(p => p.Id).ValueGeneratedOnAdd();



        }


    }
}
