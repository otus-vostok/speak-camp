using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SL = SixLabors.ImageSharp;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Formats;
using SixLabors.ImageSharp.Processing;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Helpers;
using SixLabors.ImageSharp.Formats.Jpeg;

namespace SpeakCamp.MainService.Infrastructure.ImageProcess
{
    /// <summary>
    /// Класс, занимающийся обработкой изображений
    /// </summary>
    internal class ImageProcessor : IImageProcessor
    {
        const ResizeMode DEFAULT_RESIZE_MODE = ResizeMode.Crop;
        const int DEFAULT_JPEG_QUALITY = 90;

        public async Task<Dictionary<ImageSizes, byte[]>> ProcessAsync(ImageTypes imageType, byte[] file)
        {
            return await Task.Run(() =>
            {
                using var sourceImage = SL.Image.Load(file);

                return ImageHelper.ImageTypesImageSizes[imageType].AsParallel().Select(x => {

                    var imageSize = x.Key;
                    var imageConfig = x.Value;

                    /**
                     * Клонирование происходит сразу с обрезкой
                     */
                    using var imageCopy = sourceImage.Clone(x => { });

                    FormatImage(imageCopy, imageConfig);
                
                    var processedData = ConvertImageToArray(imageCopy);

                    return (imageSize, processedData);
                })
                .ToDictionary(x => x.imageSize, x => x.processedData);
            }); 
        }

        private static byte[] ConvertImageToArray(SL.Image image)
        {
            using var ms = new MemoryStream();

            image.SaveAsJpeg(ms, new JpegEncoder() { Quality = DEFAULT_JPEG_QUALITY });

            return ms.ToArray();
        }

        private static void FormatImage(SL.Image image, ImageConfiguration imageConfig)
        {
            image.Mutate(c => c.Resize(
                new ResizeOptions
                {
                    Size = new Size(imageConfig.Width, imageConfig.Height),
                    Mode = DEFAULT_RESIZE_MODE,
                    Sampler = KnownResamplers.Bicubic
                }
            ));
        }


    }
}
