using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SpeakCamp.MainService.Application.Common.Abstractions;

namespace SpeakCamp.MainService.Infrastructure.ImageProcess
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddImageServices(this IServiceCollection services)
        {
            services.AddTransient(typeof(IImageProcessor), typeof(ImageProcessor));

            return services;
        }
    }
}
