using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using SpeakCamp.MainService.Application.Common.Abstractions;

namespace SpeakCamp.MainService.Infrastructure.Caching
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddCachingServices(this IServiceCollection services)
        {
            services.AddMemoryCache();
            services.AddScoped(typeof(ICacheManager), typeof(MemoryCacheManager));

            return services;
        }
    }
}
