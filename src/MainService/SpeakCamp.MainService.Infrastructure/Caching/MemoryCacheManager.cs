using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;
using SpeakCamp.MainService.Application.Common.Abstractions;

namespace SpeakCamp.MainService.Infrastructure.Caching
{
    public class MemoryCacheManager: ICacheManager
    {
        private readonly IMemoryCache _cache;
        public MemoryCacheManager(IMemoryCache cache)
        {
            _cache = cache;
        }

        public T Get<T>(string key)
        {
            return _cache.Get<T>(key);
            
        }

        public void Set<T>(string key, T item, TimeSpan absoluteExpiration)
        {
            _cache.Set<T>(key, item, new MemoryCacheEntryOptions() {AbsoluteExpirationRelativeToNow = absoluteExpiration });
        }

    }
}
