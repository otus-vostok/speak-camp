using System;
using System.Threading;
using AutoFixture;
using AutoFixture.AutoMoq;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using Moq;
using Xunit;
using SpeakCamp.MainService.Application.Meetings.StartMeeting;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Exceptions;
using FluentAssertions;

namespace MainService.Tests.Controllers
{
    /// <summary>
    /// Тест на старт встречи
    /// </summary>
    public class StartMeetingCommandTest
    {
        private readonly Mock<IUsersRepository> _usersRepositoryMock;

        private readonly Mock<IMeetingsRepository> _meetingsRepositoryMock;

        private readonly IRequestHandler<StartMeetingCommand, int> _startMeetingCommandHandler;

        private readonly StartMeetingCommand request;

        public StartMeetingCommandTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _usersRepositoryMock = fixture.Freeze<Mock<IUsersRepository>>();
            _meetingsRepositoryMock = fixture.Freeze<Mock<IMeetingsRepository>>();

            _startMeetingCommandHandler = fixture.Build<StartMeetingCommandHandler>().Create();

            // Arrange
            request = new StartMeetingCommand
            {
                AccountId = 123,
                MeetingId = 456,
                ConferenceLink = "test",
                Note = "Запись встречи будет доступна в течение 48 часов"
            };
        }

        [Fact]
        public async void StartMeetingCommand_ForValidData_MeetingStarted()
        {
            Meeting savedMeeting = null;
            _meetingsRepositoryMock.Setup(c => c.Update(It.IsAny<Meeting>()))
                    .Callback<Meeting>((obj) => savedMeeting = obj);

            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User {  Id = 0 } );
            _meetingsRepositoryMock.Setup(repo => repo.GetById(request.MeetingId)).ReturnsAsync(()
                => new Meeting { State = MeetingStates.Active, StartDate = DateTime.UtcNow + TimeSpan.FromMinutes(30)});

            // Act
            var cancellationToken = new CancellationToken();
            var result = await _startMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            _meetingsRepositoryMock.Verify(m => m.Update(It.IsAny<Meeting>()), Times.Once);
            Assert.Equal(MeetingStates.Started, savedMeeting.State);
            Assert.True(savedMeeting.ConferenceLink.Length > 0);
            Assert.True(savedMeeting.ConferenceNotes.Length > 0);
        }

        /// <summary>
        /// При вызове команды на старт встречи не автором встречи - исключение типа BusinessException
        /// </summary>
        [Fact]
        public async void StartMeetingCommand_IfNotAuthor_ThrowsBusinessException()
        {
            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 1 });
            _meetingsRepositoryMock.Setup(repo => repo.GetById(request.MeetingId)).ReturnsAsync(()
                => new Meeting { State = MeetingStates.Active, StartDate = DateTime.UtcNow + TimeSpan.FromMinutes(30) });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _startMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("Only author can start the meeting");
        }

        /// <summary>
        /// При вызове команды на старт неактивной встречи - исключение типа BusinessException
        /// </summary>
        [Fact]
        public async void StartMeetingCommand_IfNotNotActiveMeeting_ThrowsBusinessException()
        {
            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 0 });
            _meetingsRepositoryMock.Setup(repo => repo.GetById(request.MeetingId)).ReturnsAsync(()
                => new Meeting { State = MeetingStates.Deleted, StartDate = DateTime.UtcNow + TimeSpan.FromMinutes(30) });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _startMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("Only Active meetings can be started");
        }

        /// <summary>
        /// При вызове команды на старт встречи раннее 30 минут до запланированной даты - исключение типа BusinessException
        /// </summary>
        [Fact]
        public async void StartMeetingCommand_InMore30Minutes_ThrowsBusinessException()
        {
            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 0 });
            _meetingsRepositoryMock.Setup(repo => repo.GetById(request.MeetingId)).ReturnsAsync(()
                => new Meeting { State = MeetingStates.Active, StartDate = DateTime.UtcNow + TimeSpan.FromMinutes(40) });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _startMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("The meeting could be started on 30 minutes before the planned date");
        }

        /// <summary>
        /// При вызове команды на старт встречи позднее запланированной даты - исключение типа BusinessException
        /// </summary>
        [Fact]
        public async void StartMeetingCommand_AfterPlannedDate_ThrowsBusinessException()
        {
            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 0 });
            _meetingsRepositoryMock.Setup(repo => repo.GetById(request.MeetingId)).ReturnsAsync(()
                => new Meeting { State = MeetingStates.Active, StartDate = DateTime.UtcNow - TimeSpan.FromDays(1) });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _startMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("The meeting cannot be started after the planned date");
        }

    }
}
