using System.Threading;
using AutoFixture;
using AutoFixture.AutoMoq;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using Moq;
using Xunit;
using SpeakCamp.MainService.Application.Visits.CreateVisit;
using System.Collections.Generic;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Exceptions;
using System;
using FluentAssertions;

namespace MainService.Tests.Controllers
{
    /// <summary>
    /// Тест на создание записи на встречу
    /// </summary>
    public class CreateVisitCommandTest
    {
        private readonly Mock<IUsersRepository> _usersRepositoryMock;

        private readonly Mock<IMeetingsRepository> _meetingsRepositoryMock;

        private readonly IRequestHandler<CreateVisitCommand, int> _createVisitCommandHandler;

        public CreateVisitCommandTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _usersRepositoryMock = fixture.Freeze<Mock<IUsersRepository>>();
            _meetingsRepositoryMock = fixture.Freeze<Mock<IMeetingsRepository>>();

            _createVisitCommandHandler = fixture.Build<CreateVisitCommandHandler>().Create();
        }

        [Fact]
        public async void CreateVisitCommand_ForValidData_VisitSaved()
        {
            // Arrange
            var request = new CreateVisitCommand
            {
                AccountId = 123,
                MeetingId = 456
            };

            Meeting savedObject = null;
            _meetingsRepositoryMock.Setup(c => c.Update(It.IsAny<Meeting>()))
                    .Callback<Meeting>((obj) => savedObject = obj);

            _meetingsRepositoryMock.Setup(repo => repo.GetByIdDetailed(request.MeetingId)).ReturnsAsync(()
                => new Meeting { State = MeetingStates.Active, UserId = 1, PlacesLimit=1, Visits = new List<Visit>() });

            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User());

            // Act
            var cancellationToken = new CancellationToken();
            var result = await _createVisitCommandHandler.Handle(request, cancellationToken);

            // Asserts
            _meetingsRepositoryMock.Verify(m => m.Update(It.IsAny<Meeting>()), Times.Once);
            Assert.Equal(1, savedObject.PlacesOccupied);
           
        }

        [Fact]
        public async void CreateVisitCommand_NotActiveMeeting_ThrowBusinessException()
        {
            // Arrange
            var request = new CreateVisitCommand
            {
                AccountId = 123,
                MeetingId = 456
            };

            _meetingsRepositoryMock.Setup(repo => repo.GetByIdDetailed(request.MeetingId)).ReturnsAsync(()
                => new Meeting { State = MeetingStates.Finished, UserId = 1, PlacesLimit = 1, Visits = new List<Visit>() });

            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User());

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _createVisitCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("You can join only Active meetings");

        }

        [Fact]
        public async void CreateVisitCommand_OwnMeeting_ThrowBusinessException()
        {
            // Arrange
            var request = new CreateVisitCommand
            {
                AccountId = 123,
                MeetingId = 456
            };

            _meetingsRepositoryMock.Setup(repo => repo.GetByIdDetailed(request.MeetingId)).ReturnsAsync(()
                => new Meeting { State = MeetingStates.Active, UserId = 1, PlacesLimit = 1, Visits = new List<Visit>() });

            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 1 });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _createVisitCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("You can't join your own meeting");

        }

        [Fact]
        public async void CreateVisitCommand_NotSuitableLevel_ThrowBusinessException()
        {
            // Arrange
            var request = new CreateVisitCommand
            {
                AccountId = 123,
                MeetingId = 456
            };

            _meetingsRepositoryMock.Setup(repo => repo.GetByIdDetailed(request.MeetingId)).ReturnsAsync(()
                => new Meeting { State = MeetingStates.Active, UserId = 1, PlacesLimit = 1, Visits = new List<Visit>() });

            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { LanguageId = 5 });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _createVisitCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("Meeting language/level is not suitable for you");

        }

        [Fact]
        public async void CreateVisitCommand_AlreadyJoined_ThrowBusinessException()
        {
            // Arrange
            var request = new CreateVisitCommand
            {
                AccountId = 123,
                MeetingId = 456
            };

            var visitItem = new Visit { UserId = 1, State = States.Active};

            _meetingsRepositoryMock.Setup(repo => repo.GetByIdDetailed(request.MeetingId)).ReturnsAsync(()
                => new Meeting
                {
                    State = MeetingStates.Active,
                    PlacesLimit = 1,
                    Visits = new List<Visit>() { visitItem }
                });

            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 1 });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _createVisitCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("You have already joined this meeting");

        }

        [Fact]
        public async void CreateVisitCommand_NoFreePlaces_ThrowBusinessException()
        {
            // Arrange
            var request = new CreateVisitCommand
            {
                AccountId = 123,
                MeetingId = 456
            };

            var visitItem = new Visit { UserId = 1, State = States.Active };

            _meetingsRepositoryMock.Setup(repo => repo.GetByIdDetailed(request.MeetingId)).ReturnsAsync(()
                => new Meeting
                {
                    State = MeetingStates.Active,
                    PlacesLimit = 5,
                    PlacesOccupied = 5,
                    Visits = new List<Visit>() { visitItem }
                });

            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 2 });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _createVisitCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("This meeting doesn't have free places");

        }

    }
}
