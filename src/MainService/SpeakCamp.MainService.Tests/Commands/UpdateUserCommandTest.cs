using System;
using System.Threading;
using AutoFixture;
using AutoFixture.AutoMoq;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Users.CreateUser;
using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using Moq;
using SpeakCamp.MainService.Application.Users.UpdateUser;
using Xunit;
using System.Threading.Tasks;
using FluentAssertions;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace MainService.Tests.Controllers
{
    /// <summary>
    /// Тест на обновление пользователя
    /// </summary>
    public class UpdateUserCommandTest
    {
        private readonly Mock<IUsersRepository> _usersRepositoryMock;

        private readonly Mock<ILanguagesRepository> _languagesRepositoryMock;

        private readonly IRequestHandler<UpdateUserCommand, int> _updateUserCommandHandler;

        public UpdateUserCommandTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _usersRepositoryMock = fixture.Freeze<Mock<IUsersRepository>>();
            _languagesRepositoryMock = fixture.Freeze<Mock<ILanguagesRepository>>();

            _updateUserCommandHandler = fixture.Build<UpdateUserCommandHandler>().Create();
        }

        /// <summary>
        /// При вызове команды на обновление пользователя с валидными данными, пользователь успешно сохранен в БД
        /// </summary>
        [Fact]
        public async void UpdateUserCommand_ForValidData_UserSaved()
        {
            // Arrange

            var request = new UpdateUserCommand
            {
                AccountId = 123,
                FirstName = "Профессор",
                LastName = "Дамблдор",
                LanguageKey = "10",
                LanguageLevel = LanguageLevels.C1.ToString(),
                TimezoneOffsetMinutes = 60,
                Description = "Хогвардс",
                BirthDate = DateTime.Now,
                PhotoKey = Guid.Parse("0AB709BE-28EC-4E05-974B-027B2D786234"),
                Gender = Genders.Male.ToString(),
                Email = "hogwarts@gmail.com"
            };

            _languagesRepositoryMock.Setup(repo => repo.GetByKey(request.LanguageKey)).ReturnsAsync(new Language());
            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(new User());

            // Act
            var cancellationToken = new CancellationToken();
            var result = await _updateUserCommandHandler.Handle(request, cancellationToken);

            // Assert
            _usersRepositoryMock.Verify(u => u.Update(It.IsAny<User>()), Times.Once);
        }

        /// <summary>
        /// При вызове команды на обновление несуществующего пользователя - исключение типа EntityNotFoundException
        /// </summary>
        [Fact]
        public async void UpdateUserCommand_AccountIdAlreadyExists_ThrowsEntityNotFoundException()
        {
            // Arrange

            var request = new UpdateUserCommand
            {
                AccountId = 123,
                FirstName = "Профессор",
                LastName = "Дамблдор",
                LanguageKey = "10",
                LanguageLevel = LanguageLevels.C1.ToString(),
                TimezoneOffsetMinutes = 60,
                Description = "Хогвардс",
                BirthDate = DateTime.Now,
                PhotoKey = Guid.Parse("0AB709BE-28EC-4E05-974B-027B2D786234"),
                Gender = Genders.Male.ToString(),
                Email = "hogwarts@gmail.com"
            };

            _languagesRepositoryMock.Setup(repo => repo.GetByKey(request.LanguageKey)).ReturnsAsync(() => new Language());
            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => null);

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _updateUserCommandHandler.Handle(request, cancellationToken);
 
            // Assert
            await result.Should().ThrowAsync<EntityNotFoundException>();
        }
    }
}
