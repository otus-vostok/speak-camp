using System.Threading;
using AutoFixture;
using AutoFixture.AutoMoq;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using Moq;
using Xunit;
using System.Collections.Generic;
using SpeakCamp.MainService.Application.Visits.DeleteVisit;
using System.Threading.Tasks;
using System;
using FluentAssertions;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace MainService.Tests.Controllers
{
    /// <summary>
    /// Тест на удаление записи на встречу
    /// </summary>
    public class DeleteVisitCommandTest
    {
        private readonly Mock<IUsersRepository> _usersRepositoryMock;

        private readonly Mock<IMeetingsRepository> _meetingsRepositoryMock;

        private readonly Mock<IVisitsRepository> _visitsRepositoryMock;

        private readonly IRequestHandler<DeleteVisitCommand, bool> _deleteVisitCommandHandler;

        public DeleteVisitCommandTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _usersRepositoryMock = fixture.Freeze<Mock<IUsersRepository>>();
            _meetingsRepositoryMock = fixture.Freeze<Mock<IMeetingsRepository>>();
            _visitsRepositoryMock = fixture.Freeze<Mock<IVisitsRepository>>();

            _deleteVisitCommandHandler = fixture.Build<DeleteVisitCommandHandler>().Create();
        }

        [Fact]
        public async void DeleteVisitCommand_ForValidData_VisitDeleted()
        {
            // Arrange
            var request = new DeleteVisitCommand
            {
                AccountId = 123,
                VisitId = 456
            };

            var meetingId = 201;
            var userId = 1;

            Visit savedVisit = null;
            _visitsRepositoryMock.Setup(c => c.Update(It.IsAny<Visit>()))
                    .Callback<Visit>((obj) => savedVisit = obj);

            Meeting savedMeeting = null;
            _meetingsRepositoryMock.Setup(c => c.Update(It.IsAny<Meeting>()))
                    .Callback<Meeting>((obj) => savedMeeting = obj);

            _visitsRepositoryMock.Setup(repo => repo.GetById(request.VisitId)).ReturnsAsync(()
                => new Visit { State =States.Active, UserId = userId, MeetingId = meetingId });

            _meetingsRepositoryMock.Setup(repo => repo.GetByIdDetailed(meetingId)).ReturnsAsync(()
               => new Meeting { State = MeetingStates.Active, UserId = userId, Visits = new List<Visit>(), PlacesOccupied = 1 });

            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = userId });

            // Act
            var cancellationToken = new CancellationToken();
            var result = await _deleteVisitCommandHandler.Handle(request, cancellationToken);

            // Asserts
            _visitsRepositoryMock.Verify(m => m.Update(It.IsAny<Visit>()), Times.Once);
            Assert.Equal(States.Deleted, savedVisit.State);

            _meetingsRepositoryMock.Verify(m => m.Update(It.IsAny<Meeting>()), Times.Once);
            Assert.Equal(MeetingStates.Active, savedMeeting.State);
            Assert.Equal(0, savedMeeting.PlacesOccupied);
        }

        [Fact]
        public async void DeleteVisitCommand_ForAnotherUser_ThrowBusinessException()
        {
            // Arrange
            var request = new DeleteVisitCommand
            {
                AccountId = 123,
                VisitId = 456
            };

            var meetingId = 201;
            var userId = 1;

            _visitsRepositoryMock.Setup(repo => repo.GetById(request.VisitId)).ReturnsAsync(()
                => new Visit { State = States.Active, UserId = userId, MeetingId = meetingId });

            _meetingsRepositoryMock.Setup(repo => repo.GetByIdDetailed(meetingId)).ReturnsAsync(()
               => new Meeting { State = MeetingStates.Active, UserId = userId, Visits = new List<Visit>(), PlacesOccupied = 1 });

            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 2 });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<bool>> result = async () => await _deleteVisitCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("You can't cancel visit for another user");
        }

        [Fact]
        public async void DeleteVisitCommand_IncorrectState_ThrowBusinessException()
        {
            // Arrange
            var request = new DeleteVisitCommand
            {
                AccountId = 123,
                VisitId = 456
            };

            var meetingId = 201;
            var userId = 1;
            var state = MeetingStates.Finished;

            _visitsRepositoryMock.Setup(repo => repo.GetById(request.VisitId)).ReturnsAsync(()
                => new Visit { State = States.Active, UserId = userId, MeetingId = meetingId });

            _meetingsRepositoryMock.Setup(repo => repo.GetByIdDetailed(meetingId)).ReturnsAsync(()
               => new Meeting { State = state, UserId = userId, Visits = new List<Visit>(), PlacesOccupied = 1 });

            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 1 });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<bool>> result = async () => await _deleteVisitCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage($"You can't cancel visit for meeting in {state} state");
        }

    }
}
