using System.Threading;
using AutoFixture;
using AutoFixture.AutoMoq;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using Moq;
using Xunit;
using SpeakCamp.MainService.Application.Meetings.DeleteMeeting;
using System;
using System.Threading.Tasks;
using FluentAssertions;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace MainService.Tests.Controllers
{
    /// <summary>
    /// Тест на удаление встречи
    /// </summary>
    public class DeleteMeetingCommandTest
    {
        private readonly Mock<IUsersRepository> _usersRepositoryMock;

        private readonly Mock<IMeetingsRepository> _meetingsRepositoryMock;

        private readonly IRequestHandler<DeleteMeetingCommand, bool> _deleteMeetingCommandHandler;

        public DeleteMeetingCommandTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _usersRepositoryMock = fixture.Freeze<Mock<IUsersRepository>>();
            _meetingsRepositoryMock = fixture.Freeze<Mock<IMeetingsRepository>>();

            _deleteMeetingCommandHandler = fixture.Build<DeleteMeetingCommandHandler>().Create();
        }

        [Fact]
        public async void DeleteMeetingCommand_ForValidData_MeetingDeleted()
        {
            // Arrange
            var request = new DeleteMeetingCommand
            {
                AccountId = 123,
                MeetingId = 456
            };

            Meeting savedMeeting = null;
            _meetingsRepositoryMock.Setup(c => c.Update(It.IsAny<Meeting>()))
                    .Callback<Meeting>((obj) => savedMeeting = obj);

            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 0 });
            _meetingsRepositoryMock.Setup(repo => repo.GetById(request.MeetingId)).ReturnsAsync(()
              => new Meeting { State = MeetingStates.Draft });

            // Act
            var cancellationToken = new CancellationToken();
            var result = await _deleteMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            _meetingsRepositoryMock.Verify(m => m.Update(It.IsAny<Meeting>()), Times.Once);
            Assert.Equal(MeetingStates.Deleted, savedMeeting.State);
        }

        [Fact]
        public async void DeleteMeetingCommand_NoEnoughRights_ThrowBusinessException()
        {
            // Arrange
            var request = new DeleteMeetingCommand
            {
                AccountId = 123,
                MeetingId = 456
            };

            Meeting savedMeeting = null;
            _meetingsRepositoryMock.Setup(c => c.Update(It.IsAny<Meeting>()))
                    .Callback<Meeting>((obj) => savedMeeting = obj);

            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 1 });
            _meetingsRepositoryMock.Setup(repo => repo.GetById(request.MeetingId)).ReturnsAsync(()
              => new Meeting { State = MeetingStates.Draft });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<bool>> result = async () => await _deleteMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("You don't have enough rights to remove the meeting");
        }

        [Fact]
        public async void DeleteMeetingCommand_OngoingMeeting_ThrowBusinessException()
        {
            // Arrange
            var request = new DeleteMeetingCommand
            {
                AccountId = 123,
                MeetingId = 456
            };

            Meeting savedMeeting = null;
            _meetingsRepositoryMock.Setup(c => c.Update(It.IsAny<Meeting>()))
                    .Callback<Meeting>((obj) => savedMeeting = obj);

            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 0 });
            _meetingsRepositoryMock.Setup(repo => repo.GetById(request.MeetingId)).ReturnsAsync(()
              => new Meeting { State = MeetingStates.Started });

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<bool>> result = async () => await _deleteMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("Deleting ongoing meeting is not allowed");
        }

    }
}
