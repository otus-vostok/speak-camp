using System;
using System.Threading;
using AutoFixture;
using AutoFixture.AutoMoq;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using Moq;
using Xunit;
using System.Linq.Expressions;
using System.Linq;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Exceptions;
using FluentAssertions;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.CreateMeeting;

namespace MainService.Tests.Controllers
{
    /// <summary>
    /// Тест на создание встречи
    /// </summary>
    public class CreateMeetingCommandTest
    {
        private readonly Mock<IUsersRepository> _usersRepositoryMock;

        private readonly Mock<ILanguagesRepository> _languagesRepositoryMock;

        private readonly Mock<IMeetingsRepository> _meetingsRepositoryMock;

        private readonly Mock<IImageRepository> _imageRepositoryMock;

        private readonly IRequestHandler<CreateMeetingCommand, int> _createMeetingCommandHandler;

        private readonly Mock<IMeetingModelFactory> _meetingFactory;

        public CreateMeetingCommandTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _usersRepositoryMock = fixture.Freeze<Mock<IUsersRepository>>();
            _languagesRepositoryMock = fixture.Freeze<Mock<ILanguagesRepository>>();
            _meetingsRepositoryMock = fixture.Freeze<Mock<IMeetingsRepository>>();
            _imageRepositoryMock = fixture.Freeze<Mock<IImageRepository>>();
            _meetingFactory = fixture.Freeze<Mock<IMeetingModelFactory>>();

            _createMeetingCommandHandler = fixture.Build<CreateMeetingCommandHandler>().Create();
        }

        [Fact]
        public async void CreateMeetingCommand_ForValidData_MeetingSaved()
        {
            // Arrange
            var request = new CreateMeetingCommand
            {
                AccountId = 123,
                StartDateUtc = DateTime.UtcNow.AddDays(7),
                PhotoKey = Guid.Parse("2CCDA65C-58F2-4617-A664-A68988F36C7D"),
                LanguageKey = "10",
                LanguageLevel = LanguageLevels.A1.ToString(),
            };

            var Guids = new[] { Guid.NewGuid(), Guid.NewGuid() };
            var items = Guids.Select(x => new Image { Key = (Guid)request.PhotoKey });

            _languagesRepositoryMock.Setup(repo => repo.GetByKey(request.LanguageKey)).ReturnsAsync(() => new Language());
            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 1, UserType = UserTypes.Tutor });
            _imageRepositoryMock.Setup(m => m.CheckExists(It.IsAny<Expression<Func<Image, bool>>>()))
                .ReturnsAsync(() => true);

            // Act
            var cancellationToken = new CancellationToken();
            var result = await _createMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            _meetingsRepositoryMock.Verify(m => m.Create(It.IsAny<Meeting>()), Times.Once);
        }

        [Fact]
        public async void CreateMeetingCommand_StartDateIsTooLate_ThrowBusinessException()
        {
            // Arrange
            var request = new CreateMeetingCommand
            {
                AccountId = 123,
                StartDateUtc = DateTime.UtcNow.AddMinutes(15),
                PhotoKey = Guid.Parse("2CCDA65C-58F2-4617-A664-A68988F36C7D"),
                LanguageKey = "10",
                LanguageLevel = LanguageLevels.A1.ToString(),
            };

            var app = new MeetingModelFactory(_usersRepositoryMock.Object, _languagesRepositoryMock.Object, _imageRepositoryMock.Object);

            var Guids = new[] { Guid.NewGuid(), Guid.NewGuid() };
            var items = Guids.Select(x => new Image { Key = (Guid)request.PhotoKey });

            _meetingFactory.Setup(a => a.Create(request)).ReturnsAsync(() => app.Create(request).Result);
            _languagesRepositoryMock.Setup(repo => repo.GetByKey(request.LanguageKey)).ReturnsAsync(() => new Language());
            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 1, UserType = UserTypes.Tutor });
            _imageRepositoryMock.Setup(m => m.CheckExists(It.IsAny<Expression<Func<Image, bool>>>()))
                .ReturnsAsync(() => true);

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _createMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("The meeting couldn't be arranged later then 1 hour before the start date");
        }

        [Fact]
        public async void CreateMeetingCommand_UserIsNotTutor_ThrowBusinessException()
        {
            // Arrange
            var request = new CreateMeetingCommand
            {
                AccountId = 123,
                StartDateUtc = DateTime.UtcNow.AddDays(7),
                PhotoKey = Guid.Parse("2CCDA65C-58F2-4617-A664-A68988F36C7D"),
                LanguageKey = "10",
                LanguageLevel = LanguageLevels.A1.ToString(),
            };

            var app = new MeetingModelFactory(_usersRepositoryMock.Object, _languagesRepositoryMock.Object, _imageRepositoryMock.Object);

            var Guids = new[] { Guid.NewGuid(), Guid.NewGuid() };
            var items = Guids.Select(x => new Image { Key = (Guid)request.PhotoKey });

            _meetingFactory.Setup(a => a.Create(request)).ReturnsAsync(() => app.Create(request).Result);
            _languagesRepositoryMock.Setup(repo => repo.GetByKey(request.LanguageKey)).ReturnsAsync(() => new Language());
            _usersRepositoryMock.Setup(repo => repo.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User { Id = 1, UserType = UserTypes.Student });
            _imageRepositoryMock.Setup(m => m.CheckExists(It.IsAny<Expression<Func<Image, bool>>>()))
                .ReturnsAsync(() => true);

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _createMeetingCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>().WithMessage("Students can't create their own meetings. Please register as a Tutor");
        }
    }
}
