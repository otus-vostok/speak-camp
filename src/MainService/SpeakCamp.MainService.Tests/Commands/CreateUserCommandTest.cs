using System;
using System.Threading;
using AutoFixture;
using AutoFixture.AutoMoq;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Users.CreateUser;
using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using Moq;
using Xunit;
using SpeakCamp.MainService.Application.Common.Exceptions;
using System.Threading.Tasks;
using FluentAssertions;

namespace MainService.Tests.Controllers
{
    /// <summary>
    /// Тест на создание пользователя
    /// </summary>
    public class CreateUserCommandTest
    {
        private readonly Mock<IUsersRepository> _usersRepositoryMock;

        private readonly Mock<ILanguagesRepository> _languagesRepositoryMock;

        private readonly IRequestHandler<CreateUserCommand, int> _createUserCommandHandler;

        public CreateUserCommandTest()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());

            _usersRepositoryMock = fixture.Freeze<Mock<IUsersRepository>>();
            _languagesRepositoryMock = fixture.Freeze<Mock<ILanguagesRepository>>();

            _createUserCommandHandler = fixture.Build<CreateUserCommandHandler>().Create();
        }

        /// <summary>
        /// При вызове команды на создание пользователя с валидными данными, пользователь успешно сохранен в БД
        /// </summary>
        [Fact]
        public async void CreateUserCommand_ForValidData_UserSaved()
        {
            // Arrange
            var request = new CreateUserCommand
            {
                AccountId = 123,
                UserType = UserTypes.Tutor.ToString(),
                FirstName = "Гермиона",
                LastName = "Грейнджер",
                LanguageKey = "10",
                LanguageLevel = LanguageLevels.A1.ToString(),
                TimezoneOffsetMinutes = 60
            };

            _languagesRepositoryMock.Setup(repo => repo.GetByKey(request.LanguageKey)).ReturnsAsync(() => new Language());
            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => null);

            // Act
            var cancellationToken = new CancellationToken();
            var result = await _createUserCommandHandler.Handle(request, cancellationToken);

            // Assert
            _usersRepositoryMock.Verify(u => u.Create(It.IsAny<User>()), Times.Once);
        }

        [Fact]
        public async void CreateUserCommand_NullLanguage_ThrowsEntityNotFoundException()
        {
            // Arrange
            var request = new CreateUserCommand
            {
                AccountId = 123,
                UserType = UserTypes.Tutor.ToString(),
                FirstName = "Гермиона",
                LastName = "Грейнджер",
                LanguageLevel = LanguageLevels.A1.ToString(),
                TimezoneOffsetMinutes = 60
            };

            _languagesRepositoryMock.Setup(repo => repo.GetByKey(request.LanguageKey)).ReturnsAsync(() => null);
            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => null);

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _createUserCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<EntityNotFoundException>();
        }

        [Fact]
        public async void CreateUserCommand_AccountIdAlreadyExists_ThrowsBusinessException()
        {
            // Arrange
            var request = new CreateUserCommand
            {
                AccountId = 123,
                UserType = UserTypes.Tutor.ToString(),
                FirstName = "Гермиона",
                LastName = "Грейнджер",
                LanguageKey = "10",
                LanguageLevel = LanguageLevels.A1.ToString(),
                TimezoneOffsetMinutes = 60
            };

            _languagesRepositoryMock.Setup(repo => repo.GetByKey(request.LanguageKey)).ReturnsAsync(() => new Language());
            _usersRepositoryMock.Setup(x => x.GetByAccountId(request.AccountId)).ReturnsAsync(() => new User() { AccountId = request.AccountId});

            // Act
            var cancellationToken = new CancellationToken();
            Func<Task<int>> result = async () => await _createUserCommandHandler.Handle(request, cancellationToken);

            // Assert
            await result.Should().ThrowAsync<BusinessException>();
        }
    }
}
