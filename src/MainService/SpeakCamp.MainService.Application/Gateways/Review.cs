﻿using System;

namespace SpeakCamp.MainService.Application.Gateways
{
    public class Review
    {
        public Guid Id { get; set; }
        public int TutorId { get; set; }
        public string Body { get; set; }
        public DateTime CreationDateUtc { get; set; }
        public uint Rating { get; set; }
        public int InitiatorUserId { get; set; }
    }
}
