﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Gateways
{
    public interface IReviewGateway
    {
        Task<Guid> CreateReview(int initiatorId, int userId, string body, uint rating);

        Task<List<Review>> GetReviews(int userId);
    }
}
