﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.References
{
    public class ReferenceGroupDto
    {
        public string Key { get; set; }
        public ICollection<ReferenceDto> Items { get; set; }
    }
}
