using FluentValidation;
using SpeakCamp.MainService.Application.Meetings.GetUserMeetings;

namespace SpeakCamp.MainService.Application.References.GetReferences
{
    public class GetReferencesQueryValidator : AbstractValidator<GetReferencesQuery>
    {
        public GetReferencesQueryValidator()
        {

        }
    }
}
