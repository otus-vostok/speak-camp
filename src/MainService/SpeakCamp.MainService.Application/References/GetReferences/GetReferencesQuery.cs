using AutoMapper;
using EnumsNET;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.References.GetReferences
{
    public class GetReferencesQuery : IQuery<ICollection<ReferenceGroupDto>>
    {

    }

    public class GetReferencesQueryHandler : IRequestHandler<GetReferencesQuery, ICollection<ReferenceGroupDto>>
    {


        public  async Task<ICollection<ReferenceGroupDto>> Handle(GetReferencesQuery request, CancellationToken cancellationToken)
        {

            return await Task.Run(() =>
            {
                var result = new List<ReferenceGroupDto>();

                result.Add(GetReferenceGroup<UserTypes>("userTypes"));
                result.Add(GetReferenceGroup<LanguageLevels>("languageLevels"));
                result.Add(GetReferenceGroup<Genders>("genders"));

                return result;
            
            });

        }


        private ReferenceGroupDto GetReferenceGroup<T>(string key) where T : struct, Enum
        {
            var group = new ReferenceGroupDto() { Key = key, Items = new List<ReferenceDto>() };
            foreach (var member in Enums.GetMembers<T>())
            {
                group.Items.Add(
                    new ReferenceDto() { 
                        Key = member.Name,
                        Value = member.ToInt32(),
                        Name = member.Value.AsString(EnumFormat.DisplayName, EnumFormat.Name),
                        Description = member.Value.AsString(EnumFormat.Description)
                    }
                    );
            }
            return group;
        }

    }
}
