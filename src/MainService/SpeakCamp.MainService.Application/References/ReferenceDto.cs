﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.References
{
    public class ReferenceDto
    {
        public string Key { get; set; }
        public int Value { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

    }
}
