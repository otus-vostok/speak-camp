using System;
using System.Collections.Generic;

namespace SpeakCamp.MainService.Application.Entities
{
    public class Meeting : BaseEntity
    {

        public int UserId { get; set; }
        public User User { get; set; }

        public string Name { get; set; }
        public MeetingStates State { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// Ссылка на Image
        /// </summary>
        public Guid? PhotoKey { get; set; }
        public DateTime StartDate { get; set; }
        public int DurationMinutes { get; set; }
        public int LanguageId { get; set; }
        public Language Language { get; set; }
        public LanguageLevels LanguageLevel { get; set; }
        public int PlacesLimit { get; set; }
        public int PlacesOccupied { get; set; }
        public string ConferenceLink { get; set; }
        public string ConferenceNotes { get; set; }

        public ICollection<Visit> Visits { get; set; }

    }
}
