﻿
namespace SpeakCamp.MainService.Application.Entities
{
    /// <summary>
    /// Язык для изучения
    /// </summary>
    public class Language : BaseEntity
    {
        /// <summary>
        /// Название
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Обозначение
        /// </summary>
        public string KeyName { get; set; }

    }
}
