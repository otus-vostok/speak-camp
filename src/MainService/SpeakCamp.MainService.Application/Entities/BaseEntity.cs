﻿using System;

namespace SpeakCamp.MainService.Application.Entities 
{
    /// <summary>
    /// Базовый класс экземпляра объекта в БД
    /// </summary>
    public abstract class BaseEntity 
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Дата создания
        /// </summary>
        public DateTime CreationDate { get; set; }
    }
}
