using System;
using System.Collections.Generic;

namespace SpeakCamp.MainService.Application.Entities {


    /// <summary>
    /// Пользователь - Student или Tutor
    /// Id совпадает с Id в UserAccount
    /// </summary>
    public class User : BaseEntity {

        public int AccountId { get; set; }
        public UserTypes UserType { get; set; }
        public States State { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int LanguageId { get; set; }
        public Language Language { get; set; }
        public LanguageLevels LanguageLevel { get; set; }

        public string Description { get; set; }
        public DateTime? BirthDate { get; set; }
        public Genders? Gender { get; set; }
        public short TimezoneOffsetMinutes { get; set; }
        /// <summary>
        /// Ссылка на Media
        /// </summary>
        public Guid? PhotoKey { get; set; }


        /// <summary>
        /// Копируем из UserAccount для удобства нотификаций
        /// </summary>
        public string Email { get; set; }
        public int MeetingsCount { get; set; }
        public decimal Rating { get; set; }

        public int ReviewsCount  { get; set; }

        public ICollection<Visit> Visits { get; set; }
        public ICollection<Meeting> Meetings { get; set; }

    }
}
