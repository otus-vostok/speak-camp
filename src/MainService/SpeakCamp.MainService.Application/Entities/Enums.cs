using System.ComponentModel;

namespace SpeakCamp.MainService.Application.Entities
{
    public enum States
    {
        Draft = 0,
        Active = 1,
        Deleted = 100,
    }

    public enum MeetingStates
    {
        Draft = 0,
        Active = 1,
        Started = 3,
        Finished = 4,
        Deleted = 100,
    }

    public enum UserTypes
    {
        Student = 1,
        Tutor = 2,
    }

    public enum Genders
    {
        Male = 1,
        Female = 2,
    }

    public enum LanguageLevels
    {
        [Description("Beginner")]
        A1 = 1,
        [Description("Elementary")]
        A2 = 2,
        [Description("Intermediate")]
        B1 = 3,
        [Description("Upper Intermediate")]
        B2 = 4,
        [Description("Advanced")]
        C1 = 5,
        [Description("Proficient")]
        C2 = 6,
    }

    public enum ImageTypes
    {
        UserPhoto = 1,
        MeetingPhoto = 2
    }

    public enum ImageSizes
    {
        Full = 1,
        Preview = 2
    }
}
