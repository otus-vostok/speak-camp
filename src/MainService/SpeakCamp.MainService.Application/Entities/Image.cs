using System;

namespace SpeakCamp.MainService.Application.Entities
{
   /// <summary>
   /// Хранит фотки для meeting, users
   /// </summary>
    public class Image : BaseEntity
    {
        public States State { get; set; }
        public Guid Key { get; set; }
        public byte[] Data { get; set; }
        public ImageTypes ImageType { get; set; }
        public ImageSizes ImageSize { get; set; }

        /// <summary>
        /// Хранит MIME тип.
        /// 
        /// TODO на будущее: Хотелось бы чистое расширение только хранить,
        /// но тогда при отправке обратно придется преобразовывать в корректный MIME. Упрощаем.
        /// </summary>
        public string ContentType { get; set; }
    }
}
