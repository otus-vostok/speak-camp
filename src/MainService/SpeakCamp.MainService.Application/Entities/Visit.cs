﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Entities
{
    /// <summary>
    /// Запись на meeting
    /// </summary>
    public class Visit : BaseEntity
    {

        public int MeetingId { get; set; }
        public Meeting Meeting { get; set; }
        public int UserId { get; set; }
        public User User { get; set; }
        public States State { get; set; }
        

    }
}
