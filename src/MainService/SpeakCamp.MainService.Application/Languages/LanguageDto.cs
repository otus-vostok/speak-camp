﻿
namespace SpeakCamp.MainService.Application.Languages
{
    public class LanguageDto
    {
        public int Id { get; set; }
        public string KeyName { get; set; }
        public string Name { get; set; }

    }
}
