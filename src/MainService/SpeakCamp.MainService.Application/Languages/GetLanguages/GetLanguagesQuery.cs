using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;

namespace SpeakCamp.MainService.Application.Languages
{
    public class GetLanguagesQuery : IQuery<ICollection<LanguageDto>>
    {
    }


    public class GetLanguagesQueryHandler : IRequestHandler<GetLanguagesQuery, ICollection<LanguageDto>>
    {
        private readonly ILanguagesRepository _languagesRepository;
        private readonly IMapper _mapper;

        public GetLanguagesQueryHandler(ILanguagesRepository languagesRepository, IMapper mapper)
        {
            _languagesRepository = languagesRepository;
            _mapper = mapper;
        }

        public async Task<ICollection<LanguageDto>> Handle(GetLanguagesQuery request, CancellationToken cancellationToken)
        {
            var languages =  await _languagesRepository.GetAllCached();

            return _mapper.Map<ICollection<LanguageDto>>(languages);

        }
    }
}
