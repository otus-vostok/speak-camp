using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Users
{
    public enum UsersSortings
    {
        CreationDateAsc = 0,
        CreationDateDesc = 1,
        RatingAsc = 2,
        RatingDesc = 3,
    }
}
