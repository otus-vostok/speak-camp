using FluentValidation;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Users.GetUsers
{
    public class GetUsersValidator : AbstractValidator<GetUsersQuery>
    {
        public GetUsersValidator()
        {

            RuleFor(p => p.UserType)
             .IsEnumName(typeof(UserTypes), false).WithMessage("{PropertyName} wrong value");


            RuleForEach(p => p.States)
              .IsEnumName(typeof(States), false).WithMessage("{PropertyName} wrong value");

            RuleFor(p => p.Sorting)
            .IsEnumName(typeof(UsersSortings), false).WithMessage("{PropertyName} wrong value");


            RuleFor(p => p.Page).GreaterThan(0).WithMessage("{PropertyName} is invalid");


            RuleFor(p => p.PageSize).GreaterThan(0).WithMessage("{PropertyName} is invalid")
                .LessThan(1000).WithMessage("{PropertyName} is invalid");

        }
    }
}
