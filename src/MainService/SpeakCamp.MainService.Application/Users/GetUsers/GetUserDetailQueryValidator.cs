using FluentValidation;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Helpers;

namespace SpeakCamp.MainService.Application.Users.GetUsers
{
    public class GetUserDetailQueryValidator : AbstractValidator<GetUserDetailQuery>
    {
        public GetUserDetailQueryValidator()
        {

            RuleFor(p => p.Id)
                .GreaterThan(0).WithMessage("{Id} wrong value");
        }
    }
}
