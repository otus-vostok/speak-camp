using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;

namespace SpeakCamp.MainService.Application.Users.GetUsers
{
    public class GetUsersQuery : IQuery<PagedResult<UserDto>>
    {
        public string UserType { get; set; }
        public string[] States { get; set; }
        public string Sorting { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }

    public class GetUsersQueryHandler : IRequestHandler<GetUsersQuery, PagedResult<UserDto>>
    {

        private readonly IUsersRepository _usersRepository;
        private readonly IMapper _mapper;

        public GetUsersQueryHandler(IUsersRepository usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }


        public async Task<PagedResult<UserDto>> Handle(GetUsersQuery request, CancellationToken cancellationToken)
        {
            var searchParams = _mapper.Map<UserSearchParams>(request);
            var usersCount = await _usersRepository.GetCount(searchParams);
            var usersPageItems =  await _usersRepository.Search(searchParams);

            return new PagedResult<UserDto> 
            {
                Count = usersCount,
                Page = request.Page,
                PageSize = request.PageSize,
                Items = _mapper.Map<IEnumerable<UserDto>>(usersPageItems)
            };
        }
    }
}
