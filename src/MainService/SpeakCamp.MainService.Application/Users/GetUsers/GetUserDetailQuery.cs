using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SpeakCamp.MainService.Application.Languages;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace SpeakCamp.MainService.Application.Users.GetUsers
{
    public class GetUserDetailQuery : IQuery<UserDetailDto>
    {
        public int Id { get; set; }
    }

    public class GetUserDetailQueryHandler : IRequestHandler<GetUserDetailQuery, UserDetailDto>
    {

        private readonly IUsersRepository _usersRepository;

        private readonly ILanguagesRepository _languagesRepository;
        
        private readonly IMapper _mapper;

        public GetUserDetailQueryHandler(IUsersRepository usersRepository, 
            ILanguagesRepository languagesRepository, 
            IMapper mapper)
        {
            _usersRepository = usersRepository;
            _languagesRepository = languagesRepository;
            _mapper = mapper;
        }

        public async Task<UserDetailDto> Handle(GetUserDetailQuery request, 
            CancellationToken cancellationToken)
        {
            var user = await _usersRepository.GetById(request.Id);
            if (user == null)
                throw new EntityNotFoundException($"User with Id '{request.Id}' not found");

            var result = new UserDetailDto();
            result.Id = user.Id;
            result.FirstName = user.FirstName;
            result.LastName = user.LastName;
            result.Description = user.Description;
            result.LanguageLevel = user.LanguageLevel.ToString();
            result.Gender = user.Gender?.ToString();
            result.BirthDate = user.BirthDate;
            result.PhotoKey = user.PhotoKey;
            result.MeetingsCount = user.MeetingsCount;
            result.State = user.State.ToString();
            result.Rating = user.Rating;
            result.TimezoneOffsetMinutes = user.TimezoneOffsetMinutes;
            result.UserType = user.UserType;
            result.CreationDate = user.CreationDate;

            var language = await _languagesRepository.GetById(user.LanguageId);
            result.Language = _mapper.Map<LanguageDto>(language);

            return result;
        }
    }
}
