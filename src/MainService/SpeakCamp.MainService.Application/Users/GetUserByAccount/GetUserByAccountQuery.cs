using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace SpeakCamp.MainService.Application.Users.GetUserByAccount
{
    public class GetUserByAccountQuery : IQuery<UserDto>
    {
        public int AccountId { get; set; }
    }

    public class GetUserByAccountQueryHandler : IRequestHandler<GetUserByAccountQuery, UserDto>
    {

        private readonly IUsersRepository _usersRepository;
        private readonly IMapper _mapper;

        public GetUserByAccountQueryHandler(IUsersRepository usersRepository, IMapper mapper)
        {
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        public async Task<UserDto> Handle(GetUserByAccountQuery request, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.GetByAccountId(request.AccountId);

            if (user == null)
                throw new EntityNotFoundException($"User with AccountId {request.AccountId} not found");

            return _mapper.Map<UserDto>(user);
          
        }
    }
}
