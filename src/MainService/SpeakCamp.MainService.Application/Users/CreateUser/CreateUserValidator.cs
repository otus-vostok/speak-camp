﻿using FluentValidation;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Users.CreateUser
{
    public class CreateUserValidator : AbstractValidator<CreateUserCommand>
    {
        public CreateUserValidator()
        {

            RuleFor(p => p.UserType)
             .NotEmpty().WithMessage("{PropertyName} is required")
             .IsEnumName(typeof(UserTypes), false).WithMessage("{PropertyName} wrong value");

            RuleFor(p => p.FirstName)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .MaximumLength(50).WithMessage("{PropertyName} length is invalid")
                .MinimumLength(1).WithMessage("{PropertyName}  length is invalid");

            RuleFor(p => p.LastName)
               .NotEmpty().WithMessage("{PropertyName} is required.")
               .MaximumLength(50).WithMessage("{PropertyName} length is invalid")
               .MinimumLength(1).WithMessage("{PropertyName}  length is invalid");

            RuleFor(p => p.LanguageLevel)
              .NotEmpty().WithMessage("{PropertyName} is required")
              .IsEnumName(typeof(LanguageLevels), false).WithMessage("{PropertyName} wrong value");

            RuleFor(p => p.LanguageKey)
              .NotEmpty().WithMessage("{PropertyName} is required.")
              .MaximumLength(2).WithMessage("{PropertyName} length is invalid")
              .MinimumLength(2).WithMessage("{PropertyName} length is invalid");

            RuleFor(p => (int)p.TimezoneOffsetMinutes)
                .GreaterThan(-12 * 60).WithMessage("{PropertyName} is invalid")
                .LessThan(14 * 60).WithMessage("{PropertyName} is invalid");

        }
    }

}
