using System;
using System.Threading.Tasks;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using System.Threading;
using SpeakCamp.MainService.Application.Common.Events;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace SpeakCamp.MainService.Application.Users.CreateUser {
    
    /// <summary>
    /// Команда на создание нового пользователя
    /// </summary>
    public class CreateUserCommand : ICommand<int> {
        public int AccountId { get; set; }
        public string UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string LanguageKey { get; set; }
        public string LanguageLevel { get; set; }
        public short TimezoneOffsetMinutes { get; set; }
    }

    public class CreateUserCommandHandler : IRequestHandler<CreateUserCommand, int>
    {
        private readonly IUsersRepository _usersRepository;
        private readonly ILanguagesRepository _languagesRepository;
        private readonly IMediator _mediator;

        public CreateUserCommandHandler(IUsersRepository usersRepository,
            ILanguagesRepository languagesRepository,
            IMediator mediator)
        {
            _usersRepository = usersRepository;
            _languagesRepository = languagesRepository;
            _mediator = mediator;
        }

        public async Task<int> Handle(CreateUserCommand request, CancellationToken cancellationToken)
        {
            if ((await _usersRepository.GetByAccountId(request.AccountId)) != null)
                throw new BusinessException($"User with AccountId '{request.AccountId}' already exists");

            var user = new User
            {
                State = States.Active,
                CreationDate = DateTime.UtcNow,
                AccountId = request.AccountId,
                UserType = Enum.Parse<UserTypes>(request.UserType, true),
                FirstName = request.FirstName,
                LastName = request.LastName,
                LanguageLevel = Enum.Parse<LanguageLevels>(request.LanguageLevel, true),
                TimezoneOffsetMinutes = request.TimezoneOffsetMinutes
            };

            var language = await _languagesRepository.GetByKey(request.LanguageKey);
            if (language == null)
                throw new EntityNotFoundException($"Language '{request.LanguageKey}' not found");
            user.LanguageId = language.Id;

            await _usersRepository.Create(user);

            _ = _mediator.Publish(new UserCreatedEvent(), cancellationToken);

            return user.Id;
        }
    }
}
