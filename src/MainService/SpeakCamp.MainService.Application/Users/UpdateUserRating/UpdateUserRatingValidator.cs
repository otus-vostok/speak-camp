// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;
using System.Data;
using FluentValidation;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Users.UpdateUserRating
{
    public class UpdateUserRatingValidator : AbstractValidator<UpdateUserRatingCommand>
    {
        public UpdateUserRatingValidator()
        {
            RuleFor(p => p.UserId).GreaterThanOrEqualTo(1).WithMessage("{AccountId}'s value is invalid");
        }
    }
}
