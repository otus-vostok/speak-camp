using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace SpeakCamp.MainService.Application.Users.UpdateUserRating
{
    public class UpdateUserRatingCommand: ICommand<int>
    {
        public int UserId { get; set; }
        public decimal Rating { get; set; }
        public int ReviewsCount { get; set; }
    }

    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserRatingCommand, int>
    {
        private readonly IUsersRepository _usersRepository;

        public UpdateUserCommandHandler(IUsersRepository usersRepository)
        {
            _usersRepository = usersRepository;
        }

        public async Task<int> Handle(UpdateUserRatingCommand command, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.GetByAccountId(command.UserId);

            if (user == null)
                throw new EntityNotFoundException($"User with UserId '{command.UserId}' not found");

            user.Rating = command.Rating;
            user.ReviewsCount = command.ReviewsCount;
            await _usersRepository.Update(user);

            return user.Id;
        }
    }
}
