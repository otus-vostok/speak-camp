using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Languages;
using System;

namespace SpeakCamp.MainService.Application.Users {
    
    public class UserDetailDto {

        // Поля общие с UserDetail
        public int Id { get; set; }
        public UserTypes UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public short TimezoneOffsetMinutes { get; set; }
        public Guid? PhotoKey { get; set; }
        public int MeetingsCount { get; set; }
        public decimal Rating { get; set; }

        public LanguageDto Language { get; set; }

        public string LanguageLevel { get; set; }

        // Дополнительные поля
        public string Description { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        public DateTime CreationDate { get; set; }
        public string State { get; set; }

    }
    
}
