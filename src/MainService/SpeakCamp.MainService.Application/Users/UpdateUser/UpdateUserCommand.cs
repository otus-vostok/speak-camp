using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Common.Exceptions;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Users.UpdateUser
{

    /// <summary>
    /// Команда на изменение пользователя
    /// </summary>
    public class UpdateUserCommand : ICommand<int>
    {

        public int AccountId { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string LanguageKey { get; set; }

        public string LanguageLevel { get; set; }

        public string Description { get; set; }

        public DateTime BirthDate { get; set; }

        public string Gender { get; set; }

        public Guid PhotoKey { get; set; }

        public short TimezoneOffsetMinutes { get; set; }

        public string Email { get; set; }
    }

    public class UpdateUserCommandHandler : IRequestHandler<UpdateUserCommand, int>
    {
        private readonly IUsersRepository _usersRepository;

        private readonly ILanguagesRepository _languagesRepository;

        public UpdateUserCommandHandler(IUsersRepository usersRepository, ILanguagesRepository languagesRepository)
        {
            _usersRepository = usersRepository;
            _languagesRepository = languagesRepository;
        }

        public async Task<int> Handle(UpdateUserCommand request, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.GetByAccountId(request.AccountId);
            if (user == null)
                throw new EntityNotFoundException($"User with AccountId '{request.AccountId}' not found");

            user.FirstName = request.FirstName.Trim();
            user.LastName = request.LastName.Trim();
            user.Description = request.Description?.Trim();
            user.LanguageLevel = Enum.Parse<LanguageLevels>(request.LanguageLevel, true);
            user.Gender = Enum.Parse<Genders>(request.Gender, true);
            user.BirthDate = request.BirthDate;
            user.PhotoKey = request.PhotoKey;
            user.Email = request.Email;

            var language = await _languagesRepository.GetByKey(request.LanguageKey);
            if (language == null)
                throw new EntityNotFoundException($"Language '{request.LanguageKey}' not found");
            user.LanguageId = language.Id;

            await _usersRepository.Update(user);

            return user.Id;
        }

    }
}
