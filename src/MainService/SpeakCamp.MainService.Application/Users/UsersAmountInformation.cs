using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Users
{
    public class UsersAmountInformation
    {
        public int StudentsAmount { get; set; }

        public int TutorsAmount { get; set; }
    }
}
