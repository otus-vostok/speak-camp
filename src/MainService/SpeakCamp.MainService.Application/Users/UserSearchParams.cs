using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Users
{
    public class UserSearchParams
    {
        public UserTypes? UserType { get; set; }
        public States[] States { get; set; }
        public UsersSortings Sorting { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
