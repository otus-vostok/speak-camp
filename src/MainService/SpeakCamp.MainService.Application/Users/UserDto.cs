﻿using SpeakCamp.MainService.Application.Languages;

namespace SpeakCamp.MainService.Application.Users {
    public class UserDto {
        public int Id { get; set; }
        public string UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public short TimezoneOffsetMinutes { get; set; }
        public string PhotoKey { get; set; }
        public int MeetingsCount { get; set; }
        public decimal Rating { get; set; }

        public LanguageDto Language { get; set; }
        public string LanguageLevel { get; set; }
    }
    
}
