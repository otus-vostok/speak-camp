using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace SpeakCamp.MainService.Application.Common.Events
{
    public class UserCreatedEvent : INotification { }
}
