using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace SpeakCamp.MainService.Application.Common.Abstractions {
    /// <summary>
    /// Интерфейс, определяющий наследника как команду
    /// </summary>
    /// <typeparam name="T">Возвращаемый тип команды</typeparam>
    public interface ICommand<T> : IRequest<T> {
    }
}
