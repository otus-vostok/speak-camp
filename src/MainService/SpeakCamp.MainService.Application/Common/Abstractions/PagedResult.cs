using System;
using System.Collections.Generic;


namespace SpeakCamp.MainService.Application.Common.Abstractions
{
    public class PagedResult<T>
    {
        public int Count { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }

        public int PagesCount  { 
            get {
                if (PageSize == 0)
                    return 0;

                return (int)Math.Ceiling((double)Count / PageSize); 
            } }

        public IEnumerable<T> Items { get; set; }

        public PagedResult()
        {
            PageSize = 1;
        }
    }
}
