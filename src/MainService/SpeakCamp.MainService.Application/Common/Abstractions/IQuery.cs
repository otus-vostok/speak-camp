using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;

namespace SpeakCamp.MainService.Application.Common.Abstractions {
    /// <summary>
    /// Интерфейс, определяющий наследника как запрос
    /// </summary>
    /// <typeparam name="T">Возвращаемый тип запроса</typeparam>
    public interface IQuery<T> : IRequest<T> {
    }
}
