using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Meetings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Common.Abstractions
{
    public interface IMeetingsRepository :  IRepository<Meeting>
    {

        Task<Meeting> GetByIdDetailed(int id);
        Task<ICollection<Meeting>> Search(MeetingSearchParams searchParams);
        Task<int> GetCount(MeetingFilterParams filterParams);
    }
}
