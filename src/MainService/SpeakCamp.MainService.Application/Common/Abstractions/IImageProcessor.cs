using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Common.Abstractions
{
    /// <summary>
    /// Интерфейс для объекта, занимающегося
    /// обработкой изображений
    /// </summary>
    public interface IImageProcessor
    {
        Task<Dictionary<ImageSizes, byte[]>> ProcessAsync(ImageTypes imageType, byte[] file);
    }
}
