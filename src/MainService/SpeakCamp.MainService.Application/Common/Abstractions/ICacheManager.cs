using System;

namespace SpeakCamp.MainService.Application.Common.Abstractions
{
    public interface  ICacheManager
    {
        T Get<T>(string key);
        void Set<T>(string key, T item, TimeSpan absoluteExpiration);
    }
}
