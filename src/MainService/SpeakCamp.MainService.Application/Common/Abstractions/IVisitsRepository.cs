using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Visits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Common.Abstractions
{
    public interface IVisitsRepository : IRepository<Visit>
    {

        Task<ICollection<Visit>> GetForMeeting(int meetingId);
        Task<ICollection<Visit>> Search(VisitSearchParams searchParams);
        Task<int> GetCount(int? userId, States[] states);
    }
}
