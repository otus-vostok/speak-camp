using SpeakCamp.MainService.Application.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Common.Abstractions
{
    public interface IRepository<T>
       where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> GetById(int id);
        Task<IEnumerable<T>> GetByIds(int[] ids);
        Task Create(T entity);
        Task CreateMany(IEnumerable<T> entities);
        Task Update(T entity);
        Task Delete(T entity);
        Task<bool> CheckExists(Expression<Func<T, bool>> expr);
    }
}
