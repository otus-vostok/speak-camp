using SpeakCamp.MainService.Application.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Common.Abstractions
{
    public interface ILanguagesRepository : IRepository<Language>
    {
        Task<Language> GetByKey(string key);
        Task<ICollection<Language>> GetAllCached();
    }
}
