using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Users;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Common.Abstractions
{
    public interface IUsersRepository : IRepository<User>
    {
        Task<User> GetByAccountId(int accountId);
        Task<ICollection<User>> Search(UserSearchParams searchParams);
        Task<int> GetCount(UserSearchParams searchParams);
        Task<IEnumerable<(UserTypes userType, int amount)>> GetTypesCount();
    }
}
