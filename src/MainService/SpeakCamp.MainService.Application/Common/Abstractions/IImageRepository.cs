using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Images.GetImage;

namespace SpeakCamp.MainService.Application.Common.Abstractions
{
    public interface IImageRepository : IRepository<Image>
    {
        Task<Image> GetImageByParamsAsync(ImageSearchParams searchParams);
    }
}
