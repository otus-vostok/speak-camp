using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Common.Helpers
{
    public static class ImageHelper
    {
        /// <summary>
        /// Замена табличным вариантам в БД.
        /// </summary>
        public static Dictionary<ImageTypes, Dictionary<ImageSizes, ImageConfiguration>> ImageTypesImageSizes
            => new()
            {
                [ImageTypes.UserPhoto] = new Dictionary<ImageSizes, ImageConfiguration> {
                    [ImageSizes.Full] = new ImageConfiguration { Width = 200, Height = 200 },
                    [ImageSizes.Preview] = new ImageConfiguration { Width = 60, Height = 60 },
                },
                [ImageTypes.MeetingPhoto] = new Dictionary<ImageSizes, ImageConfiguration> {
                    [ImageSizes.Full] = new ImageConfiguration { Width = 762, Height = 450 },
                    [ImageSizes.Preview] = new ImageConfiguration { Width = 352, Height = 200 },
                }
            };
    }

    public struct ImageConfiguration
    {
        public int Width { get; init; }
        public int Height { get; init; }
    }
}
