using FluentValidation;
using SpeakCamp.MainService.Application.Meetings.DeleteMeeting;

namespace SpeakCamp.MainService.Application.Meetings.GetUserMeetings
{
    public class GetUserMeetingsQueryValidator : AbstractValidator<GetUserMeetingsQuery>
    {
        public GetUserMeetingsQueryValidator()
        {
            RuleFor(x => x.UserId)
                .GreaterThan(0).WithMessage("{UserId} is invalid");

            RuleFor(x => x.Page)
                .GreaterThan(0).WithMessage("{Page} is invalid");

            RuleFor(x => x.PageSize)
                .GreaterThan(0).LessThanOrEqualTo(10000).WithMessage("{PageSize} is invalid");

           
        }
    }
}
