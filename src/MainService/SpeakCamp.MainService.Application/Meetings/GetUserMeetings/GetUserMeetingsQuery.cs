using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;


namespace SpeakCamp.MainService.Application.Meetings.GetUserMeetings
{
    public class GetUserMeetingsQuery : IQuery<PagedResult<MeetingDto>>
    {
        public int UserId { get; set; }
        public string[] States { get; set; }
        public string Sorting { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }

    public class GetUserMeetingsQueryHandler : IRequestHandler<GetUserMeetingsQuery, PagedResult<MeetingDto>>
    {

        private readonly IMeetingsRepository _meetingsRepository;
        private readonly IMapper _mapper;

        public GetUserMeetingsQueryHandler(IMeetingsRepository meetingsRepository, IMapper mapper)
        {
            _meetingsRepository = meetingsRepository;
            _mapper = mapper;
        }

        public async Task<PagedResult<MeetingDto>> Handle(GetUserMeetingsQuery request, CancellationToken cancellationToken)
        {
            var searchParams = _mapper.Map<MeetingSearchParams>(request);
            var count = await _meetingsRepository.GetCount(searchParams);
            var items = await _meetingsRepository.Search(searchParams);

            return new PagedResult<MeetingDto>
            {
                Count = count,
                Page = request.Page,
                PageSize = request.PageSize,
                Items = _mapper.Map<IEnumerable<MeetingDto>>(items)
            };
        }
    }
}
