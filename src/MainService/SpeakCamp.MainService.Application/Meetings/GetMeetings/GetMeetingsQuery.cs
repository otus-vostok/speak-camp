using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;

namespace SpeakCamp.MainService.Application.Meetings.GetMeetings
{
    public class GetMeetingsQuery : IQuery<PagedResult<MeetingDto>>
    {
        public string LanguageKey { get; set; }
        public string LanguageLevel { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string[] States { get; set; }
        public string Sorting { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }


    public class GetMeetingsQueryHandler : IRequestHandler<GetMeetingsQuery, PagedResult<MeetingDto>>
    {

        private readonly IMeetingsRepository _meetingsRepository;
        private readonly IMapper _mapper;

        public GetMeetingsQueryHandler(IMeetingsRepository meetingsRepository, IMapper mapper)
        {
            _meetingsRepository = meetingsRepository;
            _mapper = mapper;
        }

        public async Task<PagedResult<MeetingDto>> Handle(GetMeetingsQuery request, CancellationToken cancellationToken)
        {
            var searchParams = _mapper.Map<MeetingSearchParams>(request);
            var count = await _meetingsRepository.GetCount(searchParams);
            var items = await _meetingsRepository.Search(searchParams);

            return new PagedResult<MeetingDto>
            {
                Count = count,
                Page = request.Page,
                PageSize = request.PageSize,
                Items = _mapper.Map<IEnumerable<MeetingDto>>(items)
            };
        }
    }
}
