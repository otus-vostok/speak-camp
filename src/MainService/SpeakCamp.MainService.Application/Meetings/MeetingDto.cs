using System;
using SpeakCamp.MainService.Application.Languages;
using SpeakCamp.MainService.Application.Users;

namespace SpeakCamp.MainService.Application.Meetings
{
    public  class MeetingDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string State { get; set; }
        public string PhotoKey { get; set; }
        public string StartDate { get; set; }
        public int DurationMinutes { get; set; }
        public int PlacesLimit { get; set; }
        public int PlacesOccupied { get; set; }
        public string LanguageLevel { get; set; }

        public LanguageDto Language { get; set; }
        public UserDto User { get; set; }

    }
}
