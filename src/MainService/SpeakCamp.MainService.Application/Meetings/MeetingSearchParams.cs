using System;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Meetings
{
    public class MeetingFilterParams
    {
        public int? UserId { get; set; }
        public MeetingStates[] States { get; set; }
        public string LanguageKey { get; set; }
        public LanguageLevels? LanguageLevel { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
    }

    public class MeetingSearchParams : MeetingFilterParams
    {
        public MeetingSortings Sorting { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
