using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;

namespace SpeakCamp.MainService.Application.Meetings.StartMeeting
{
    public class StartMeetingCommandValidator : AbstractValidator<StartMeetingCommand>
    {
        public StartMeetingCommandValidator()
        {
            RuleFor(x => x.ConferenceLink)
                .NotEmpty()
                .WithMessage("The {PropertyName} is required.")
                .DependentRules(() =>
                {
                    RuleFor(x => x.ConferenceLink)
                        .Custom((x, context) =>
                        {
                            var isValidConferenceLink = Uri.TryCreate(x, UriKind.Absolute, out var uri) &&
                                                        (uri.Scheme == Uri.UriSchemeHttp ||
                                                         uri.Scheme == Uri.UriSchemeHttps);

                            if (!isValidConferenceLink) context.AddFailure("The conference link is not valid");
                        });
                });
        }
    }
}
