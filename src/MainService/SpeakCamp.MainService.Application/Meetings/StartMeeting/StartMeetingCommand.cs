using System;
using System.Threading;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Exceptions;
using MediatR;

namespace SpeakCamp.MainService.Application.Meetings.StartMeeting
{
    public class StartMeetingCommand : ICommand<int>
    {
        public int AccountId { get; set; }
        public int MeetingId { get; set; }
        public string ConferenceLink { get; set; }
        public string Note { get; set; }
    }

    public class StartMeetingCommandHandler : IRequestHandler<StartMeetingCommand, int>
    {
        private readonly IMeetingsRepository _meetingsRepository;
        private readonly IUsersRepository _usersRepository;

        public StartMeetingCommandHandler(IMeetingsRepository meetingsRepository,
            IUsersRepository usersRepository)
        {
            _meetingsRepository = meetingsRepository;
            _usersRepository = usersRepository;
        }

        public async Task<int> Handle(StartMeetingCommand request, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.GetByAccountId(request.AccountId);

            if (user == null)
                throw new EntityNotFoundException($"User with AccountId '{request.AccountId}' doesn't exist");

            var meeting = await _meetingsRepository.GetById(request.MeetingId);

            if (meeting == null)
                throw new EntityNotFoundException($"Meeting with Id={request.MeetingId} not found");

            if (meeting.UserId != user.Id)
                throw new BusinessException("Only author can start the meeting");

            if (meeting.State != MeetingStates.Active)
                throw new BusinessException("Only Active meetings can be started");

            if (meeting.StartDate - TimeSpan.FromMinutes(30) > DateTime.UtcNow)
                throw new BusinessException("The meeting could be started on 30 minutes before the planned date");

            if (DateTime.UtcNow > meeting.StartDate)
                throw new BusinessException("The meeting cannot be started after the planned date");

            meeting.ConferenceLink = request.ConferenceLink;
            meeting.ConferenceNotes = request.Note;

            meeting.State = MeetingStates.Started;
            await _meetingsRepository.Update(meeting);

            return meeting.Id;
        }
    }
}
