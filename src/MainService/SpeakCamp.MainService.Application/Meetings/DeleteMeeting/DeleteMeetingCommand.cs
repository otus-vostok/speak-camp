using System.Threading;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Exceptions;
using MediatR;

namespace SpeakCamp.MainService.Application.Meetings.DeleteMeeting
{
    public class DeleteMeetingCommand : ICommand<bool>
    {
        public int AccountId { get; set; }
        public int MeetingId { get; set; }
    }

    public class DeleteMeetingCommandHandler : IRequestHandler<DeleteMeetingCommand, bool>
    {
        private readonly IMeetingsRepository _meetingsRepository;
        private readonly IUsersRepository _usersRepository;

        public DeleteMeetingCommandHandler(IMeetingsRepository meetingsRepository, IUsersRepository usersRepository)
        {
            _meetingsRepository = meetingsRepository;
            _usersRepository = usersRepository;
        }

        public async Task<bool> Handle(DeleteMeetingCommand request, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.GetByAccountId(request.AccountId);

            if (user == null)
                throw new EntityNotFoundException($"User with AccountId={request.AccountId} not found");

            var meeting = await _meetingsRepository.GetById(request.MeetingId);

            if (meeting == null)
                throw new EntityNotFoundException($"Meeting with Id={request.MeetingId} not found");

            if (meeting.UserId != user.Id)
                throw new BusinessException("You don't have enough rights to remove the meeting");

            if (meeting.State == MeetingStates.Started)
                throw new BusinessException("Deleting ongoing meeting is not allowed");

            if (meeting.State == MeetingStates.Deleted)
                return false;

            meeting.State = MeetingStates.Deleted;
            await _meetingsRepository.Update(meeting);

            return true;
        }
    }
}
