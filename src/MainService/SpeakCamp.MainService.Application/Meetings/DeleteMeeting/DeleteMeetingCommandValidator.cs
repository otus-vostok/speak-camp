using System;
using FluentValidation;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Meetings.DeleteMeeting
{
    public class DeleteMeetingCommandValidator : AbstractValidator<DeleteMeetingCommand>
    {
        public DeleteMeetingCommandValidator()
        {
            RuleFor(x => x.AccountId)
                .GreaterThan(0).WithMessage("{AccountId} is invalid");

            RuleFor(x => x.MeetingId)
                .GreaterThan(0).WithMessage("{MeetingId} is invalid");
        }
    }
}
