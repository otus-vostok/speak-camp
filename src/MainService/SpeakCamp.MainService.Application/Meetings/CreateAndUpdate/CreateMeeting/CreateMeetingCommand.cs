using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;

namespace SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.CreateMeeting
{


    public abstract class BaseMeetingCommand : ICommand<int> {
        public int AccountId { get; set; }
        public string Name { get; set; }
        public DateTime StartDateUtc { get; set; }
        public int DurationMinutes { get; set; }
        public string LanguageKey { get; set; }
        public string LanguageLevel { get; set; }
        public int PlacesLimit { get; set; }
        public string Description { get; set; }
        public bool IsDraft { get; set; }
        public Guid? PhotoKey { get; set; }
    }
    /// <summary>
    /// Команда на создание новой встречи
    /// </summary>
    public class CreateMeetingCommand : BaseMeetingCommand
    {
    }

    public class CreateMeetingCommandHandler : IRequestHandler<CreateMeetingCommand, int>
    {
        private readonly IMeetingModelFactory _meetingCreator;
        private readonly IMeetingsRepository _meetingsRepository;

        public CreateMeetingCommandHandler(IMeetingsRepository meetingsRepository, IMeetingModelFactory meetingCreator)
        {
            _meetingsRepository = meetingsRepository;
            _meetingCreator = meetingCreator;
        }

        public async Task<int> Handle(CreateMeetingCommand request, CancellationToken cancellationToken)
        {
            var meeting = await _meetingCreator.Create(request);
            await _meetingsRepository.Create(meeting);
            return meeting.Id;
        }
    }
}
