using System;
using FluentValidation;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.CreateMeeting;

namespace SpeakCamp.MainService.Application.Meetings.CreateAndUpdate
{
    public abstract class BaseMeetingValidator<T> : AbstractValidator<T> where T : BaseMeetingCommand
    {
        public BaseMeetingValidator()
        {
            RuleFor(p => p.Name)
                .NotEmpty()
                .WithMessage("{PropertyName} is not assigned");

            RuleFor(p => p.LanguageKey)
                .NotEmpty().WithMessage("{PropertyName} is required.")
                .MaximumLength(2).WithMessage("{PropertyName} length is invalid")
                .MinimumLength(2).WithMessage("{PropertyName} length is invalid");

            RuleFor(p => p.LanguageLevel)
                .NotEmpty().WithMessage("{PropertyName} is required")
                .IsEnumName(typeof(LanguageLevels), false).WithMessage("{PropertyName} is invalid");

            RuleFor(p => p.PlacesLimit)
                .GreaterThan(0).WithMessage("{PropertyName} <= 0")
                .LessThan(30).WithMessage("{PropertyName} should not be greater than 30");

            RuleFor(x => x.StartDateUtc)
                .Must(x => x > DateTime.UtcNow)
                .WithMessage("The {PropertyName} is less then the current time.");

            RuleFor(p => p.DurationMinutes)
                .InclusiveBetween(10, 4 * 60)
                .WithMessage("{PropertyName} must be >= 10 min and <=4 hours");
        }
    }
}
