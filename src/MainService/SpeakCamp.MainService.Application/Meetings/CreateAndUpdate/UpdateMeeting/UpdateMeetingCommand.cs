using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Common.Exceptions;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.CreateMeeting;

namespace SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.UpdateMeeting {

    /// <summary>
    /// Команда на обновление встречи
    /// </summary>
    public class UpdateMeetingCommand : BaseMeetingCommand
    {
        public int Id { get; set; }
    }

    public class UpdateMeetingCommandHandler : IRequestHandler<UpdateMeetingCommand, int>
    {
        private readonly IMeetingModelFactory _meetingCreator;
        private readonly IMeetingsRepository _meetingsRepository;

        public UpdateMeetingCommandHandler(IMeetingsRepository meetingsRepository, IMeetingModelFactory meetingCreator)
        {
            _meetingsRepository = meetingsRepository;
            _meetingCreator = meetingCreator;
        }

        public async Task<int> Handle(UpdateMeetingCommand request, CancellationToken cancellationToken)
        {
            var entity = await _meetingsRepository.GetById(request.Id);

            if (entity == null)
                throw new BusinessException($"Meeting with id '{request.Id}' doesn't exist");

            if (entity.UserId != request.AccountId)
                throw new BusinessException ("Access is denied");

            if (entity.State != MeetingStates.Active)
                throw new BusinessException ("The meeting is not active");

            if ( entity.PlacesOccupied != 0)
                throw new BusinessException ("The meeting is not empty");


            var meeting = await _meetingCreator.Update(entity, request);
            await _meetingsRepository.Update(entity);
            return meeting.Id;
        }
    }
}
