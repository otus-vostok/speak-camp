using FluentValidation;

namespace SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.UpdateMeeting
{
    public class UpdateMeetingValidator : BaseMeetingValidator<UpdateMeetingCommand>
    {
        public UpdateMeetingValidator()
        {
            RuleFor(p => p.Id)
                .GreaterThan(0).WithMessage("{Id} <= 0");
        }
    }
}
