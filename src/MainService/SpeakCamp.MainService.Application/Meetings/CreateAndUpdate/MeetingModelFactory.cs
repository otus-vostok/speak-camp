// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com

using System;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Common.Exceptions;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.CreateMeeting;
using SpeakCamp.MainService.Application.Meetings.CreateAndUpdate.UpdateMeeting;

namespace SpeakCamp.MainService.Application.Meetings.CreateAndUpdate
{
    public interface IMeetingModelFactory
    {
        Task<Meeting> Create(CreateMeetingCommand request);
        Task<Meeting> Update(Meeting entity, UpdateMeetingCommand request);
    }

    public class MeetingModelFactory : IMeetingModelFactory
    {
        private readonly IUsersRepository _usersRepository;
        private readonly ILanguagesRepository _languagesRepository;
        private readonly IImageRepository _imageRepository;

        public MeetingModelFactory(IUsersRepository usersRepository, ILanguagesRepository languagesRepository, IImageRepository imageRepository)
        {
            _usersRepository = usersRepository;
            _languagesRepository = languagesRepository;
            _imageRepository = imageRepository;
        }

        public async Task<Meeting> Create(CreateMeetingCommand request) => await CreateModelFromRequestWithChecks(request);

        public async Task<Meeting> Update(Meeting entity, UpdateMeetingCommand request)
        {
            var updates = await CreateModelFromRequestWithChecks(request);
            return UpdateModel(entity, updates);
        }


        private async Task<Meeting> CreateModelFromRequestWithChecks(BaseMeetingCommand request)
        {
            if (request.StartDateUtc - TimeSpan.FromHours(1) < DateTime.UtcNow)
                throw new BusinessException("The meeting couldn't be arranged later then 1 hour before the start date");

            var user = await _usersRepository.GetByAccountId(request.AccountId);
            if (user == null)
                throw new EntityNotFoundException($"User with AccountId '{request.AccountId}' doesn't exist");

            if (user.UserType == UserTypes.Student)
                throw new BusinessException($"Students can't create their own meetings. Please register as a Tutor");

            var language = await _languagesRepository.GetByKey(request.LanguageKey);
            if (language == null)
                throw new EntityNotFoundException($"Language '{request.LanguageKey}' is not found");

            if (request.PhotoKey.HasValue)
            {
                var imageExist = await _imageRepository.CheckExists(x => x.Key == request.PhotoKey.Value);

                if (!imageExist)
                    throw new EntityNotFoundException("Meeting image not found");
            }

            var meetingState = request.IsDraft
                ? MeetingStates.Draft
                : MeetingStates.Active;

            var meeting = CreateModel(meetingState, user, request, language);
            return meeting;
        }

        private Meeting CreateModel(MeetingStates meetingState, User user, BaseMeetingCommand request, Language language) => new Meeting
        {
            State = meetingState,
            UserId = user.Id,
            Name = request.Name,
            StartDate = request.StartDateUtc,
            CreationDate = DateTime.UtcNow,
            DurationMinutes = request.DurationMinutes,
            LanguageId = language.Id,
            LanguageLevel = Enum.Parse<LanguageLevels>(request.LanguageLevel, true),
            PlacesLimit = request.PlacesLimit,
            Description = request.Description,
            PhotoKey = request.PhotoKey
        };

        private Meeting UpdateModel(Meeting entity, Meeting updates)
        {
            entity.State = updates.State;
            entity.Name = updates.Name;
            entity.StartDate = updates.StartDate;
            entity.CreationDate = updates.CreationDate;
            entity.DurationMinutes = updates.DurationMinutes;
            entity.LanguageId = updates.LanguageId;
            entity.LanguageLevel = updates.LanguageLevel;
            entity.PlacesLimit = updates.PlacesLimit;
            entity.Description = updates.Description;
            entity.PhotoKey = updates.PhotoKey;

            return entity;
        }


    }
}
