
namespace SpeakCamp.MainService.Application.Meetings
{
    public enum MeetingSortings
    {
        Popularity = 0,
        StartDate = 1,
        Rating = 2,
    }
}
