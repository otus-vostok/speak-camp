using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Users;

namespace SpeakCamp.MainService.Application.Meetings.GetMeetingDetails
{
    public class MeetingVisitDto
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int MeetingId { get; set; }
        public string State { get; set; }

        public UserDto User { get; set; }
    }
}
