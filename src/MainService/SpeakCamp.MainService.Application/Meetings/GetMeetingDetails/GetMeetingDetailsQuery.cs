using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace SpeakCamp.MainService.Application.Meetings.GetMeetingDetails
{
    public class GetMeetingDetailsQuery : IQuery<MeetingDetailsDto>
    {
        public int Id { get; set; }
      
    }

    public class GetMeetingDetailsQueryHandler : IRequestHandler<GetMeetingDetailsQuery, MeetingDetailsDto>
    {

        private readonly IMeetingsRepository _meetingsRepository;
        private readonly IVisitsRepository _visitsRepository;
        private readonly IMapper _mapper;

        public GetMeetingDetailsQueryHandler(IMeetingsRepository meetingsRepository, IVisitsRepository visitsRepository, IMapper mapper)
        {
            _meetingsRepository = meetingsRepository;
            _visitsRepository = visitsRepository;
            _mapper = mapper;
        }

        public async Task<MeetingDetailsDto> Handle(GetMeetingDetailsQuery request, CancellationToken cancellationToken)
        {
            var meeting = await _meetingsRepository.GetByIdDetailed(request.Id);

            if (meeting == null)
                throw new EntityNotFoundException($"Meeting with id {request.Id} not found"); // todo: use  EntityNotFoundExcption

            var res = _mapper.Map<MeetingDetailsDto>(meeting);
            res.Visits = _mapper.Map<ICollection<MeetingVisitDto>>(await _visitsRepository.GetForMeeting(request.Id));

            return res;
        }
    }
}
