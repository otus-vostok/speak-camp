using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using SpeakCamp.MainService.Application.Common.Abstractions;
using MediatR;

namespace SpeakCamp.MainService.Application.Visits.GetUserVisits
{
    public class GetUserVisitsQuery : IQuery<PagedResult<VisitDto>>
    {
        public int UserId { get; set; }
        public string[] States { get; set; }
        public string Sorting { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }

    public class GetUserVisitsQueryHandler : IRequestHandler<GetUserVisitsQuery, PagedResult<VisitDto>>
    {

        private readonly IVisitsRepository _visitsRepository;
        private readonly IMapper _mapper;

        public GetUserVisitsQueryHandler(IVisitsRepository visitsRepository, IMapper mapper)
        {
            _visitsRepository = visitsRepository;
            _mapper = mapper;
        }

        public async Task<PagedResult<VisitDto>> Handle(GetUserVisitsQuery request, CancellationToken cancellationToken)
        {
            var searchParams = _mapper.Map<VisitSearchParams>(request);
            var count = await _visitsRepository.GetCount(searchParams.UserId, searchParams.States);
            var items = await _visitsRepository.Search(searchParams);

            return new PagedResult<VisitDto>
            {
                Count = count,
                Page = request.Page,
                PageSize = request.PageSize,
                Items = _mapper.Map<IEnumerable<VisitDto>>(items)
            };
        }
    }
}
