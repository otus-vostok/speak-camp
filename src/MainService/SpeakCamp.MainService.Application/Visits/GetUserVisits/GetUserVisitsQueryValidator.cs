using FluentValidation;

namespace SpeakCamp.MainService.Application.Visits.GetUserVisits
{
    public class GetUserVisitsQueryValidator : AbstractValidator<GetUserVisitsQuery>
    {
        public GetUserVisitsQueryValidator()
        {
            RuleFor(p => p.UserId)
                .GreaterThan(0).WithMessage("{UserId} is invalid");

            RuleFor(p => p.Page)
                .GreaterThan(0).WithMessage("{Page} is invalid");

            RuleFor(p => p.PageSize)
                .GreaterThan(0).LessThanOrEqualTo(10000).WithMessage("{PageSize} is invalid");

            RuleFor(p => p.States)
                .NotEmpty().WithMessage("{States} is invalid");

            RuleFor(p => p.Sorting)
                .NotEmpty().WithMessage("{Sorting} is invalid");
        }
    }
}
