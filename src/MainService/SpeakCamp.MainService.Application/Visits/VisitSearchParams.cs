using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Visits
{
    public class VisitSearchParams
    {
        public int? UserId { get; set; }
        public States[] States { get; set; }
        public VisitSortings Sorting { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }
}
