using FluentValidation;

namespace SpeakCamp.MainService.Application.Visits.DeleteVisit
{
    public class DeleteVisitCommandValidator : AbstractValidator<DeleteVisitCommand>
    {
        public DeleteVisitCommandValidator()
        {
            RuleFor(p => p.AccountId)
                .NotEmpty().WithMessage("{AccountId} is invalid");

            RuleFor(p => p.VisitId)
                .NotEmpty().WithMessage("{VisitId} is invalid");
        }
    }
}
