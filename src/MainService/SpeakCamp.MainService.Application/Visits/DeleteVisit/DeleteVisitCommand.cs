using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Common.Exceptions;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Visits.DeleteVisit
{
    /// <summary>
    /// Отмена записи на встречу
    /// </summary>
    public class DeleteVisitCommand : ICommand<bool>
    {
        public int AccountId { get; set; }
        public int VisitId { get; set; }
    }

    public class DeleteVisitCommandHandler : IRequestHandler<DeleteVisitCommand, bool>
    {

        private readonly IMeetingsRepository _meetingsRepository;
        private readonly IUsersRepository _usersRepository;
        private readonly IVisitsRepository _visitsRepository;

        public DeleteVisitCommandHandler(IMeetingsRepository meetingsRepository,
            IUsersRepository usersRepository,
            IVisitsRepository visitsRepository
            )
        {

            _meetingsRepository = meetingsRepository;
            _usersRepository = usersRepository;
            _visitsRepository = visitsRepository;

        }

        public async Task<bool> Handle(DeleteVisitCommand request, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.GetByAccountId(request.AccountId);
            var visit = await _visitsRepository.GetById(request.VisitId);

            if (user == null)
                throw new EntityNotFoundException($"User with AccountId={request.AccountId} not found");

            if (visit == null)
                throw new EntityNotFoundException($"Visit with Id={request.VisitId} not found");

            var meeting = await _meetingsRepository.GetByIdDetailed(visit.MeetingId);
            if (meeting == null)
                throw new EntityNotFoundException($"Meeting with Id={visit.MeetingId} not found");

            if (visit.UserId != user.Id)
                throw new BusinessException("You can't cancel visit for another user");

            if (meeting.State != MeetingStates.Active)
                throw new BusinessException($"You can't cancel visit for meeting in {meeting.State} state");

            if (visit.State !=  States.Active)
                return false;

            visit.State = States.Deleted;

            if (meeting.PlacesOccupied > 0)
                meeting.PlacesOccupied--;

            await _meetingsRepository.Update(meeting);
            await _visitsRepository.Update(visit);
            
            return true;
        }
    }
}
