using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Meetings;

namespace SpeakCamp.MainService.Application.Visits
{
    public class VisitDto
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int UserId { get; set; }
        public string State { get; set; }

        public MeetingDto Meeting { get; set; }


    }
}
