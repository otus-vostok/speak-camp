using System;
using FluentValidation;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Visits.CreateVisit
{
    public class CreateVisitValidator : AbstractValidator<CreateVisitCommand>
    {
        public CreateVisitValidator()
        {
            RuleFor(p => p.AccountId)
                .GreaterThan(0).WithMessage("{AccountId} is invalid");

            RuleFor(p => p.MeetingId)
                .GreaterThan(0).WithMessage("{MeetingId} is invalid");
        }
    }
}
