using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using SpeakCamp.MainService.Application.Common.Exceptions;
using MediatR;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SpeakCamp.MainService.Application.Visits.CreateVisit
{
    /// <summary>
    /// Создание записи на встречу
    /// </summary>
    public class CreateVisitCommand : ICommand<int>
    {
        public int AccountId { get; set; }
        public int MeetingId { get; set; }
    }

    public class CreateVisitCommandHandler : IRequestHandler<CreateVisitCommand, int>
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IMeetingsRepository _meetingsRepository;


        public CreateVisitCommandHandler(IUsersRepository usersRepository, IMeetingsRepository meetingsRepository)
        {
            _usersRepository = usersRepository;
            _meetingsRepository = meetingsRepository;

        }

        public async Task<int> Handle(CreateVisitCommand request, CancellationToken cancellationToken)
        {

            var user = await _usersRepository.GetByAccountId(request.AccountId);
            var meeting = await _meetingsRepository.GetByIdDetailed(request.MeetingId);

            if (user == null)
                throw new EntityNotFoundException($"User with AccountId={request.AccountId} not found");
            if (meeting == null)
                throw new EntityNotFoundException($"Meeting with Id={request.MeetingId} not found");

            if (meeting.State != MeetingStates.Active)
                throw new BusinessException("You can join only Active meetings");

            if (meeting.UserId == user.Id)
                throw new BusinessException("You can't join your own meeting");

            if (meeting.LanguageId != user.LanguageId || (int)meeting.LanguageLevel > (int)user.LanguageLevel)
                throw new BusinessException("Meeting language/level is not suitable for you");

            if (meeting.Visits.Any(o => o.UserId == user.Id && o.State == States.Active))
                throw new BusinessException("You have already joined this meeting");

            if (meeting.PlacesOccupied >= meeting.PlacesLimit)
                throw new BusinessException("This meeting doesn't have free places");

            var visit = new Visit
            {
                CreationDate = DateTime.UtcNow, // todo: from dates service
                UserId = user.Id,
                State = States.Active
            };

            meeting.Visits.Add(visit);
            meeting.PlacesOccupied++;

            await _meetingsRepository.Update(meeting);

            return visit.Id;
        }
    }
}
