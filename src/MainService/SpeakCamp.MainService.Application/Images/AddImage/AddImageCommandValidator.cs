using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Images.AddImage
{
    public class AddImageCommandValidator : AbstractValidator<AddImageCommand>
    {
        public AddImageCommandValidator()
        {
            RuleFor(x => x.ImageType)
                .IsEnumName(typeof(ImageTypes)).WithMessage("{PropertyName} with value {PropertyValue} doesn't exist");
        }
    }
}
