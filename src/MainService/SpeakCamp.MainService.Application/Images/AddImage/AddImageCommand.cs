using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using MediatR;
using FluentValidation;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace SpeakCamp.MainService.Application.Images.AddImage
{
    public class AddImageCommand : ICommand<Guid>
    {
        public string ImageType { get; init; }
        public byte[] File { get; init; }
        public string ContentType { get; init; }
    }

    public class AddImageCommandHandler : IRequestHandler<AddImageCommand, Guid>
    {
        private readonly IImageProcessor _imageProcessor;

        private readonly IValidator<AddImageCommand> _validator;

        private readonly IRepository<Image> _mediaRepository;

        public AddImageCommandHandler(
            IImageProcessor imageProcessor,
            IValidator<AddImageCommand> validator,
            IRepository<Image> mediaRepository)
        {
            _imageProcessor = imageProcessor;
            _validator = validator;
            _mediaRepository = mediaRepository;
        }


        public async Task<Guid> Handle(AddImageCommand request, CancellationToken cancellationToken)
        {
           
            var imageType = Enum.Parse<ImageTypes>(request.ImageType);

            var processedImages = await _imageProcessor.ProcessAsync(imageType, request.File);

            var newKey = Guid.NewGuid();

            var imagesForSaving = processedImages.Select(x => new Image
                {
                    State = States.Active,
                    Key = newKey,
                    Data = x.Value,
                    ImageType = imageType,
                    ImageSize = x.Key,
                    ContentType = request.ContentType
                });

            await _mediaRepository.CreateMany(imagesForSaving);

            return newKey;
        }
    }
}
