using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Images.GetImage
{
    public class ImageSearchParams
    {
        public Guid Key { get; init; }
        public ImageSizes ImageSize { get; init; }
    }
}
