using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Entities;
using MediatR;
using FluentValidation;
using SpeakCamp.MainService.Application.Common.Exceptions;

namespace SpeakCamp.MainService.Application.Images.GetImage
{
    public class GetImageQuery : IQuery<Image>
    {
        public Guid Key { get; init; }
        public string ImageSize { get; init; }
    }

    public class GetImageQueryHandler : IRequestHandler<GetImageQuery, Image>
    {
        private readonly IImageRepository _imageRepository;

        private readonly IValidator<GetImageQuery> _validator;

        public GetImageQueryHandler(
            IValidator<GetImageQuery> validator,
            IImageRepository imageRepository)
        {
            _imageRepository = imageRepository;
            _validator = validator;
        }

        public async Task<Image> Handle(GetImageQuery request, CancellationToken cancellationToken)
        {
           var imageSize = Enum.Parse<ImageSizes>(request.ImageSize);

            var searchParams = new ImageSearchParams
            {
                Key = request.Key,
                ImageSize = imageSize
            };

            return await _imageRepository.GetImageByParamsAsync(searchParams);
        }
    }
}
