using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation;
using SpeakCamp.MainService.Application.Entities;

namespace SpeakCamp.MainService.Application.Images.GetImage
{
    public class GetImageQueryValidator : AbstractValidator<GetImageQuery>
    {
        public GetImageQueryValidator()
        {
            RuleFor(x => x.ImageSize)
                .IsEnumName(typeof(ImageSizes)).WithMessage("{PropertyName} with value {PropertyValue} doesn't exist");
        }
    }
}
