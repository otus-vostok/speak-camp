﻿using System;
using SpeakCamp.MainService.Application.Users;

namespace SpeakCamp.MainService.Application.Reviews.GetReviews
{
    public class ReviewDto
    {
        public Guid Id { get; set; }
        public int TutorId { get; set; }
        public string Body { get; set; }
        public DateTime CreationDate { get; set; }
        public uint Rating { get; set; }
        public UserDto User { get; set; }
    }
}
