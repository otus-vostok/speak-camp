﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Gateways;
using SpeakCamp.MainService.Application.Users;

namespace SpeakCamp.MainService.Application.Reviews.GetReviews
{
    public class GetReviewsQuery : IQuery<PagedResult<ReviewDto>>
    {
        public int UserId { get; set; }
    }

    public class GetReviewsQueryHandler : IRequestHandler<GetReviewsQuery, PagedResult<ReviewDto>>
    {
        private readonly IReviewGateway _reviewGateway;
        private readonly IUsersRepository _usersRepository;
        private readonly IMapper _mapper;

        public GetReviewsQueryHandler(IReviewGateway reviewGateway, IUsersRepository usersRepository, IMapper mapper)
        {
            _reviewGateway = reviewGateway;
            _usersRepository = usersRepository;
            _mapper = mapper;
        }

        public async Task<PagedResult<ReviewDto>> Handle(GetReviewsQuery request, CancellationToken cancellationToken)
        {
            var reviews = await _reviewGateway.GetReviews(request.UserId);

            var reviewInitiators = await _usersRepository.GetByIds(reviews.Select(x => x.InitiatorUserId).ToArray());

            var users = _mapper.Map<IEnumerable<UserDto>>(reviewInitiators)
                .ToDictionary(x => x.Id, x => x);

            var reviewItems = reviews
                .Select(x =>
                    new ReviewDto
                    {
                        Id = x.Id,
                        Body = x.Body,
                        CreationDate = x.CreationDateUtc,
                        TutorId = x.TutorId,
                        Rating = x.Rating,
                        User = users[x.InitiatorUserId]
                    })
                .ToList();

            return new PagedResult<ReviewDto>
            {
                Count = reviews.Count,
                Page = 1,
                PageSize = 100,
                Items = reviewItems
            };
        }
    }
}
