﻿using FluentValidation;

namespace SpeakCamp.MainService.Application.Reviews.CreateReview
{
    public class CreateReviewCommandValidator : AbstractValidator<CreateReviewCommand>
    {
        public CreateReviewCommandValidator()
        {
            RuleFor(x => x.UserId)
                .GreaterThan(0).WithMessage("{UserId} is invalid");

            RuleFor(x => x.Rating)
                .LessThanOrEqualTo(5U).WithMessage("{Rating} is invalid");
        }
    }
}
