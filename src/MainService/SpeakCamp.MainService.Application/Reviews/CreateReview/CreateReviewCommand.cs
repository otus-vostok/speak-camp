﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using SpeakCamp.MainService.Application.Common.Abstractions;
using SpeakCamp.MainService.Application.Common.Exceptions;
using SpeakCamp.MainService.Application.Gateways;

namespace SpeakCamp.MainService.Application.Reviews.CreateReview
{
    /// <summary>
    /// Команда на создание отзыва.
    /// </summary>
    public class CreateReviewCommand : ICommand<Guid> {
        public int InitiatorAccountId { get; set; }
        public int UserId { get; set; }
        public string Body { get; set; }
        public uint Rating { get; set; }
    }

    public class CreateReviewCommandHandler : IRequestHandler<CreateReviewCommand, Guid>
    {
        private readonly IUsersRepository _usersRepository;
        private readonly IReviewGateway _reviewGateway;

        public CreateReviewCommandHandler(IUsersRepository usersRepository, IReviewGateway reviewGateway)
        {
            _usersRepository = usersRepository;
            _reviewGateway = reviewGateway;
        }

        public async Task<Guid> Handle(CreateReviewCommand command, CancellationToken cancellationToken)
        {
            var user = await _usersRepository.GetByAccountId(command.InitiatorAccountId);

            if (user == null)
                throw new EntityNotFoundException($"User with AccountId '{command.InitiatorAccountId}' doesn't exist");

            return await _reviewGateway.CreateReview(user.Id, command.UserId, command.Body, command.Rating);
        }
    }
}
