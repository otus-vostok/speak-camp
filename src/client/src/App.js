import {React} from 'react';
import {BrowserRouter as Router, Switch, Route, useLocation  } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './store';
import Content from './components/layout/Content';

import  Header  from './components/layout/Header';
import  Footer  from './components/layout/Footer';
import './styles/all.scss';

const App = () => {



  return (
    <Provider store={store}>
      <Router>
      <Header />
     
      <Content />

        <Footer />
      </Router>
    </Provider>
  );
}

export default App;
