import { 
    HubConnectionBuilder,
    HttpTransportType
} 
from "@microsoft/signalr";
import { MAIN_API_URL_SIGNALR } from "../config/apiUrls";
import { usersAmountChange } from "../actions/users";


const connection = new HubConnectionBuilder()
    .withUrl(MAIN_API_URL_SIGNALR, {
        skipNegotiation: true,
        transport: HttpTransportType.WebSockets,
    })
    .build();

const initializeSignalrHub = (store) => {

    try {
        const start = async () => {
            try {
                await connection.start();
                console.log("SignalR Connected.");
                connection.invoke("InitialInformationRequest")
            } catch (err) {
                console.log(err);
                setTimeout(start, 30000);
            }
        };
        
        connection.onclose(async () => {
            await start();
        });

        
        connection.on("ReceiveUsersAmount", (usersAmountInformation) => {
            try {
                store.dispatch(usersAmountChange(usersAmountInformation));
            } 
            catch (err) {
                console.error(`Something wrong with client SignalR in handling ReceiveUsersAmount: ${err}`);
            }         
        }); 

        start();
    }
    catch (err) {
        console.error(`Something wrong with SignalR: ${err}`);
    }

};


export const signalrMiddleware = store => { 

    initializeSignalrHub(store);

    return next => action => {

        if (action.type === "SIGNALR_ACTION") {
            /* 
                Something for manipulating of signalr connection. 
                Main reason - invoking something on the backend...
            */
            return;
        }  

        return next(action);
    }
};