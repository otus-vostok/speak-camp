import React from 'react';
import PageHeader from '../components/layout/PageHeader';
import MeetingSummary from '../components/meetings/MeetingSummary';
import MeetingUsers from '../components/meetings/MeetingUsers';
import { useSelector, useDispatch } from 'react-redux';
import { getMeetingDetails } from '../actions/meetings';

const MeetingDetails = ({match}) => {

    const dispatch = useDispatch();
    const meeting = useSelector(({ meetings }) => meetings.details);
    const authUser = useSelector(({ auth }) => auth.user);
    
    React.useEffect( () => {
            dispatch(getMeetingDetails(match.params.meetingId)); 
            window.scrollTo(0, 0);
    },[]);

return (

<div className="container">

{meeting ? (
<>
    <PageHeader name={meeting.name} />

    <MeetingSummary meeting={meeting} authUser={authUser} />

    <div className="meeting-description text-block" dangerouslySetInnerHTML={{ __html: meeting.description.replace('\n','<br><br>') }}>
        
    </div>

    <h2>Participants</h2>
    <MeetingUsers users={meeting.visits.map(visit=>visit.user)} />
</>) 

: ""}

    


</div>


);

}

export default MeetingDetails;
/*
const meeting =  {id: 1, name: "In the office: useful phrases, verbs and idioms", 
languageLevel: "B1", 
startDate: "20 May, 9:30 am", 
language: {keyName:"EN"}, 
description: "Meeting description is copy that aims to tell your potential attendees what will be happening at the event, who will be speaking, and what they will get out of attending. Good event descriptions can drive attendance to events and also lead to more media coverage.<br><br>Also description is copy that aims to tell your potential attendees what will be happening at the event, who will be speaking, and what they will get out of attending. Good event descriptions can drive attendance to events and also lead to more media coverage.",
user:{id: 1, rating: 4.5, firstName:"Alex", lastName:"Jonson", languageLevel:"C1", language:{name:"English"}},
visits: 
[
    {id: 1, user:{id: 4, rating: 4.5, firstName:"Anna", lastName:"Petrovsky", languageLevel:"B1", language:{name:"English"}}},
    {id: 2, user:{id: 5, rating: 4.5, firstName:"Maria", lastName:"Watson", languageLevel:"B1", language:{name:"English"}}},
    {id: 3, user:{id: 6, rating: 4.5, firstName:"John", lastName:"Skyworker", languageLevel:"B2", language:{name:"English"}}},
    {id: 3, user:{id: 3, rating: 4.5, firstName:"Alexa", lastName:"Promo", languageLevel:"B2", language:{name:"English"}}},
]
};
*/

