import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import {TextField} from '@mui/material';
import {login} from '../actions/auth';

const Login = () => {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(({ auth }) => auth.isAuthenticated);
    const loginError = useSelector(({ auth }) => auth.loginError);
    
    const [email, setEmail] = useState("");
    const [emailError, setEmailError] = useState("");
    const [password, setPassword] = useState("");
    const [passwordError, setPasswordError] = useState("");
    
    const handleSubmit = (e) => {
        e.preventDefault();
        if (!validate())
            return;

        dispatch(login({email, password})); 
    }

  const validate = () => {
    
    let valid = true;
    if (!/^\S+@\S+\.\S+$/.test(email))
    {   
        setEmailError("Enter valid email");
        valid = false;    
    } else 
    {
        setEmailError("") ;
    }

    if (!/^\S{3,}$/.test(password)){  
        setPasswordError("Password should contain 3+ symbols"); 
        valid = false;
    } else {
        setPasswordError("");
    }

   return valid;
}

    if (isAuthenticated)
        return <Redirect to="/" />;


    return (
    <div className="container">

        <div className="form-box">

            <form onSubmit={handleSubmit} className="form-mini"  autoComplete="off" >
                
                <h1 className="form-header">Log In</h1>
                <div className="form-subheader">Use your credentials or <Link to="/registration">Sign up</Link> if you don't have an account</div>
                <div className="form-row">
                    <TextField id="email" error={emailError !== ""} helperText={emailError} value={email} onChange={e => setEmail(e.target.value)}  label="Email" fullWidth   />
                </div>
                <div className="form-row">
                    <TextField id="password"  error={passwordError !== ""} helperText={passwordError} value={password} onChange={e => setPassword(e.target.value)}  label="Password" type="password" fullWidth />
                </div>
              
                <div className="form-buttons">
                    <button className="btn-primary" type="submit">Login to SPEAKCAMP</button>
                </div>
                {loginError ? <div class="form-msg">Login failed. Wrong email or password</div> : ""}
            </form>

        </div>
    </div>
    );

}


export default Login;