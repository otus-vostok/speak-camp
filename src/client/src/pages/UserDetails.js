import React from 'react';
import MeetingsList from '../components/meetings/MeetingsList';
import UserSummary from '../components/users/UserSummary';
import ReviewsList from '../components/reviews/ReviewsList';
import ReviewForm from '../components/reviews/ReviewForm';
import { useSelector, useDispatch } from 'react-redux';
import { getUserDetails, getUserMeetings, getUserReviews } from '../actions/users';

const UserDetails = ({match}) => {

    const dispatch = useDispatch();
    const user = useSelector(({ users }) => users.details);
    const meetings = useSelector(({ users }) => users.meetings);
    const reviews = useSelector(({ users }) => users.reviews);
    const authUser = useSelector(({ auth }) => auth.user);
    
    React.useEffect( () => {
            dispatch(getUserDetails(match.params.userId)); 
            dispatch(getUserMeetings(match.params.userId)); 
            dispatch(getUserReviews(match.params.userId)); 
            window.scrollTo(0, 0);
    },[]);
 
        
      

return (

<div className="container">


    {user ? (
    <>
            <UserSummary user={user} />
            <div className="user-description text-block">
            {user.description}
            </div>
    </>) : "" }


    {meetings?.length > 0 ? (
            <div className="user-meetings"> 
                <h2>Tutor's meetings</h2>
                <MeetingsList items={meetings}/>
            </div>  ) : ""}
            
    {reviews?.length > 0 ? (     
            <div className="reviews">
                <h2>Reviews</h2>
                <ReviewsList items={reviews}/>
                <ReviewForm authUser={authUser} userId={user.id}/>

            </div>) : "" }

</div>
);

}

export default UserDetails;