import React from 'react';
import HomePromo from '../components/home/HomePromo';
import MeetingsList from '../components/meetings/MeetingsList';
import UsersList from '../components/users/UsersList';
import { useSelector, useDispatch } from 'react-redux';
import { getUpcomingMeetings} from '../actions/meetings';
import {getRatedUsers} from '../actions/users'

const Home = () => {
const dispatch = useDispatch();
const upcomingMeetings = useSelector(({ meetings }) => meetings.upcoming); 
const ratedUsers = useSelector(({ users }) => users.rated);   
React.useEffect( () => {
        dispatch(getUpcomingMeetings()); 
        dispatch(getRatedUsers());
        window.scrollTo(0, 0);
},[]);




return (
<>
<HomePromo />

<div className="container">
        
        <div className="recent-meetings">
        <div className="home-sub-box">
        <h2 className="home-sub">Upcoming  meetings </h2> <i className="home-sub-em">online</i>
        </div>
        <MeetingsList items={upcomingMeetings}/>

        </div>
        <div className="home-sub-box">
        <h2 className="home-sub">Our   tutors </h2><i className="home-sub-em-alt">rated</i>
        </div>
        <UsersList items={ratedUsers}/>
</div>
</>
);

}


export default Home;