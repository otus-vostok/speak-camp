import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import {TextField, InputLabel, MenuItem, FormControl, Select} from '@mui/material';
import {register} from '../actions/auth';

const Registration = () => {
    const dispatch = useDispatch();
    const isAuthenticated = useSelector(({ auth }) => auth.isAuthenticated);
    const registerError = useSelector(({ auth }) => auth.registerError);
    
    const [role, setRole] = useState("Student");
    const [firstName, setFirstName] = useState("");
    const [firstNameError, setFirstNameError] = useState("");
    const [lastName, setLastName] = useState("");
    const [lastNameError, setLastNameError] = useState("");
    const [email, setEmail] = useState("");
    const [emailError, setEmailError] = useState("");
    const [password, setPassword] = useState("");
    const [passwordError, setPasswordError] = useState("");
    const [language, setLanguage] = useState("en");
    const [languageLevel, setLanguageLevel] = useState("b1");
    
    const handleSubmit = (e) => {
        e.preventDefault();
        if (!validate())
            return;

        dispatch(register({role, email, password, firstName, lastName, language, languageLevel})); 
    }

  const validate = () => {
    
    let valid = true;
    
    if (!/^[a-zA-Z\-]{1,}$/.test(firstName)){  
        setFirstNameError("Enter First Name"); 
        valid = false;
    } else {
        setFirstNameError("");
    }

    if (!/^[a-zA-Z\-]{1,}$/.test(lastName)){  
        setLastNameError("Enter Last Name"); 
        valid = false;
    } else {
        setLastNameError("");
    }
        
    if (!/^\S+@\S+\.\S+$/.test(email))
    {   
        setEmailError("Enter valid email");
        valid = false;    
    } else 
    {
        setEmailError("") ;
    }

    if (!/^\S{3,}$/.test(password)){  
        setPasswordError("Password should contain 3+ symbols"); 
        valid = false;
    } else {
        setPasswordError("");
    }

   return valid;
}

    if (isAuthenticated)
        return <Redirect to="/" />;


    return (
    <div className="container">

        <div className="form-box">

            <form onSubmit={handleSubmit} className="form-mini"  autoComplete="off" >
                
                <h1 className="form-header">Sign Up</h1>
                <div className="form-subheader">Fill out the form or <Link to="/login">Log In</Link> if you already have an account </div>
                
                <div className="form-row">
                <FormControl fullWidth>
                        <InputLabel id="role-label">Role</InputLabel>
                        <Select
                        labelId="role-label"
                        id="role"
                        value={role}
                        label="Role"
                        onChange={e => setRole(e.target.value)}
                        >
                        <MenuItem value="Student">Student</MenuItem>
                        <MenuItem value="Tutor">Tutor</MenuItem>
                       
                        </Select>
                    </FormControl>

                </div>
                
                <div className="form-row">
                    <TextField id="firstName" error={firstNameError !== ""} helperText={firstNameError} value={firstName} onChange={e => setFirstName(e.target.value)}  label="First Name" fullWidth   />
                </div>
                <div className="form-row">
                    <TextField id="lastName" error={lastNameError !== ""} helperText={lastNameError} value={lastName} onChange={e => setLastName(e.target.value)}  label="Last Name" fullWidth   />
                </div>

                <div className="form-row">
                    <TextField id="email" error={emailError !== ""} helperText={emailError} value={email} onChange={e => setEmail(e.target.value)}  label="Email" fullWidth   />
                </div>
                <div className="form-row">
                    <TextField id="password"  error={passwordError !== ""} helperText={passwordError} value={password} onChange={e => setPassword(e.target.value)}  label="Password" type="password" fullWidth />
                </div>


                <div className="form-row">
                <FormControl fullWidth>
                        <InputLabel id="language-label">Language</InputLabel>
                        <Select
                        labelId="language-label"
                        id="language"
                        value={language}
                        label="Language"
                        onChange={e => setLanguage(e.target.value)}
                        >
                        <MenuItem value="en">English</MenuItem>
                        <MenuItem value="de">German</MenuItem>
                        <MenuItem value="es">Spanish</MenuItem>
                        </Select>
                    </FormControl>

                </div>

                <div className="form-row">
                <FormControl fullWidth>
                        <InputLabel id="language-level-label">Language level</InputLabel>
                        <Select
                        labelId="language-level-label"
                        id="language-level"
                        value={languageLevel}
                        label="Language lvel"
                        onChange={e => setLanguageLevel(e.target.value)}
                        >
                        <MenuItem value="a1">A1 - Beginner</MenuItem>
                        <MenuItem value="a2">A2 - Elementary</MenuItem>
                        <MenuItem value="b1">B1 - Intermediate</MenuItem>
                        <MenuItem value="b2">B2 - Upper Intermediate</MenuItem>
                        <MenuItem value="c1">C1 - Advanced</MenuItem>
                        <MenuItem value="c2">C2 - Proficient</MenuItem>
                        </Select>
                    </FormControl>

                </div>

              
                <div className="form-buttons">
                    <button className="btn-primary" type="submit">Join to SPEAKCAMP</button>
                </div>
                {registerError ? <div class="form-msg">Registration failed. Please, use another email</div> : ""}
            </form>

        </div>
    </div>
    );

}


export default Registration;