import React from 'react';
import MeetingsSearch from '../components/meetings/MeetingsSearch';
import MeetingsSort from '../components/meetings/MeetingsSort';
import MeetingsList from '../components/meetings/MeetingsList';
import PageHeader from '../components/layout/PageHeader';
import { useSelector, useDispatch } from 'react-redux';
import { getMeetings, setMeetingsSort, setMeetingsSearch} from '../actions/meetings';

const Meetings = () => {

const dispatch = useDispatch();
const items = useSelector(({ meetings }) => meetings.items);
const sort = useSelector(({ meetings }) => meetings.sort);
const search = useSelector(({ meetings }) => meetings.search);
const count = useSelector(({ meetings }) => meetings.count);

console.log(search);

React.useEffect( () => {
        dispatch(getMeetings(sort, search)); 
},[sort, search]);

const onSortChange = React.useCallback((sort) => {
        dispatch(setMeetingsSort(sort));
      }, []);
    
      
const onSearchChange = React.useCallback((search) => {
        dispatch(setMeetingsSearch(search));
      }, []);


return (
<div className="container">
        <PageHeader name="Speaking club meetings" />

        <MeetingsSearch search={search} onSearchChanged={onSearchChange} />
        <MeetingsSort count={count} sort={sort} onSortChange={onSortChange} />

        <MeetingsList items={items}/>
</div>
);

}

export default Meetings;
/*
const meetings =  [
{id: "1", name: "In the office: useful phrases, verbs and idioms", languageLevel: "B1", startDate: "20 May, 9:30 am", language: {keyName:"EN"}, user:{id: 1, rating: 4.5, firstName:"Alex", lastName:"Jonson"}},
{id: "2", name: "Education system in the USA and Britain", languageLevel: "B1", startDate: "20 May, 9:30 am", language: {keyName:"EN"}, user:{id: 2, rating: 4.5, firstName:"Tina", lastName:"House"}},
{id: "3", name: "Birthday is coming", languageLevel: "B1", startDate: "20 May, 9:30 am", language: {keyName:"EN"}, user:{id: 3, rating: 4.5, firstName:"Maria", lastName:"Rodrigez"}},
{id: "4", name: "Tasty topic: let's discuss food and drinks", languageLevel: "B1", startDate: "20 May, 9:30 am", language: {keyName:"EN"}, user:{id: 1, rating: 4.5, firstName:"Alex", lastName:"Jonson"}},
{id: "6", name: "Pets at home and outdoors", languageLevel: "B1", startDate: "20 May, 9:30 am", language: {keyName:"EN"}, user:{id: 1, rating: 4.5, firstName:"Alex", lastName:"Jonson"}},
{id: "9", name: "Famous British music groups", languageLevel: "B1", startDate: "20 May, 9:30 am", language: {keyName:"EN"}, user:{id: 1, rating: 4.5, firstName:"Alex", lastName:"Jonson"}}
];
*/