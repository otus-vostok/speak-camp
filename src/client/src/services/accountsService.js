import axios from 'axios';
import {ACCOUNT_API_URL} from '../config/apiUrls';

export const accountsService = {
    async login(userData) {
        const res =  await axios.post(`${ACCOUNT_API_URL}login`, userData);
        return res.data;
    },

    async register(userData) {
        const res =  await axios.post(`${ACCOUNT_API_URL}register`, userData);
        return res.data;
    },

   
}  