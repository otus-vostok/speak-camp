import axios from 'axios';
import {MAIN_API_URL} from '../config/apiUrls';
import { getDateForRequest } from "../helpers/dateHelper";

export const meetingsService = {
    async getList(sorting, searching) {

        const dateFrom = getDateForRequest(searching.dateFrom);
        const dateTo = getDateForRequest(searching.dateTo);

        const res =  await axios.get(
            `${MAIN_API_URL}Meetings?`
            + `Sorting=${sorting}`
            + `&LanguageKey=${searching.languageLabel}`
            + `&LanguageLevel=${searching.languageLevel}`
            + `&DateFrom=${dateFrom}`
            + `&DateTo=${dateTo}`
        );
        return res.data;
    },

    async getDetails(id) {
        const res =  await axios.get(`${MAIN_API_URL}Meetings/${id}`);
        return res.data;
    },

    async join(meetingId, tokenConfig) {
        console.log(meetingId);
        const res =  await axios.post(`${MAIN_API_URL}Visits`, {meetingId:meetingId}, tokenConfig);
        return res.data;
    },

    async cancelJoin(visitId, tokenConfig) {
        const res =  await axios.delete(`${MAIN_API_URL}Visits/${visitId}`,  tokenConfig);
        return res.data;
    },

    async getUpcoming(top = 3) {
        const res =  await axios.get(`${MAIN_API_URL}Meetings?Sorting=StartDate&PageSize=${top}`);
        return res.data;
    },
}  