import axios from 'axios';
import {MAIN_API_URL} from '../config/apiUrls';

export const usersService = {
    async getByAccountId(accountId) {
        const res =  await axios.get(`${MAIN_API_URL}Users/byaccount/${accountId}`);
        return res.data;
    },

    async getDetails(id) {
        const res =  await axios.get(`${MAIN_API_URL}Users/${id}`);
        return res.data;
    },

    async create(userData, tokenConfig) {
        const res =  await axios.post(`${MAIN_API_URL}Users`, userData, tokenConfig);
        return res.data;
    },

    async getMeetings(id) {
        const res =  await axios.get(`${MAIN_API_URL}Users/${id}/meetings`, );
        return res.data;
    },
   
    async getReviews(id) {
        const res =  await axios.get(`${MAIN_API_URL}Users/${id}/reviews`, );
        return res.data;
    },

    async addReview(review, tokenConfig) {
        const res =  await axios.post(`${MAIN_API_URL}Reviews`, review, tokenConfig);
        return res.data;
    },

    async getRated(top = 4) {
        const res =  await axios.get(`${MAIN_API_URL}Users?userType=tutor&sorting=RatingDesc&pageSize=${top}`);
        return res.data;
    },
}  