import {MAIN_API_URL} from '../config/apiUrls';

export const getPhotoUrl = (key, preview = false) => {
    if (!key)
        return '/images/profile.jpg';
        
    return `${MAIN_API_URL}Images/?key=${key}&imageSize=${preview ? "Preview" : "Full"}`;
}
