export const configAuthHeader = (authData) => {
         
  const token = authData?.accessToken;

    const config = {
      headers: {
        'Content-Type': 'application/json',
      },
    };
    
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }
  
    return config;
  };