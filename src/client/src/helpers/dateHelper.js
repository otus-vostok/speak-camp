import differenceInDays from 'date-fns/differenceInDays'

export const convertDate = (dateString) => {
   
   
    const d = new Date(dateString);
    const minutes = d.getMinutes() ;
    return `${d.getDate()} ${getMonthName(d)}, ${d.getHours()}:${minutes < 10 ? '0' + minutes : minutes}`;
}


export const convertDateWithYear = (dateString) => {
  
    const d = new Date(dateString);
  
    return `${d.getDate()} ${getMonthName(d)} ${d.getFullYear()}`;
}

export const getMonthName = (date) => {
    const months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];
    return months[date.getMonth()];
}

export const timezoneOffset = () => {
    return new Date().getTimezoneOffset();
}

export const getDateForRequest = (date) => {
    return date === null ? "" : new Date(date).toUTCString();
};


export const getPeriod = (date) => {

    const days = differenceInDays( new Date(), new Date(date));

    if (days < 1)
        return "Newly";
    
    if (days < 30)
        return `${days} days`;

    const months = Math.ceil(days / 30)

    return `${months} months`;

};