import React from 'react';
import { Link } from 'react-router-dom';
import {getPhotoUrl} from '../../helpers/photoHelper';
import {convertDate} from '../../helpers/dateHelper';

const MeetingCard = ({item}) => {

return (
<Link to={`/meeting/${item.id}`}  className="meeting-card" >
    <div className="meeting-card-img" style={{ backgroundImage: `url(${getPhotoUrl(item.photoKey, true)})` }}>

    </div>
    <div className="meeting-card-head">
     <div className="meeting-card-name"><h3>{item.name}</h3></div>

     <div className="meeting-card-user">
         <img className="meeting-card-user-photo" src={getPhotoUrl(item.user.photoKey, true)} alt="" /> 
         <span className="meeting-card-user-name">{item.user.firstName} {item.user.lastName}</span> 
         {item.user.rating ? (<span> <span className="meeting-card-user-rating-icon">★</span> 
         <span className="meeting-card-user-rating-value">{item.user.rating}</span> </span> ) : ""}
         
     </div>
         
     <div className="meeting-card-params">
     <div> <span className="meeting-card-lang-label">{item.language.keyName}</span> {item.languageLevel}+ level </div>

     <div>on {convertDate(item.startDate)}</div>
     </div>
 
 </div>
  
 </Link> 

);

}


export default MeetingCard;