import React from 'react';
import { Link } from 'react-router-dom';
import MeetingSummaryActions from './MeetingSummaryActions';
import {getPhotoUrl} from '../../helpers/photoHelper';
import {convertDate} from '../../helpers/dateHelper';

const MeetingSummary = ({meeting, authUser}) => {

return (
    <div className="meeting-summary">
    <div className="meeting-summary-img" style={{ backgroundImage: `url(${getPhotoUrl(meeting.photoKey)})` }}>

    </div>
    <div className="meeting-summary-head">
     
    <Link to={`/user/${meeting.user.id}`} className="meeting-summary-user" >
    
        <div> <img className="meeting-summary-user-img" src={getPhotoUrl(meeting.user.photoKey)} alt=""/> </div>
        
        <div>
          <div className="meeting-summary-user-head">
              <div className="meeting-summary-user-name" >{meeting.user.firstName} {meeting.user.lastName}</div> 
              {meeting.user.rating ? (<span> <span className="meeting-summary-user-rating-icon">★</span> 
         <span className="meeting-summary-tutor-user-value">{meeting.user.rating}</span> </span> ) : ""}
          </div>
          <div className="meeting-summary-user-params">
          {meeting.user.language.name}, {meeting.user.languageLevel}
          </div>
        </div>  
      
     </Link>
     <ul>
        <li className="meeting-summary-param">
            for {meeting.language.name} {meeting.languageLevel}+ language learners             
        </li>
        <li className="meeting-summary-param">
            on {convertDate(meeting.startDate)},  for {meeting.durationMinutes} min              
        </li>
        <li className="meeting-summary-param">
        {meeting.placesLimit} places limit,  {meeting.placesLimit - meeting.placesOccupied} avaliable           
        </li>

     </ul>


     <MeetingSummaryActions meeting={meeting} authUser={authUser} />
    

  </div> 


</div>

);

}


export default MeetingSummary;