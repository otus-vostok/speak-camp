import { TextField, InputLabel, MenuItem, FormControl, Select } from '@mui/material';
import { DatePicker, LocalizationProvider } from '@mui/lab';
import AdapterDateFns from '@mui/lab/AdapterDateFns';

const MeetingsSearch = ({ search, onSearchChanged }) => {
 
  const languageLabelChangedHandler = (event) => {
    const value = event.target.value;
    onSearchChanged({
      ...search,
      languageLabel: value
    });
  };

  const languageLevelChangedHandler = (event) => {
    const value = event.target.value;
    onSearchChanged({
      ...search,
      languageLevel: value
    });
  };

  const dateFromChangedHandler = (date) => {
    let dateFrom = date;

    if (search.dateTo && new Date(search.dateTo).getTime() < new Date(date).getTime()) {
      dateFrom = search.dateTo;
    }

    onSearchChanged({
      ...search,
      dateFrom: dateFrom
    });
  };

  const dateToChangedHandler = (date) => {
    let dateTo = date;

    if (search.dateFrom && new Date(search.dateFrom).getTime() > new Date(date).getTime()) {
      dateTo = search.dateFrom;
    }

    onSearchChanged({
      ...search,
      dateTo: dateTo
    });
  };

return ( 
    <div className="meetings-search"> 

      <div className="meetings-search-col">
        <FormControl fullWidth>
          <InputLabel id="language-label">Language</InputLabel>
          <Select
            labelId="language-label"
            id="language"
            value={search.languageLabel}
            label="Language"
            onChange={languageLabelChangedHandler}
          >
            <MenuItem value="">Any language</MenuItem>
            <MenuItem value="en">English</MenuItem>
            <MenuItem value="de">German</MenuItem>
            <MenuItem value="es">Spanish</MenuItem>     
          </Select>
      </FormControl>
    </div>

    <div className="meetings-search-col">
      <FormControl fullWidth>
          <InputLabel id="language-level-label">Language level</InputLabel>
          <Select
            labelId="language-level-label"
            id="language-level"
            value={search.languageLevel}
            label="Language level"  
            onChange={languageLevelChangedHandler}   
          >
            <MenuItem value="">All levels</MenuItem>
            <MenuItem value="a1">A1 - Beginner</MenuItem>
            <MenuItem value="a2">A2 - Elementary</MenuItem>
            <MenuItem value="b1">B1 - Intermediate</MenuItem>
            <MenuItem value="b2">B2 - Upper Intermediate</MenuItem>
            <MenuItem value="c1">C1 - Advanced</MenuItem>
            <MenuItem value="c2">C2 - Proficient</MenuItem>
          </Select>
      </FormControl>
    </div>

    <div className="meetings-search-col">
      <LocalizationProvider dateAdapter={AdapterDateFns}>  
      <ul className="meetings-search-split">
        <li>
          <DatePicker 
            fullWidth
            label="Date from"
            value={search.dateFrom}
            onChange={dateFromChangedHandler}
            renderInput={(params) => <TextField {...params} />}
          />
        </li>
        <li>
          <DatePicker 
            fullWidth
            label="Date to"
            value={search.dateTo}
            onChange={dateToChangedHandler}
            renderInput={(params) => <TextField {...params} />}
          />
        </li>
      </ul>
      </LocalizationProvider>
    </div>

  </div>);

}

export default MeetingsSearch;