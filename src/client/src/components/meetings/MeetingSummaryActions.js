import React from 'react';
import {  useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import {joinMeeting, cancelJoinMeeting} from '../../actions/meetings';

const MeetingSummaryActions = ({ meeting, authUser}) => {
 
    const dispatch = useDispatch();
    
 
    const  handleCancel = (e) => {
        e.preventDefault();

        const visit = meeting.visits.find(visit=>visit.user.id == authUser?.id)
        if (!visit)
            return;
        dispatch(cancelJoinMeeting(meeting.id, visit.id));   

      };

      const  handleJoin = (e) => {
        e.preventDefault();
              
        dispatch(joinMeeting(meeting.id));

      };


if (!authUser)
    return (
    <div className="meeting-summary-actions">
        <div className="meeting-summary-actions-msg">
            Please,  <Link to="/registration">Sign up</Link> to join this meeting
        </div>
    </div>
    );

  

if (authUser && authUser.id == meeting.user.id)
    return (
    <div className="meeting-summary-actions">
        <div className="meeting-summary-actions-msg">
          This is your meeting
        </div>
    </div>
    );


if (authUser && meeting.visits.find(visit=>visit.user.id == authUser.id))
    return (
        <div className="meeting-summary-actions">
            <div className="meeting-summary-actions-msg">
            You are joined. <a href="javascript:;" onClick={handleCancel}>Cancel appointment</a>
            </div>
        </div>
        );


return ( 
<div className="meeting-summary-actions">
<a  href="javascript:;" onClick={handleJoin} className="meeting-summary-action btn-primary">Join meeting</a>
<Link to={"/user/" + meeting.user.id} className="meeting-summary-action btn-alt">Tutor details</Link>
</div>
 );

}

export default MeetingSummaryActions;
