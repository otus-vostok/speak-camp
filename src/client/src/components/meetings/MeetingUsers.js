import React from 'react';
import UserCard from '../users/UserCard';


const MeetingUsers = ({users}) => {

if (!users || users.length == 0)
    return (<div className="list-message"><span className="list-message-inner">😢 No one has yet joined this fascinating meeting. Be the first!</span></div>);
else
    return ( <div className="meeting-users user-cards">
    {users.map((user) => <UserCard key={user.id}  user={user} />)}
    </div>);

}

export default MeetingUsers;