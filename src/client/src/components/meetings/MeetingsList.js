import React from 'react';
import MeetingCard from './MeetingCard';


const MeetingsList = ({items}) => {

return ( <div className="meeting-cards">
{items && items.map((meeting) => <MeetingCard key={meeting.id} item={meeting}  />)}
</div>);

}

export default MeetingsList;