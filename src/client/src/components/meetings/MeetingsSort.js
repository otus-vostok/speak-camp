import React, {useState} from 'react';



const MeetingsSort = ({ count, sort, onSortChange}) => {
 
  
 
    const  handleChange = (event) => {
      
        onSortChange(event.target.value);
      };
return ( 

    (<div className="meetings-sort"> 
    <div><b>{count}</b> meetings found </div>
    
    <div className="meetings-sort-box">
     <label> Sort by </label>
    <select  onChange={(e) => handleChange(e)}  value={sort} className="meetings-sort-list">
            <option value="Popularity">Popularity</option>
            <option value="StartDate">Start date</option>
            <option value="Rating">Tutor rating</option>
    </select>
    
    </div>
    
    </div>));

}

export default MeetingsSort;
