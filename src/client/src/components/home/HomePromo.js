import { React } from 'react';
import { Link } from 'react-router-dom';
import {ReactComponent as PeopleImg} from './people.svg'


const HomePromo  = () => {



  return (
    <div className="home-promo-box">

        <div className="container">
        
            <div className="home-promo">
                <div className="home-promo-header-box">
                <h1 className="home-promo-header">Online speaking clubs<br />for language learners</h1>
                <div className="home-promo-text">
                Our tutors organize online zoom meetings, and students sign up for and participate in meetings. 
                Students choose meetings according to their interests and language level. 
                After a meeting, students can rate a tutor.
                </div>
                
                <div className="home-promo-actions">
                        <Link to="/meetings" className="home-promo-btn  btn-primary" >Browse meetings</Link>
                        <Link to="/about" className="home-promo-btn btn-alt" >How it works</Link>
                </div>
                </div>
                
                <div className="home-promo-img-box">
                  
                  <PeopleImg />
                                 
                
                </div>
            
            </div>
        
        
        
        </div>
    </div>
  );

}

export default HomePromo;