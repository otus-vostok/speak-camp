import React from 'react';
import {getPhotoUrl} from '../../helpers/photoHelper';

const UserCard = ({user}) => {

return ( 
<div className="user-card">
<img className="user-card-img"  src={getPhotoUrl(user.photoKey)}/>
<div className="user-card-head">
    <div className="user-card-name" >{user.firstName} {user.lastName}</div> 
    <div className="user-card-params" >{user.language.name}, {user.languageLevel}</div> 
</div>
</div>

);

}

export default UserCard;


