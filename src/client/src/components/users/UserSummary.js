import React from 'react';
import {getPhotoUrl} from '../../helpers/photoHelper';
import {getPeriod} from '../../helpers/dateHelper';
const UserSummary = ({user}) => {

return (

   
        <div className="user-header page-header">
            <img className="user-header-img" src={getPhotoUrl(user.photoKey)} alt=""/> 
            <h1 className="user-header-name">{user.firstName} {user.lastName}
                <span className="user-header-rating">
                    <span className="user-header-rating-icon">★</span> 
                    <span className="user-header-rating-val">{user.rating}</span> 
                </span>
        
            </h1>

            <ul className="user-header-params">
                <li className="user-header-param">{user.language.name}, {user.languageLevel}</li>
                <li className="user-header-param">{user.meetingsCount} meetings held</li>
                <li className="user-header-param">{getPeriod(user.creationDate)} on SpeakCamp</li>
            </ul>
        </div>
  

    );
}

export default UserSummary;