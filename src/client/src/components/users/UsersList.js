import React from 'react';
import UserCard from '../users/UserCard';


const UsersList = ({items}) => {

if (!items || items.length == 0)
    return null;
else
    return ( <div className="user-cards">
    {items.map((user) => <UserCard key={user.id}  user={user} />)}
    </div>);

}

export default UsersList;