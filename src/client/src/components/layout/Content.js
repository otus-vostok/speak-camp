import { React } from 'react';
import {BrowserRouter as Router, Switch, Route, useLocation  } from 'react-router-dom';
import { TransitionGroup, CSSTransition } from 'react-transition-group';

import  Home  from '../../pages/Home'
import  Login  from '../../pages/Login';
import  Registration  from '../../pages/Registration';
import  About  from '../../pages/About';
import  Meetings  from '../../pages/Meetings';
import  MeetingDetails  from '../../pages/MeetingDetails';
import  Users  from '../../pages/Users';
import  UserDetails  from '../../pages/UserDetails';


const Content  = () => {
    const location = useLocation();
  
  return (
    <TransitionGroup>
        <CSSTransition
          timeout={250}
          classNames='fade'
          key={location.pathname}
        >
    <div className="content">

      
          <Switch location={location}>
            <Route path="/" component={Home} exact />
            <Route path="/meetings" component={Meetings}  />
            <Route path="/meeting/:meetingId" component={MeetingDetails} exact />
            <Route path="/users" component={Users}  />
            <Route path="/user/:userId" component={UserDetails} exact />
            <Route path="/about" component={About} exact />
            <Route path="/login" component={Login} exact />
            <Route path="/registration" component={Registration} exact />
          </Switch>
      

        </div>
        </CSSTransition>
        </TransitionGroup>
  );

}

export default Content;