import React from 'react';


const PageHeader = ({name}) => {

return (
    <div className="page-header">
    <h1>{name}</h1>
</div>

);

}


export default PageHeader;