import { React } from 'react';
import { useSelector } from "react-redux";


const Footer  = () => {

  const studentsAmount = useSelector(({ users }) => users.usersAmount.studentsAmount);
  const tutorsAmount = useSelector(({ users }) => users.usersAmount.tutorsAmount);

  return (
    <footer className="footer">
      <div>© 2021 SpeakCamp. All rights reserved.</div>
      <div><b>{tutorsAmount}</b> Tutors and <b>{studentsAmount}</b> Students registered</div>  
    </footer>
  );

}

export default Footer;