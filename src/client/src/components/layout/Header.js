import React from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import {logout} from '../../actions/auth';
import {getPhotoUrl} from '../../helpers/photoHelper';

const Header = () => {

    const dispatch = useDispatch();
    const user = useSelector(({ auth }) => auth.user);

    const  handleLogout = (event) => {
      
        dispatch(logout());
      };


return (

<header className="header">
    <div className="container-header">
        <div className="header-inner">
            <div className="header-logo"><img className="header-logo-img" src="/images/logo3.svg" alt="SpeakCamp logo" /></div>

            <nav className="header-nav">
                <Link to="/"  className="header-nav-link">Home</Link>
                <Link to="/meetings" className="header-nav-link" >Meetings</Link>
                <Link to="/users" className="header-nav-link" >Tutors</Link>
                <Link to="/about" className="header-nav-link" >About</Link>
              
            </nav>
       
            {user ? (
             <div className="header-user">
                <img className="header-user-img"  src={getPhotoUrl(user.photoKey)}/>
                <Link to="/" className="header-user-name" >{user.firstName} {user.lastName}</Link>
               
                <button className="header-user-btn  btn-alt" onClick={(e) => handleLogout(e)}>Logout</button>
            </div>
            ) : (
            <div className="header-user">
                <Link to="/login" className="header-user-btn btn-label" >Log In</Link>
                <Link to="/registration" className="header-user-btn  btn-primary" >Sign Up</Link>
            </div>
            )}
            
         

        </div>
    </div>
</header>
);

}


export default Header;