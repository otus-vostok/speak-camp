import React from 'react';
import Rating from '@mui/material/Rating';
import {getPhotoUrl} from '../../helpers/photoHelper';
import {convertDateWithYear} from '../../helpers/dateHelper';
const Review = ({review}) => {

return (

    <div className="review">
        <div className="review-rating">
        <div className="review-rating-icons">
           
            <Rating name="size-small" value={review.rating} readOnly size="small" />
        </div>

        </div>
        <div className="review-head">
            <div className="review-text">{review.body}  
            </div>
            <div className="review-params">
                <img className="meeting-card-user-photo" src={getPhotoUrl(review.user.photoKey, true)} alt="" /> 
                <span className="review-author-name">{review.user.firstName} {review.user.lastName} </span>
                <span className="review-date">{convertDateWithYear(review.creationDate)}</span>
            </div>
        </div>
    </div>

    );
}

export default Review;