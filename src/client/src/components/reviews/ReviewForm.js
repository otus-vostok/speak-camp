import React, {useState} from 'react';
import {  useDispatch } from 'react-redux';
import Rating from '@mui/material/Rating';
import TextField from '@mui/material/TextField';
import {addUserReview} from '../../actions/users';
import {getPhotoUrl} from '../../helpers/photoHelper';
import {convertDateWithYear} from '../../helpers/dateHelper';

const ReviewForm = ({authUser, userId}) => {
    const dispatch = useDispatch();
    const handleSubmit = (e) => {
        e.preventDefault();
        if (!validate())
            return;

        dispatch(addUserReview({body, rating, userId})); 
        setShowForm(false);
        setPosted(true);
    }

    const cancelClick = (e) => {
        e.preventDefault();
        setShowForm(false);
        
    }

    const addClick = (e) => {
        e.preventDefault();
        setShowForm(true);
        
    }

    const validate = () => {
        if (body.trim() == "" || rating == 0)
            return false;

        return true;
    }

    const [body, setBody] = useState("");
    const [rating, setRating] = useState(0);
    const [showForm, setShowForm] = React.useState(false);
    const [posted, setPosted] = React.useState(false);

    if (!authUser || !userId || authUser.id == userId)
        return null;


    return (
    
      <div className="review-form-box">

    {!posted && !showForm ? <button className="btn-alt review-form-btn" onClick={addClick}>Add review</button> : null}

    {!posted && showForm ? 

    <form onSubmit={handleSubmit} className="review-form" autoComplete="off" >
                    
            <div class="review-form-rating-box">
                <Rating name="size-small" value={rating} onChange={(event, newValue) => {setRating(newValue)}} size="small" />
            </div>
            <div class="review-form-head-box">
                
            <div className="review-form-row">
                <TextField
                            id="body"
                            label="Body"
                            multiline
                            rows={4}
                            value={body}
                            onChange={e => setBody(e.target.value)} 
                            defaultValue=""
                            fullWidth
                        />
            </div>
            
            
            <div className="review-form-buttons">
                <button className="btn-primary" type="submit">Post review</button>
                <button className="btn-alt" onClick={cancelClick}>Cancel</button>
            </div>
                
        </div>               
    
        </form> : null}

    </div>


    
  
    

        );
}

export default ReviewForm;