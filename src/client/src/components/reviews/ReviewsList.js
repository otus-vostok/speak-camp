import React from 'react';
import Review from './Review';


const ReviewsList = ({items}) => {

if (!items || items.length == 0)
    return null;
else
    return ( <div className="reviews-list">
    {items.map((review) => <Review key={review.id}  review={review} />)}
    </div>);

}

export default ReviewsList;