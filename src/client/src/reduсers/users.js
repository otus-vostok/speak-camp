const initialState = { 
    usersAmount: {
        studentsAmount: 0,
        tutorsAmount: 0,
    },
    rated: null,
    details: null,
    meetings: null,
    reviews: null
};

export default function usersReducer(state = initialState, action) {
  switch (action.type) {   
    case "USERS_AMOUNT_CHANGE":
      return {
        ...state,
        usersAmount: action.payload
      }
      case 'GET_RATED_USERS':
        return  {
          ...state,
          rated: action.payload.items
       }

       case 'GET_USER_DETAILS':
        return  {
          ...state,
          details: action.payload
       }

       case 'GET_USER_MEETINGS':
        return  {
          ...state,
          meetings: action.payload.items
       }

       case 'GET_USER_REVIEWS':
        return  {
          ...state,
          reviews: action.payload.items
       }
    default:
      return state;
  }
}