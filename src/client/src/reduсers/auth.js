import jwt_decode from "jwt-decode";
const initialState = {
  isAuthenticated: false, 
  accessToken:null, 
  refreshToken:null,  
  accessTokenExpDate:null, 
  accountId: null, 
  user:null, 
  loginError: false,
  registerError: false};

export default function (state = initialState, action) {
  switch (action.type) {
    
    case 'SET_TOKEN':
      
      const decodedToken = jwt_decode(action.payload.accessToken);
      const accountId = decodedToken["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"];
      const expTicks = decodedToken["exp"];
   
      return  {
        ...state,
        accessToken: action.payload.accessToken,
        refreshToken: action.payload.refreshToken,
        isAuthenticated: true,
        accountId: accountId,
        accessTokenExpDate: new Date(expTicks * 1000),
        loginError: false,
      }
      

    case 'SET_LOGIN_ERROR':
      return  {
        ...state,
        loginError: true,
      }

    case 'SET_REGISTER_ERROR':
      return  {
        ...state,
        registerError: true,
      }

    case 'SET_AUTH_USER':
    return  {
      ...state,
      user: action.payload,
    }
    
    case 'LOGOUT':
      return  {
       ...initialState
      }
   
    default:
      return state;
  }
}

// jwt-decode