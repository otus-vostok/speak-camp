import { combineReducers } from 'redux';
import meetings from './meetings';
import auth from './auth';
import users from "./users";


const rootReducer = combineReducers({
  meetings,
  auth,
  users
});

export default rootReducer;