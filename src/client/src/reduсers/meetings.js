const initialState = {
  items:[], 
  sort:"Popularity", 
  count: 0, 
  upcoming: [], 
  search: {
    languageLabel: "",
    languageLevel: "",
    dateFrom: null,
    dateTo: null
  },
  details: null
};

export default function (state = initialState, action) {
  switch (action.type) {
    
    case 'SET_MEETINGS_SORT':
      return  {
        ...state,
        sort: action.payload,
      }

    case 'SET_MEETINGS_SEARCH':
      return  {
        ...state,
        search: action.payload,
      }
    
    case 'GET_MEETINGS':
      return  {
        ...state,
        items: action.payload.items,
        count: action.payload.count
      }
     case 'GET_MEETING_DETAILS':
      return  {
        ...state,
        details: action.payload,
      }

      case 'GET_UPCOMING_MEETINGS':
        return  {
          ...state,
          upcoming: action.payload.items
       }

    default:
      return state;
  }
}