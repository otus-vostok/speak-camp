import {accountsService} from '../services/accountsService';
import {usersService} from '../services/usersService';
import {configAuthHeader} from '../helpers/authHelper';
import {timezoneOffset} from '../helpers/dateHelper';

 export const login = (userData) => async (dispatch) => {
    const loginResult = await accountsService.login(userData);
    
    if (loginResult.successful)
    {
        dispatch({
            type: 'SET_TOKEN',
            payload: loginResult,
          });

        dispatch(setAuthUser());

    } else
    {
        dispatch({
            type: 'SET_LOGIN_ERROR',
            payload: true,
          });
    }
    
   
  };



  
 export const register = (userData) => async (dispatch, getState) => {
  const registerResult = await accountsService.register(
     {role:userData.role, 
      email:userData.email, 
      password: userData.password});
  
  if (registerResult.successful)
  {
      dispatch({
          type: 'SET_TOKEN',
          payload: registerResult,
        });

        // create user in main
        const userResult = await usersService.create(
              { userType: userData.role, 
                firstName: userData.firstName,
                lastName: userData.lastName,
                languageKey: userData.language,
                languageLevel: userData.languageLevel, 
                timezoneOffsetMinutes: timezoneOffset() }, 
            configAuthHeader(getState().auth));

      dispatch(setAuthUser());

  } else
  {
      dispatch({
          type: 'SET_REGISTER_ERROR',
          payload: true,
        });
  }
  
 
};



  
 export const logout = () => async (dispatch) => {
  dispatch({
    type: 'LOGOUT'
    
  });
 }

const setAuthUser = () => async (dispatch, getState) => {
    
  const accountId = getState().auth.accountId;
  if (!accountId)
    return;
  const userResult = await usersService.getByAccountId(accountId);
  dispatch({
    type: 'SET_AUTH_USER',
    payload: userResult,
  });
};

