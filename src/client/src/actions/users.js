import {usersService} from '../services/usersService';
import {configAuthHeader} from '../helpers/authHelper';

export const usersAmountChange = (usersAmountInformation) => async (dispatch) => {
    dispatch({
      type: 'USERS_AMOUNT_CHANGE',
      payload: {
        studentsAmount: usersAmountInformation.studentsAmount,
        tutorsAmount: usersAmountInformation.tutorsAmount,
      }
    });
  };

  export const getRatedUsers = () => async (dispatch) => {
    const data = await usersService.getRated();
    dispatch({
      type: 'GET_RATED_USERS',
      payload: data,
    });
  };


  export const getUserDetails = (id) => async (dispatch) => {
    const data = await usersService.getDetails(id);
    dispatch({
      type: 'GET_USER_DETAILS',
      payload: data,
    });
  };


  export const getUserMeetings = (id) => async (dispatch) => {
    const data = await usersService.getMeetings(id);
    dispatch({
      type: 'GET_USER_MEETINGS',
      payload: data,
    });
  };


  export const getUserReviews = (id) => async (dispatch) => {
    const data = await usersService.getReviews(id);
    dispatch({
      type: 'GET_USER_REVIEWS',
      payload: data,
    });
  };


   
export const addUserReview = (review) => async (dispatch, getState) => {
 
  const reviewResult = await usersService.addReview(review, configAuthHeader(getState().auth));
  dispatch(getUserReviews(review.userId));

  setTimeout(() => {
    dispatch(getUserDetails(review.userId));
  }, 1000);
  

};
