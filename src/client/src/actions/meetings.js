import {meetingsService} from '../services/meetingsService';
import {configAuthHeader} from '../helpers/authHelper';

  export const getMeetings = (sort, search) => async (dispatch) => {
    console.log(search);
    const data = await meetingsService.getList(sort, search);
    dispatch({
      type: 'GET_MEETINGS',
      payload: data,
    });
  };

  export const setMeetingsSort = (sort) => async (dispatch)  => {
    dispatch({
      type: 'SET_MEETINGS_SORT',
      payload: sort,
    });
  };

  export const setMeetingsSearch = (search) => async (dispatch)  => {
    dispatch({
      type: 'SET_MEETINGS_SEARCH',
      payload: search,
    });
  };
  
  export const getMeetingDetails = (id) => async (dispatch) => {
    const data = await meetingsService.getDetails(id);
    dispatch({
      type: 'GET_MEETING_DETAILS',
      payload: data,
    });
  };


    export const joinMeeting = (meetingId) => async (dispatch, getState) => {
      try {
        const data = await meetingsService.join(meetingId, configAuthHeader(getState().auth));
        dispatch(getMeetingDetails(meetingId));
      } catch (err)
      {
        alert(err);

      }
      
    };

    export const cancelJoinMeeting = (meetingId, visitId) => async (dispatch, getState) => {
      try {
        const data = await meetingsService.cancelJoin(visitId, configAuthHeader(getState().auth));
        dispatch(getMeetingDetails(meetingId));
      } catch (err)
      {
        alert(err);

      }
      
    };


    export const getUpcomingMeetings = () => async (dispatch) => {
      const data = await meetingsService.getUpcoming();
      dispatch({
        type: 'GET_UPCOMING_MEETINGS',
        payload: data,
      });
    };





 