import { createFeature, createReducer, on } from '@ngrx/store';
import { CurrentUser } from 'src/app/shared/models/current-user';
import { AuthToken } from '../../models/auth-token';
import { TokenHelper } from '../../services/token.helper';
import { authActions } from '../actions/auth-actions';

export interface AuthState {
  user: CurrentUser | null;
  token: AuthToken | null;
  authenticated: boolean;
  authenticating: boolean;
  error: string | null;
}

export const authInitialState: AuthState = {
  user: null,
  token: null,
  authenticated: false,
  authenticating: false,
  error: null,
};

export const authFeature = createFeature({
  name: 'auth',
  reducer: createReducer(
    authInitialState,

    on(authActions.login, authActions.register, (state) => {
      return { ...state, authenticating: true };
    }),

    on(
      authActions.loginSuccess,
      authActions.registerSuccess,
      authActions.loadtokenSuccess,
      (state, { result }) => {
        return {
          ...state,
          token: TokenHelper.decode(result.accessToken, result.refreshToken),
          authenticated: true,
          authenticating: false,
          error: null,
        };
      }
    ),
    on(
      authActions.loginFailure,
      authActions.registerFailure,
      (state, { message }) => {
        return {
          ...state,
          authenticating: false,
          authenticated: false,
          error: message,
        };
      }
    ),
    on(authActions.loadCurrentuserSuccess, (state, { result }) => {
      return {
        ...state,
        user: result,
      };
    }),

    on(authActions.logoutSuccess, (state) => {
      return {
        ...state,
        user: null,
        token: null,
        authenticating: false,
        authenticated: false,
        error: null,
      };
    })
  ),
});
