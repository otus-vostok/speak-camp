import { authFeature } from '../reducers/auth-reducers';

const { selectUser, selectToken, selectError, selectAuthenticated } =
  authFeature;

export const authQuery = {
  selectUser,
  selectToken,
  selectError,
  selectAuthenticated,
};
