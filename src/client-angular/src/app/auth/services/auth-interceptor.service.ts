import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { first, mergeMap, Observable } from 'rxjs';

import { authQuery } from '../state/selectors/auth-selectors';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private store: Store) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return this.store.select(authQuery.selectToken).pipe(
      first(),
      mergeMap((token) => {
        const authReq = !token
          ? request
          : request.clone({
              setHeaders: { Authorization: 'Bearer ' + token.accessToken },
            });
        return next.handle(authReq);
      })
    );
  }
}
