import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { AuthResponse } from '../models/auth-response';
import { LoginRequest } from '../models/login-request';
import { CreateAccountRequest } from '../models/create-account-request';

@Injectable({ providedIn: 'root' })
export class AccountService {
  constructor(private http: HttpClient) {}

  login(request: LoginRequest): Observable<AuthResponse> {
    return this.http.post(`${environment.accountApiUrl}login`, request).pipe(
      map((response: any) => {
        return response;
      })
    );
  }

  register(request: CreateAccountRequest): Observable<AuthResponse> {
    return this.http.post(`${environment.accountApiUrl}register`, request).pipe(
      map((response: any) => {
        return response;
      })
    );
  }
}
