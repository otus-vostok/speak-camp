import { Injectable } from '@angular/core';
import { map, Observable, of, concatMap } from 'rxjs';
import { CurrentUser } from 'src/app/shared/models/current-user';
import { AuthResponse } from '../models/auth-response';
import { LoginRequest } from '../models/login-request';
import { RegisterRequest } from '../models/register-request';
import { AccountService } from './account.service';
import { UserCreationService } from './user-creation.service';

@Injectable({ providedIn: 'root' })
export class AuthFacade {
  constructor(
    private accountService: AccountService,
    private userCreationService: UserCreationService
  ) {}

  getCurrentUser(accountId: number): Observable<CurrentUser> {
    return this.userCreationService.getByAccountId(accountId);
  }

  login(request: LoginRequest): Observable<AuthResponse> {
    return this.accountService.login(request);
  }

  register(request: RegisterRequest): Observable<AuthResponse> {
    return this.accountService
      .register({
        role: request.role,
        email: request.email,
        password: request.password,
      })
      .pipe(
        concatMap((authRes) => {
          if (!authRes.successful) return of(authRes);
          else
            return this.userCreationService
              .create(
                {
                  userType: request.role,
                  firstName: request.firstName,
                  lastName: request.lastName,
                  languageKey: request.languageKey,
                  languageLevel: request.languageLevel,
                  timezoneOffsetMinutes: 0,
                },
                authRes.accessToken
              )
              .pipe(map((_) => authRes));
        })
      );
  }
}
