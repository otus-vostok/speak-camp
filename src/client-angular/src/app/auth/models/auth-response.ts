export interface AuthResponse {
  successful: boolean;
  accessToken?: string;
  refreshToken?: string;
}
