export interface AuthToken {
  accessToken: string;
  refreshToken: string;
  accountId: number;
  expDate: Date;
}
