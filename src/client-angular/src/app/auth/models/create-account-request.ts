export interface CreateAccountRequest {
  role: string;
  email: string;
  password: string;
}
