import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { authActions } from '../../state/actions/auth-actions';
import { authQuery } from '../../state/selectors/auth-selectors';

@Component({
  templateUrl: './registration.component.html',
})
export class RegistrationComponent implements OnInit {
  constructor(private fb: FormBuilder, private store: Store) {}
  form: FormGroup;
  error$: Observable<string | null>;
  submitted: boolean = false;

  ngOnInit(): void {
    this.initForm();
    this.error$ = this.store.select(authQuery.selectError);
  }

  initForm() {
    this.form = this.fb.group({
      role: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(3)]],
      languageKey: ['', [Validators.required]],
      languageLevel: ['', [Validators.required]],
    });
  }

  submit(form: FormGroup) {
    if (!form.valid) return;

    this.store.dispatch(authActions.register({ request: form.value }));
    this.submitted = true;
  }

  get firstName() {
    return this.form.get('firstName');
  }
  get lastName() {
    return this.form.get('lastName');
  }
  get email() {
    return this.form.get('email');
  }
  get password() {
    return this.form.get('password');
  }
}
