export interface AuthData {
  accessToken: string;
  refreshToken: string;
  accountId: number;
  accessTokenExpDate: Date;
}

export const AUTH_DATA_KEY: string = 'authData';
