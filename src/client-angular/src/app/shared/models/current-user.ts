import { Language } from './language';

export interface CurrentUser {
  id: number;
  userType: string;
  firstName: string;
  lastName: string;
  timezoneOffsetMinutes: number;
  photoKey: string;
  meetingsCount: number;
  rating: number;
  language: Language;
  languageLevel: string;
}
