export interface Language {
  id: number;
  keyName: string;
  name: string;
}
