import { Language } from './language';
import { User } from './user';

export interface Meeting {
  id: number;
  name: string;
  photoKey: string;
  startDate: string;

  language: Language;
  languageLevel: string;

  user: User;
}
