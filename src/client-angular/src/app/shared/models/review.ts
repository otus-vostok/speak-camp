import { User } from './user';

export interface Review {
  id: number;
  tutorId: number;
  body: string;
  rating: number;
  creationDate: Date;
  user: User;
}
