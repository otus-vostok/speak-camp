import { User } from './user';

export interface Visit {
  id: number;
  user: User;
}
