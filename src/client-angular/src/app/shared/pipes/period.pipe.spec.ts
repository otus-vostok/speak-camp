import { PeriodPipe } from './period.pipe';

describe('PeriodPipe', () => {
  const pipe = new PeriodPipe();

  it('should return Newly for now date', () => {
    const now = new Date();
    const res = pipe.transform(now.toDateString());
    expect(res).toBe('Newly');
  });

  it('should return N days for less than a month', () => {
    let testDate = new Date();
    testDate.setDate(new Date().getDate() - 5);
    const res = pipe.transform(testDate.toDateString());
    expect(res).toBe('5 days');
  });

  it('should return N months for longer than a month', () => {
    let testDate = new Date();
    testDate.setDate(new Date().getDate() - 45);
    const res = pipe.transform(testDate.toDateString());
    expect(res).toBe('2 months');
  });
});
