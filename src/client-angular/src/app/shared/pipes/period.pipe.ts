import { Pipe, PipeTransform } from '@angular/core';
import differenceInDays from 'date-fns/differenceInDays';

@Pipe({ name: 'period' })
export class PeriodPipe implements PipeTransform {
  transform(value: string): string {
    const days = differenceInDays(new Date(), new Date(value));

    if (days < 1) return 'Newly';
    if (days < 30) return `${days} days`;
    return `${Math.ceil(days / 30)} months`;
  }
}
