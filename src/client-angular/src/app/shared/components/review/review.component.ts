import { Component, Input, OnInit } from '@angular/core';
import { Review } from 'src/app/shared/models/review';

@Component({
  selector: 'sc-review',
  templateUrl: './review.component.html',
})
export class ReviewComponent implements OnInit {
  @Input()
  review: Review;

  constructor() {}

  ngOnInit(): void {}
}
