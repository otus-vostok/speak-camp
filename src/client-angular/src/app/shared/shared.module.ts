import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import {
  MatFormFieldModule,
  MAT_FORM_FIELD_DEFAULT_OPTIONS,
} from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { RouterModule } from '@angular/router';
import { MeetingCardComponent } from './components/meeting-card/meeting-card.component';
import { PhotoUrlPipe } from './pipes/photo-url.pipe';
import { UserCardComponent } from './components/user-card/user-card.component';
import { CommonModule } from '@angular/common';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { MatNativeDateModule } from '@angular/material/core';
import { SafeHtmlPipe } from './pipes/safe-html.pipe';
import { PeriodPipe } from './pipes/period.pipe';
import { ReviewComponent } from './components/review/review.component';
import { StarRatingModule } from 'angular-star-rating';

@NgModule({
  declarations: [
    PhotoUrlPipe,
    SafeHtmlPipe,
    PeriodPipe,
    MeetingCardComponent,
    UserCardComponent,
    PageHeaderComponent,
    ReviewComponent,
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ReactiveFormsModule,
    StarRatingModule,
    PhotoUrlPipe,
    SafeHtmlPipe,
    PeriodPipe,
    MeetingCardComponent,
    UserCardComponent,
    PageHeaderComponent,
    ReviewComponent,
  ],
  imports: [CommonModule, RouterModule, StarRatingModule.forRoot()],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: 'outline' },
    },
  ],
})
export class SharedModule {}
