import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class StorageHelper {
  set(key: string, data: any): void {
    try {
      sessionStorage.setItem(key, JSON.stringify(data));
    } catch (err) {
      console.error('Error saving to localStorage', err);
    }
  }

  get(key: string): any {
    try {
      return JSON.parse(sessionStorage.getItem(key) ?? 'null');
    } catch (err) {
      console.error('Error getting from localStorage', err);
      return null;
    }
  }
}
