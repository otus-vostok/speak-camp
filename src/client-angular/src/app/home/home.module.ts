import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '@shared/shared.module';
import { HomeRoutingModule } from './home.routing.module';
import { HomeComponent } from './containers/home/home.component';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { HomeEffects } from './state/effects/home-effects';
import { homeFeature } from './state/reducers/home-reducers';
import { HomeMeetingsComponent } from './components/home-meetings/home-meetings.component';
import { HomeUsersComponent } from './components/home-users/home-users.component';
import { HomePromoComponent } from './components/home-promo/home-promo.component';

@NgModule({
  declarations: [
    HomeComponent,
    HomePromoComponent,
    HomeMeetingsComponent,
    HomeUsersComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    HomeRoutingModule,
    StoreModule.forFeature(homeFeature),
    EffectsModule.forFeature([HomeEffects]),
  ],
})
export class HomeModule {}
