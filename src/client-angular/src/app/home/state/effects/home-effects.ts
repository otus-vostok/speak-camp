import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap } from 'rxjs';
import { UsersService } from 'src/app/shared/services/users.service';
import { MeetingsService } from '@shared/services/meetings.service';
import { homeActions } from '../actions/home-actions';

@Injectable()
export class HomeEffects {
  loadHomeMeetings = createEffect(() =>
    this.actions$.pipe(
      ofType(homeActions.loadHomeMeetings),
      switchMap((action) =>
        this.meetingsService.getUpcomingMeetings(action.top).pipe(
          map((result) => homeActions.loadHomeMeetingsSuccess({ result })),
          catchError((error) =>
            of(homeActions.loadHomeMeetingsFailure({ error }))
          )
        )
      )
    )
  );

  loadHomeUsers = createEffect(() =>
    this.actions$.pipe(
      ofType(homeActions.loadHomeUsers),
      switchMap((action) =>
        this.usersService.getRatedUsers(action.top).pipe(
          map((result) => homeActions.loadHomeUsersSuccess({ result })),
          catchError((error) => of(homeActions.loadHomeUsersFailure({ error })))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private meetingsService: MeetingsService,
    private usersService: UsersService,
    private store: Store
  ) {}
}
