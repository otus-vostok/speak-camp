import { createActionGroup, props } from '@ngrx/store';
import { ListResult } from 'src/app/shared/models/list-result';
import { Meeting } from 'src/app/shared/models/meeting';
import { User } from 'src/app/shared/models/user';

export const homeActions = createActionGroup({
  source: 'Home',
  events: {
    'Load Home Meetings': props<{ top: number }>(),
    'Load Home Meetings Failure': props<{ error: Error }>(),
    'Load Home Meetings Success': props<{ result: ListResult<Meeting> }>(),

    'Load Home Users': props<{ top: number }>(),
    'Load Home Users Failure': props<{ error: Error }>(),
    'Load Home Users Success': props<{ result: ListResult<User> }>(),
  },
});
