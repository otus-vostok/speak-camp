import { createSelector } from '@ngrx/store';
import { homeFeature } from '../reducers/home-reducers';

const selectUsers = createSelector(
  homeFeature.selectHomeState,
  (val) => val.users.items
);

const selectMeetings = createSelector(
  homeFeature.selectHomeState,
  (val) => val.meetings.items
);

export const homeQuery = {
  selectUsers,
  selectMeetings,
};
