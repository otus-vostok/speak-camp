import { createFeature, createReducer, on } from '@ngrx/store';
import { Meeting } from 'src/app/shared/models/meeting';
import { User } from 'src/app/shared/models/user';
import { homeActions } from '../actions/home-actions';

export interface HomeState {
  meetings: { items: Meeting[]; loading: boolean; loaded: boolean };
  users: { items: User[]; loading: boolean; loaded: boolean };
}

export const homeInitialState: HomeState = {
  meetings: { items: [], loading: false, loaded: false },
  users: { items: [], loading: false, loaded: false },
};

export const homeFeature = createFeature({
  name: 'home',
  reducer: createReducer(
    homeInitialState,

    on(homeActions.loadHomeUsers, (state) => {
      return { ...state, users: { items: [], loading: true, loaded: false } };
    }),
    on(homeActions.loadHomeUsersSuccess, (state, { result }) => {
      return {
        ...state,
        users: { items: result.items, loading: false, loaded: true },
      };
    }),
    on(homeActions.loadHomeUsersFailure, (state) => {
      return { ...state, users: { items: [], loading: false, loaded: false } };
    }),

    on(homeActions.loadHomeMeetings, (state) => {
      return {
        ...state,
        meetings: { items: [], loading: true, loaded: false },
      };
    }),
    on(homeActions.loadHomeMeetingsSuccess, (state, { result }) => {
      return {
        ...state,
        meetings: { items: result.items, loading: false, loaded: true },
      };
    }),
    on(homeActions.loadHomeMeetingsFailure, (state) => {
      return {
        ...state,
        meetings: { items: [], loading: false, loaded: false },
      };
    })
  ),
});
