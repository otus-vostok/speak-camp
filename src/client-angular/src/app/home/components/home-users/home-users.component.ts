import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'sc-home-users',
  templateUrl: './home-users.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeUsersComponent {
  @Input()
  users: User[] | null;
}
