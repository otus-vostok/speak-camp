import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { Meeting } from 'src/app/shared/models/meeting';

@Component({
  selector: 'sc-home-meetings',
  templateUrl: './home-meetings.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeMeetingsComponent {
  @Input()
  meetings: Meeting[] | null;

  constructor() {}
}
