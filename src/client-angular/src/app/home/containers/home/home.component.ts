import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Meeting } from 'src/app/shared/models/meeting';
import { User } from 'src/app/shared/models/user';
import { homeActions } from '../../state/actions/home-actions';
import { homeQuery } from '../../state/selectors/home-selectors';

@Component({
  templateUrl: './home.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HomeComponent implements OnInit {
  meetings$: Observable<Meeting[]>;
  users$: Observable<User[]>;

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(homeActions.loadHomeMeetings({ top: 3 }));
    this.store.dispatch(homeActions.loadHomeUsers({ top: 4 }));
    this.meetings$ = this.store.select(homeQuery.selectMeetings);
    this.users$ = this.store.select(homeQuery.selectUsers);
  }
}
