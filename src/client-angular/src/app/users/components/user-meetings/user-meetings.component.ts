import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Meeting } from 'src/app/shared/models/meeting';

@Component({
  selector: 'sc-user-meetings',
  templateUrl: './user-meetings.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserMeetingsComponent {
  @Input()
  meetings: Meeting[] | null;

  constructor() {}
}
