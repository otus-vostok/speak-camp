import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Review } from 'src/app/shared/models/review';

@Component({
  selector: 'sc-user-reviews',
  templateUrl: './user-reviews.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserReviewsComponent {
  @Input()
  reviews: Review[] | null;

  constructor() {}
}
