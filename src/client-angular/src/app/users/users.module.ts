import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '@shared/shared.module';
import { UserSummaryComponent } from './components/user-summary/user-summary.component';
import { UserDetailsComponent } from './containers/user-details/user-details.component';
import { UserDetailsEffects } from './state/effects/user-details-effects';
import { UsersRoutingModule } from './users.routing.module';
import { usersFeature } from './state/reducers';
import { UserReviewsComponent } from './components/user-reviews/user-reviews.component';
import { UserMeetingsComponent } from './components/user-meetings/user-meetings.component';

@NgModule({
  declarations: [
    UserDetailsComponent,
    UserSummaryComponent,
    UserMeetingsComponent,
    UserReviewsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    UsersRoutingModule,
    StoreModule.forFeature(usersFeature),
    EffectsModule.forFeature([UserDetailsEffects]),
  ],
})
export class UsersModule {}
