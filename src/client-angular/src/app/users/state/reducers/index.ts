import { createFeature, combineReducers } from '@ngrx/store';
import { userDetailsReducer, UserDetailsState } from './user-details-reducer';

export const usersFeature = createFeature({
  name: 'users',
  reducer: combineReducers({
    userDetails: userDetailsReducer,
  }),
});
