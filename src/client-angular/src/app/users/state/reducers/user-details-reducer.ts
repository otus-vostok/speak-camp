import { createReducer, on } from '@ngrx/store';
import { Meeting } from 'src/app/shared/models/meeting';
import { Review } from 'src/app/shared/models/review';
import { UserDetails } from 'src/app/shared/models/user-details';
import { userDetailsActions } from '../actions/user-details-actions';

export interface UserDetailsState {
  user: UserDetails | null;
  loading: boolean;
  loaded: boolean;

  meetings: { items: Meeting[]; loading: boolean; loaded: boolean };
  reviews: { items: Review[]; loading: boolean; loaded: boolean };
}

export const userDetailsInitialState: UserDetailsState = {
  user: null,
  loading: false,
  loaded: false,

  meetings: { items: [], loading: false, loaded: false },
  reviews: { items: [], loading: false, loaded: false },
};

export const userDetailsReducer = createReducer(
  userDetailsInitialState,

  on(userDetailsActions.loadUserDetails, (state) => {
    return { ...state, loading: true };
  }),
  on(userDetailsActions.loadUserDetailsSuccess, (state, { result }) => {
    return {
      ...state,
      user: result,
      loading: false,
      loaded: true,
    };
  }),
  on(userDetailsActions.loadUserDetailsFailure, (state) => {
    return {
      ...state,
      user: null,
      loading: false,
      loaded: false,
    };
  }),
  on(userDetailsActions.loadUserMeetings, (state) => {
    return {
      ...state,
      meetings: { items: [], loading: true, loaded: false },
    };
  }),
  on(userDetailsActions.loadUserMeetingsSuccess, (state, { result }) => {
    return {
      ...state,
      meetings: { items: result.items, loading: false, loaded: true },
    };
  }),
  on(userDetailsActions.loadUserMeetingsFailure, (state) => {
    return {
      ...state,
      meetings: { items: [], loading: false, loaded: false },
    };
  }),

  on(userDetailsActions.loadUserReviews, (state) => {
    return {
      ...state,
      reviews: { items: [], loading: true, loaded: false },
    };
  }),
  on(userDetailsActions.loadUserReviewsSuccess, (state, { result }) => {
    return {
      ...state,
      reviews: { items: result.items, loading: false, loaded: true },
    };
  }),
  on(userDetailsActions.loadUserReviewsFailure, (state) => {
    return {
      ...state,
      reviews: { items: [], loading: false, loaded: false },
    };
  })
);
