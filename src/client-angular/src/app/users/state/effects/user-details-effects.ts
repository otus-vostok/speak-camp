import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { catchError, map, of, switchMap } from 'rxjs';
import { MeetingsService } from 'src/app/shared/services/meetings.service';
import { ReviewsService } from 'src/app/shared/services/reviews.service';
import { UsersService } from '@shared/services/users.service';
import { userDetailsActions } from '../actions/user-details-actions';

@Injectable()
export class UserDetailsEffects {
  loadDetails = createEffect(() =>
    this.actions$.pipe(
      ofType(userDetailsActions.loadUserDetails),
      switchMap((action) =>
        this.usersService.getUserDetails(action.id).pipe(
          map((result) =>
            userDetailsActions.loadUserDetailsSuccess({ result })
          ),
          catchError((error) =>
            of(userDetailsActions.loadUserDetailsFailure({ error }))
          )
        )
      )
    )
  );

  loadUserMeetings = createEffect(() =>
    this.actions$.pipe(
      ofType(userDetailsActions.loadUserMeetings),
      switchMap((action) =>
        this.meetingsService.getUserMeetings(action.id).pipe(
          map((result) =>
            userDetailsActions.loadUserMeetingsSuccess({ result })
          ),
          catchError((error) =>
            of(userDetailsActions.loadUserMeetingsFailure({ error }))
          )
        )
      )
    )
  );

  loadUserReviews = createEffect(() =>
    this.actions$.pipe(
      ofType(userDetailsActions.loadUserReviews),
      switchMap((action) =>
        this.reviewsService.getUserReviews(action.id).pipe(
          map((result) =>
            userDetailsActions.loadUserReviewsSuccess({ result })
          ),
          catchError((error) =>
            of(userDetailsActions.loadUserReviewsFailure({ error }))
          )
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private usersService: UsersService,
    private reviewsService: ReviewsService,
    private meetingsService: MeetingsService,
    private store: Store
  ) {}
}
