import { createSelector } from '@ngrx/store';
import { usersFeature } from '../reducers';

const selectUser = createSelector(
  usersFeature.selectUserDetails,
  (val) => val.user
);

const selectLoading = createSelector(
  usersFeature.selectUserDetails,
  (val) => val.loading
);

const selectMeetings = createSelector(
  usersFeature.selectUserDetails,
  (val) => val.meetings?.items
);

const selectReviews = createSelector(
  usersFeature.selectUserDetails,
  (val) => val.reviews?.items
);

export const userDetailsQuery = {
  selectUser,
  selectLoading,
  selectMeetings,
  selectReviews,
};
