import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { Meeting } from 'src/app/shared/models/meeting';
import { Review } from 'src/app/shared/models/review';
import { UserDetails } from 'src/app/shared/models/user-details';
import { userDetailsActions } from '../../state/actions/user-details-actions';
import { userDetailsQuery } from '../../state/selectors/user-details-selectors';

@Component({
  templateUrl: './user-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserDetailsComponent implements OnInit {
  userId: number;
  user$: Observable<UserDetails | null>;
  meetings$: Observable<Meeting[]>;
  reviews$: Observable<Review[]>;

  constructor(private route: ActivatedRoute, private store: Store) {}

  ngOnInit(): void {
    this.userId = this.route.snapshot.params['id'];
    this.store.dispatch(
      userDetailsActions.loadUserDetails({ id: this.userId })
    );
    this.store.dispatch(
      userDetailsActions.loadUserMeetings({ id: this.userId })
    );
    this.store.dispatch(
      userDetailsActions.loadUserReviews({ id: this.userId })
    );
    this.user$ = this.store.select(userDetailsQuery.selectUser);
    this.meetings$ = this.store.select(userDetailsQuery.selectMeetings);
    this.reviews$ = this.store.select(userDetailsQuery.selectReviews);
  }
}
