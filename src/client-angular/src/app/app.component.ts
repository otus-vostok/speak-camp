import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { authActions } from './auth/state/actions/auth-actions';

@Component({
  selector: 'sc-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {
  title = 'speak-camp';
  constructor(private store: Store) {}

  ngOnInit() {
    this.store.dispatch(authActions.loadtoken());
  }
}
