import { createFeature, createReducer, on } from '@ngrx/store';
import { MeetingDetails } from 'src/app/shared/models/meeting-details';
import { meetingDetailsActions } from '../actions/meeting-details-actions';

export interface MeetingDetailsState {
  meeting: MeetingDetails | null;
  loading: boolean;
  loaded: boolean;
}

export const meetingDetailsInitialState: MeetingDetailsState = {
  meeting: null,
  loading: false,
  loaded: false,
};

export const meetingDetailsReducer = createReducer(
  meetingDetailsInitialState,

  on(meetingDetailsActions.loadMeetingDetails, (state) => {
    return { ...state, loading: true };
  }),
  on(meetingDetailsActions.loadMeetingDetailsSuccess, (state, { result }) => {
    return {
      ...state,
      meeting: result,
      loading: false,
      loaded: true,
    };
  }),
  on(meetingDetailsActions.loadMeetingDetailsFailure, (state) => {
    return {
      ...state,
      meeting: null,
      loading: false,
      loaded: false,
    };
  })
);
