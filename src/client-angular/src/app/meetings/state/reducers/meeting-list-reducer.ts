import { createFeature, createReducer, on } from '@ngrx/store';
import { Meeting } from 'src/app/shared/models/meeting';
import { MeetingFilter } from '../../models/meeting-filter';

import { meetingListActions } from '../actions/meeting-list-actions';

export interface MeetingListState {
  sorting: string;
  filter: MeetingFilter;

  count: number;
  page: number;
  pageSize: number;
  pagesCount: number;
  items: Meeting[];

  loading: boolean;
  loaded: boolean;
}

export const meetingListInitialState: MeetingListState = {
  sorting: 'Popularity',
  filter: { languageKey: '', languageLevel: '', dateFrom: null, dateTo: null },

  count: 0,
  page: 1,
  pageSize: 20,
  pagesCount: 0,
  items: [],

  loading: false,
  loaded: false,
};

export const meetingListReducer = createReducer(
  meetingListInitialState,
  on(meetingListActions.setMeetingsSorting, (state, { sorting }) => {
    return { ...state, sorting };
  }),
  on(meetingListActions.setMeetingsFilter, (state, { filter }) => {
    return { ...state, filter: filter };
  }),
  on(meetingListActions.loadMeetings, (state) => {
    return { ...state, loading: true };
  }),
  on(meetingListActions.loadMeetingsSuccess, (state, { result }) => {
    return {
      ...state,
      count: result.count,
      page: result.page,
      pagesCount: result.pagesCount,
      items: result.items,
      loading: false,
      loaded: true,
    };
  }),
  on(meetingListActions.loadMeetingsFailure, (state) => {
    return {
      ...state,
      count: 0,
      page: 1,
      pagesCount: 0,
      items: [],
      loading: false,
      loaded: false,
    };
  })
);
