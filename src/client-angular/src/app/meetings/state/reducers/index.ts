import { createFeature, combineReducers } from '@ngrx/store';
import {
  meetingDetailsReducer,
  MeetingDetailsState,
} from './meeting-details-reducer';
import { meetingListReducer, MeetingListState } from './meeting-list-reducer';

export const meetingsFeature = createFeature({
  name: 'meetings',
  reducer: combineReducers({
    meetingList: meetingListReducer,
    meetingDetails: meetingDetailsReducer,
  }),
});
