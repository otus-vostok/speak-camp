import { createSelector } from '@ngrx/store';
import { meetingsFeature } from '../reducers';

const selectFilter = createSelector(
  meetingsFeature.selectMeetingList,
  (val) => val.filter
);

const selectCount = createSelector(
  meetingsFeature.selectMeetingList,
  (val) => val.count
);

const selectItems = createSelector(
  meetingsFeature.selectMeetingList,
  (val) => val.items
);

const selectSorting = createSelector(
  meetingsFeature.selectMeetingList,
  (val) => val.sorting
);

const selectLoading = createSelector(
  meetingsFeature.selectMeetingList,
  (val) => val.loading
);

export const meetingListQuery = {
  selectFilter,
  selectCount,
  selectItems,
  selectSorting,
  selectLoading,
};
