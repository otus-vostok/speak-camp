import { createSelector } from '@ngrx/store';
import { meetingsFeature } from '../reducers';

const selectMeeting = createSelector(
  meetingsFeature.selectMeetingDetails,
  (val) => val.meeting
);

const selectLoading = createSelector(
  meetingsFeature.selectMeetingDetails,
  (val) => val.loading
);

const selectVisits = createSelector(
  selectMeeting,
  (meeting) => meeting?.visits
);

export const meetingDetailsQuery = {
  selectMeeting,
  selectLoading,
  selectVisits,
};
