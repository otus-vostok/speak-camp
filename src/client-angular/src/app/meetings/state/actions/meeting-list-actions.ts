import { createActionGroup, emptyProps, props } from '@ngrx/store';
import { ListResult } from 'src/app/shared/models/list-result';
import { Meeting } from 'src/app/shared/models/meeting';
import { MeetingFilter } from '../../models/meeting-filter';

export const meetingListActions = createActionGroup({
  source: 'Meeting List',
  events: {
    'Set Meetings Sorting': props<{ sorting: string }>(),
    'Set Meetings Filter': props<{ filter: MeetingFilter }>(),
    'Load Meetings': emptyProps(),
    'Load Meetings Failure': props<{ error: Error }>(),
    'Load Meetings Success': props<{ result: ListResult<Meeting> }>(),
  },
});
