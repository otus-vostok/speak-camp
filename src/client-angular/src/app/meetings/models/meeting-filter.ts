export interface MeetingFilter {
  languageKey: string;
  languageLevel: string;
  dateFrom: Date | null;
  dateTo: Date | null;
}
