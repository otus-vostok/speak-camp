import { MeetingDetails } from 'src/app/shared/models/meeting-details';

export interface MeetingActionEvent {
  meetingId: number;
  visitId?: number;
  action: 'join' | 'cancel';
}
