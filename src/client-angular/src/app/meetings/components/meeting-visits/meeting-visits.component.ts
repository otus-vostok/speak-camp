import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Visit } from 'src/app/shared/models/visit';

@Component({
  selector: 'sc-meeting-visits',
  templateUrl: './meeting-visits.component.html',
})
export class MeetingVisitsComponent implements OnInit {
  @Input()
  visits: Visit[];

  constructor() {}

  ngOnInit(): void {}
}
