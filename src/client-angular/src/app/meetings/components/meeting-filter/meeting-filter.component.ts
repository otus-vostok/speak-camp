import {
  ChangeDetectionStrategy,
  Component,
  DoCheck,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { debounceTime, distinctUntilChanged, take } from 'rxjs/operators';
import { MeetingFilter } from '../../models/meeting-filter';

@UntilDestroy()
@Component({
  selector: 'sc-meeting-filter',
  templateUrl: './meeting-filter.component.html',
})
export class MeetingFilterComponent implements OnInit {
  @Input()
  filter: MeetingFilter | null;

  @Output()
  filterChange: EventEmitter<MeetingFilter> = new EventEmitter<MeetingFilter>();

  form: FormGroup;

  constructor(private fb: FormBuilder) {}

  ngOnInit(): void {
    this.initForm();
    this.subscribeFormChanges();
  }

  initForm() {
    this.form = this.fb.group({
      languageKey: '',
      languageLevel: '',
      dateFrom: '',
      dateTo: '',
    });

    if (this.filter) {
      this.form.patchValue(this.filter, { emitEvent: false });
    }
  }

  subscribeFormChanges() {
    this.form.valueChanges
      .pipe(debounceTime(100), distinctUntilChanged(), untilDestroyed(this))
      .subscribe((val) => {
        this.filterChange.emit(val);
      });
  }
}
