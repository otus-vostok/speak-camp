import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
} from '@angular/core';
import { CurrentUser } from 'src/app/shared/models/current-user';
import { MeetingDetails } from 'src/app/shared/models/meeting-details';
import { Visit } from 'src/app/shared/models/visit';
import { MeetingActionEvent } from 'src/app/meetings/models/meeting-action-event';

@Component({
  selector: 'sc-meeting-summary-actions',
  templateUrl: './meeting-summary-actions.component.html',
})
export class MeetingSummaryActionsComponent implements OnChanges {
  @Input()
  authUser: CurrentUser | null;
  @Input()
  meeting: MeetingDetails;
  @Output()
  meetingAction: EventEmitter<MeetingActionEvent> = new EventEmitter<MeetingActionEvent>();

  viewState: 'NeedAuth' | 'CanJoin' | 'Joined' | 'MyMeeting';
  visit: Visit | undefined;

  constructor() {}

  ngOnChanges(): void {
    this.initViewState();
  }

  initViewState() {
    this.visit = this.meeting.visits.find(
      (visit) => visit.user.id == this.authUser?.id
    );

    if (!this.authUser) {
      this.viewState = 'NeedAuth';
    } else if (this.authUser.id === this.meeting.user.id) {
      this.viewState = 'MyMeeting';
    } else if (this.visit) {
      this.viewState = 'Joined';
    } else {
      this.viewState = 'CanJoin';
    }
  }

  cancelJoin(event: MouseEvent) {
    this.meetingAction.emit({
      meetingId: this.meeting.id,
      visitId: this.visit?.id,
      action: 'cancel',
    });
  }

  join(event: MouseEvent) {
    this.meetingAction.emit({ meetingId: this.meeting.id, action: 'join' });
  }
}
