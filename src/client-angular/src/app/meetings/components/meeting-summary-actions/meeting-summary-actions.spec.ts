import { CurrentUser } from 'src/app/shared/models/current-user';
import { MeetingDetails } from 'src/app/shared/models/meeting-details';
import { User } from 'src/app/shared/models/user';
import { MeetingSummaryActionsComponent } from './meeting-summary-actions.component';

describe('MeetingSummaryActionsComponent', () => {
  let sut: MeetingSummaryActionsComponent;

  beforeEach(() => {
    sut = new MeetingSummaryActionsComponent();
  });

  it('should use NeedAuth view for non-authorized user', () => {
    sut.authUser = null;
    sut.initViewState();
    expect(sut.viewState).toBe('NeedAuth');
  });

  it('should use MyMeeting view for meeting owner', () => {
    sut.authUser = <CurrentUser>{ id: 123 };
    sut.meeting = <MeetingDetails>{ user: { id: 123 } };
    sut.initViewState();
    expect(sut.viewState).toBe('MyMeeting');
  });

  it('should use Joined view for already joined authorized user', () => {
    sut.authUser = <CurrentUser>{ id: 123 };
    sut.meeting = <MeetingDetails>{ id: 1, user: { id: 1 } };
    sut.meeting.visits = [{ id: 1, user: <User>{ id: 123 } }];
    sut.initViewState();
    expect(sut.viewState).toBe('Joined');
  });

  it('should use CanJoin view for not joined authorized user', () => {
    sut.authUser = <CurrentUser>{ id: 123 };
    sut.meeting = <MeetingDetails>{ id: 1, user: { id: 1 } };
    sut.meeting.visits = [];
    sut.initViewState();
    expect(sut.viewState).toBe('CanJoin');
  });
});
