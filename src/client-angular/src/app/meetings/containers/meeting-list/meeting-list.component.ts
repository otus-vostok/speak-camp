import {
  ChangeDetectionStrategy,
  Component,
  DoCheck,
  OnInit,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { map, Observable } from 'rxjs';
import { Meeting } from 'src/app/shared/models/meeting';
import { MeetingFilter } from '../../models/meeting-filter';
import { meetingListActions } from '../../state/actions/meeting-list-actions';
import { meetingListQuery } from '../../state/selectors/meeting-list-selectors';

@Component({
  templateUrl: './meeting-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MeetingListComponent implements OnInit {
  meetings$: Observable<Meeting[]> = this.store.select(
    meetingListQuery.selectItems
  );
  count$: Observable<number> = this.store.select(meetingListQuery.selectCount);
  sort$: Observable<string> = this.store.select(meetingListQuery.selectSorting);
  filter$: Observable<MeetingFilter> = this.store.select(
    meetingListQuery.selectFilter
  );

  constructor(private store: Store) {}

  ngOnInit(): void {
    this.store.dispatch(meetingListActions.loadMeetings());
  }

  onSortChange(value: string) {
    this.store.dispatch(
      meetingListActions.setMeetingsSorting({ sorting: value })
    );
  }

  onFilterChange(value: MeetingFilter) {
    this.store.dispatch(
      meetingListActions.setMeetingsFilter({ filter: value })
    );
  }
}
