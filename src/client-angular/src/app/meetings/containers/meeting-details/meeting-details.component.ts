import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Observable, switchMap } from 'rxjs';
import { ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { MeetingDetails } from 'src/app/shared/models/meeting-details';
import { Store } from '@ngrx/store';
import { UntilDestroy, untilDestroyed } from '@ngneat/until-destroy';
import { CurrentUser } from 'src/app/shared/models/current-user';
import { authQuery } from 'src/app/auth/state/selectors/auth-selectors';
import { meetingDetailsActions } from '../../state/actions/meeting-details-actions';
import { meetingDetailsQuery } from '../../state/selectors/meeting-details-selectors';
import { MeetingActionEvent } from '../../models/meeting-action-event';

@Component({
  templateUrl: './meeting-details.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
@UntilDestroy()
export class MeetingDetailsComponent implements OnInit {
  meeting$: Observable<MeetingDetails | null>;
  authUser$: Observable<CurrentUser | null>;

  constructor(private route: ActivatedRoute, private store: Store) {}

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];

    this.store.dispatch(meetingDetailsActions.loadMeetingDetails({ id }));

    this.meeting$ = this.store.select(meetingDetailsQuery.selectMeeting);
    this.authUser$ = this.store.select(authQuery.selectUser);
  }

  onMeetingAction(event: MeetingActionEvent) {
    if (event.action == 'join') {
      this.store.dispatch(
        meetingDetailsActions.joinMeeting({ id: event.meetingId })
      );
    } else if (event.action == 'cancel' && event.visitId) {
      this.store.dispatch(
        meetingDetailsActions.cancelJoinMeeting({
          id: event.meetingId,
          visitId: event.visitId,
        })
      );
    }
  }
}
