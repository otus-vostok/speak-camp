import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MeetingDetailsComponent } from './containers/meeting-details/meeting-details.component';
import { MeetingListComponent } from './containers/meeting-list/meeting-list.component';

const routes: Routes = [
  {
    path: '',
    component: MeetingListComponent,
  },
  {
    path: ':id',
    component: MeetingDetailsComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class MeetingsRoutingModule {}
