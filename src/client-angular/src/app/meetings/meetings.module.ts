import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from '@shared/shared.module';
import { MeetingDetailsEffects } from './state/effects/meeting-details-effects';
import { MeetingSummaryActionsComponent } from './components/meeting-summary-actions/meeting-summary-actions.component';
import { MeetingSummaryComponent } from './components/meeting-summary/meeting-summary.component';
import { MeetingVisitsComponent } from './components/meeting-visits/meeting-visits.component';
import { MeetingDetailsComponent } from './containers/meeting-details/meeting-details.component';
import { MeetingListComponent } from './containers/meeting-list/meeting-list.component';
import { MeetingSortComponent } from './components/meeting-sort/meeting-sort.component';
import { MeetingFilterComponent } from './components/meeting-filter/meeting-filter.component';
import { meetingsFeature } from './state/reducers';
import { MeetingListEffects } from './state/effects/meeting-list-effects';
import { MeetingsRoutingModule } from './meetings.routing.module';

@NgModule({
  declarations: [
    MeetingDetailsComponent,
    MeetingSummaryComponent,
    MeetingVisitsComponent,
    MeetingSummaryActionsComponent,
    MeetingListComponent,
    MeetingSortComponent,
    MeetingFilterComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    MeetingsRoutingModule,
    StoreModule.forFeature(meetingsFeature),
    EffectsModule.forFeature([MeetingDetailsEffects, MeetingListEffects]),
  ],
})
export class MeetingsModule {}
