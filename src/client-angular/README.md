# SpeakCamp

This is Angular/NGRX client vesion for SpeakCamp speaking clubs project

Screenshots: https://www.figma.com/file/BZMvQeesgKjvTyMfZ0uI9L/SpeakCamp-Screens

UI prototype: https://www.figma.com/file/Cj9wgtfjyoludd8PMGpPbp/SpeakCamp-prototype
